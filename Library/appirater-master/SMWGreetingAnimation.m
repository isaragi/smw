//
//  SMWGreetingAnimation.m
//  News＋
//
//  Created by Atsushi Igarashi on 2014/03/18.
//  Copyright (c) 2014年 Atsushi Igarashi. All rights reserved.
//

#import "SMWGreetingAnimation.h"

@implementation SMWGreetingAnimation

- (void)animationWithImageView:(UIImageView *)imageView {
    
    // 画像を表示する
    UIImage *icon = [UIImage imageNamed:@"icon.png"];
    imageView.image = icon;
    
    // アニメーション
    [UIView animateWithDuration:0.3f // アニメーション速度
                          delay:0.1f
                        options:UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         // 画像を縮小
                         imageView.transform = CGAffineTransformMakeScale(0.8, 0.8);
                         
                     } completion:^(BOOL finished) {
                         // 次のアニメーションへ
                         [self nextAnimationWithImageView:imageView];
                     }];
}
- (void)nextAnimationWithImageView:(UIImageView *)imageView {
    [UIView animateWithDuration:0.2f
                          delay:0.0f
                        options:UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         imageView.alpha = 0.2;
                         //画像を拡大
                         imageView.transform = CGAffineTransformMakeScale(20.0, 20.0);
                         //回転アニメーション
                         [self spinAnimationWithImageView:imageView];
                         
                     } completion:^(BOOL finished) {

                         imageView.image = nil;
                     }];
}

- (void)spinAnimationWithImageView:(UIImageView *)imageView {
 
    CABasicAnimation* rotationAnimation;
    rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    rotationAnimation.toValue = [NSNumber numberWithFloat: M_PI * 2.0 /* full rotation*/ * 3 * 0.5 ];
    rotationAnimation.duration = 0.3;
    rotationAnimation.cumulative = YES;
    rotationAnimation.repeatCount = 1;
    [imageView.layer addAnimation:rotationAnimation forKey:@"rotationAnimation"];
    
}
@end
