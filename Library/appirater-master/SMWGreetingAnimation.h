//
//  SMWGreetingAnimation.h
//  News＋
//
//  Created by Atsushi Igarashi on 2014/03/18.
//  Copyright (c) 2014年 Atsushi Igarashi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SMWGreetingAnimation : NSObject

- (void)animationWithImageView:(UIImageView*)imageView;

@end
