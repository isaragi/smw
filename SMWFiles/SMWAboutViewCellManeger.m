//
//  SMWAboutViewCellManeger.m
//  TheMatome
//
//  Created by Atsushi Igarashi on 2013/12/25.
//  Copyright (c) 2013年 Atsushi Igarashi. All rights reserved.
//

#import "SMWAboutViewCellManeger.h"
#import "SMWAboutViewController.h"

@implementation TMAboutViewSectionItem

@end


@interface SMWAboutViewCellManeger ()

@property (nonatomic, strong) NSArray *sectionHeaderName;
@property (nonatomic, strong) NSArray *sectionFooterName;
@property (nonatomic, strong) NSArray *sectionFooterView;
@property (nonatomic, strong) NSArray *cellLabelText;
@property (nonatomic, strong, readonly) TMAboutViewSectionItem *sectionItem;
@end

@implementation SMWAboutViewCellManeger

- (id)init
{
    self = [super init];
    if (self) {
        
        [self data];
        _celldata = [NSMutableArray array];
        
        for (int i = 0; i < [self sectionCount]; i++) {
            
            _sectionItem = [TMAboutViewSectionItem new];
            _sectionItem.headerName = self.sectionHeaderName[i];
            _sectionItem.footerName = self.sectionFooterName[i];
            _sectionItem.footerView = self.sectionFooterView[i];
            
            if ( [self.cellLabelText count] > i ) {
                _sectionItem.cellLabelText = self.cellLabelText[i];
            }
            
            [_celldata addObject:_sectionItem];
        }
    }
    return self;
}

-(void)data
{
    //　Section Heder Title
    _sectionHeaderName = @[@"フィードバック",
                           @"使い方 & レビュー",
                           @"License",
                           @""];
    
    
    // Section Footer Title
    _sectionFooterName = @[@"何か不具合やご意見がございましたらこちらからメールにてご連絡頂けます。",
                           @"レビューを書いて頂けると今後の開発の励みになります。",
                           @"\n",
                           @"\n"];
    
    
    // Section Footer View
    UIView * copyRight = [self copyRightView];
    
    _sectionFooterView = @[[NSNull null],
                           [NSNull null],
                           [NSNull null],
                           copyRight];
    
    
    //Label Text
    NSString *section0 = @"フィードバックを送信する";
    NSArray  *section1 = @[@"使い方のヒント", @"レビューを書く"];
    NSArray *section2 = @[@"Special Thanks", @"利用規約"];
    _cellLabelText = @[section0,
                       section1,
                       section2];
    
}

- (NSInteger)sectionCount
{
    return [[self sectionFooterName] count];
}

- (NSInteger)rowCountWithSectionNumber:(NSInteger)num
{
    
    if ([_cellLabelText count] <= num) {
        return 0;
    }
    
    if ([self.cellLabelText[num] isKindOfClass:[NSString class]]) {
        
        return 1;
    } else {
        
        return [self.cellLabelText[num] count];
    }
}

- (NSArray*)footerView
{
    
    UIView * copyRight = [self copyRightView];
    
    return @[[NSNull null],[NSNull null],[NSNull null],copyRight];
}

-(UIView*)copyRightView
{
    CGRect windowSize = [[UIScreen mainScreen] bounds];
    UILabel *label = [[UILabel alloc] init];
    UIView *copyRight  = [UIView new];
    label.frame = CGRectMake(0, 0, windowSize.size.width, 40);
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont fontWithName:@"AppleGothic" size:12];
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor darkGrayColor];
    label.numberOfLines = 2;
    label.text = [self copyrightAndVersion];
    [copyRight addSubview:label];
    return copyRight;
}

- (NSString*)copyrightAndVersion
{
    NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];
    NSString *copyright = [NSString stringWithFormat:@"サクッとまとめ奴www Version %@\nCopyright © 2015 AIApps All Rights Reserved.",version];
    
    return copyright;
}
@end