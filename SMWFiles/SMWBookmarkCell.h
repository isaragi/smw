//
//  TMDetailBookmarkCell.h
//  TheMatome
//
//  Created by Atsushi Igarashi on 13/07/25.
//  Copyright (c) 2013年 Atsushi Igarashi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SMWBookmarkCell : UITableViewCell

@property (nonatomic, strong) NSString* bookmarkTitle;
@property (nonatomic, strong) NSString* bookmarkDate;
@property (nonatomic, strong) NSString* bookmarkChannelTitle;
@property (nonatomic, strong) NSString* tweet;
@property (nonatomic, strong) NSString* hatebu;

- (void)changeCellColor;
@end