//
//  SMWDeleteCache.m
//  News＋
//
//  Created by Atsushi Igarashi on 2014/02/28.
//  Copyright (c) 2014年 Atsushi Igarashi. All rights reserved.
//

#import "SMWDeleteCache.h"
#import "SMWAppDelegate.h"

@implementation SMWDeleteCache

-(void)deleteCache
{
    [self deleteCacheWithEntityName:@"FeedItem"];
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    [ud setBool:YES forKey:@"clearCache"];
    [ud synchronize];
}

-(void)deleteCacheWithEntityName:(NSString*)entityName
{
    NSManagedObjectContext *managedObjectContext;
    SMWAppDelegate *appDelegate = (SMWAppDelegate *)[[UIApplication sharedApplication] delegate];
    managedObjectContext = [appDelegate managedObjectContext];
    
    NSFetchRequest * requestDelete = [[NSFetchRequest alloc] init];
    [requestDelete setEntity:[NSEntityDescription entityForName:entityName inManagedObjectContext:managedObjectContext]];
    [requestDelete setIncludesPropertyValues:NO];
    
    NSError * error = nil;
    NSArray * dataArray = [managedObjectContext executeFetchRequest:requestDelete error:&error];
    
    for (NSManagedObject * data in dataArray) {
        [managedObjectContext deleteObject:data];
    }
    NSError *saveError = nil;
    [managedObjectContext save:&saveError];
}

@end
