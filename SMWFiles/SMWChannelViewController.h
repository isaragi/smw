//
//  TMChannelFeedsViewController.h
//  TheMatome
//
//  Created by Atsushi Igarashi on 13/07/26.
//  Copyright (c) 2013年 Atsushi Igarashi. All rights reserved.
//

#import <UIKit/UIKit.h>
@class SMWChannelViewPagerController;

@interface TMChannelViewController : UITableViewController
@property (nonatomic, assign) NSInteger feedNumber;
@property (nonatomic, strong) SMWChannelViewPagerController *viewPager;
-(void)foregroundReload;
@end
