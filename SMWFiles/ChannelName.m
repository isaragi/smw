//
//  ChannelName.m
//  TheMatome
//
//  Created by Atsushi Igarashi on 2014/02/16.
//  Copyright (c) 2014年 Atsushi Igarashi. All rights reserved.
//

#import "ChannelName.h"
#import "FeedItem.h"


@implementation ChannelName

@dynamic name;
@dynamic feedItems;

@end
