//
//  SMWDataCache.h
//  TheMatome
//
//  Created by Atsushi Igarashi on 13/07/21.
//  Copyright (c) 2013年 Atsushi Igarashi. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ChannelName;
@class FeedCategory;

@interface SMWDataCache : NSObject

@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;

- (ChannelName *)channelWithName:(NSString *)name;
- (FeedCategory *)categoryWithName:(NSString *)name;
@end
