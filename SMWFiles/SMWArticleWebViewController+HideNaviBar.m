//
//  SMWArticleWebViewController+HideNaviBar.m
//  TheMatome
//
//  Created by Atsushi Igarashi. on 2013/12/16.
//  Copyright (c) 2013年 Atsushi Igarashi. All rights reserved.
//

#import "SMWArticleWebViewController+HideNaviBar.h"

@implementation SMWArticleWebViewController (HideNaviBar)

-(void)expand
{
    [self statusBarBg];
    if(self.isNavBarHidden) {
        return;
    }
    
    self.isNavBarHidden = YES;
    [self.navigationController setNavigationBarHidden:YES  animated:YES];
    self.webView.frame = CGRectMake(0, 0, self.webView.frame.size.width, self.view.frame.size.height);
}

-(void)contract
{
    [self statusBarBg];
    if(!self.isNavBarHidden) {
        return;
    }
    
    self.isNavBarHidden = NO;
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    self.webView.frame = CGRectMake(0, 0, self.webView.frame.size.width, self.view.frame.size.height);
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    self.startContentOffset = self.lastContentOffset = scrollView.contentOffset.y;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    CGFloat currentOffset = scrollView.contentOffset.y;
    CGFloat differenceFromStart = self.startContentOffset - currentOffset;
    CGFloat differenceFromLast = self.lastContentOffset - currentOffset;
    self.lastContentOffset = currentOffset;
    if((differenceFromStart) < 0)
    {
        
        if(scrollView.isTracking && (fabs(differenceFromLast)>1)) {
            
             [self expand];
        }
    }
    else {
        if(scrollView.isTracking && (fabs(differenceFromLast)>1)) {
            
            [self contract];
        }
    }
}

- (BOOL)scrollViewShouldScrollToTop:(UIScrollView *)scrollView
{
    [self contract];
    self.statusBarBackground.hidden = YES;
    
    return YES;
}


//--------------------------------------------------------------//
#pragma mark - Create StatusBar BackGround
//--------------------------------------------------------------//

-(void)statusBarBg
{
    if(self.iOSVersion < 7.0) {
        
        self.progressView.hidden = YES;
        return;
    }
    
    if (self.isNavBarHidden) {
        
        if (!self.statusBarBackground) {
            
            [self createStatusBarBackGroundView];
        }
        self.progressView.hidden = YES;
        self.statusBarBackground.hidden = NO;
        
    } else {
        
        self.statusBarBackground.hidden = YES;
    }
    
}

-(void)createStatusBarBackGroundView
{
    UIScreen* screen = [UIScreen mainScreen];
    self.statusBarBackground = [[UIView alloc] initWithFrame:CGRectMake(0, 0, screen.applicationFrame.size.width, 20)];
    BOOL colorCheck = [self colorCheck];
    
    if (colorCheck) {
        
        self.statusBarBackground.backgroundColor = self.navigationController.navigationBar.barTintColor;
        
    } else if ([self.themeColor isEqual: @"NightBlackColor"]) {
        
        self.statusBarBackground.backgroundColor = [UIColor colorWithWhite:0.0 alpha:1.0];
        
    }else{
        
        self.statusBarBackground.backgroundColor = [UIColor colorWithWhite:0.97 alpha:0.98];
        
    }
    [self.view addSubview:self.statusBarBackground];
}

-(BOOL)colorCheck
{
    BOOL isDefaultsColor = ([self.themeColor isEqualToString:@"DefaultsColor"] || self.themeColor == nil ) ? YES : NO;
    BOOL isNightBlackColor = ([self.themeColor isEqualToString:@"NightBlackColor"]) ? YES : NO;
    
    return ( !self.isTranslucentColor && !isNightBlackColor && !isDefaultsColor )? YES : NO;
}

@end
