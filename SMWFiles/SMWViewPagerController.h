//
//  SMWViewPagerController.h
//  TheMatome
//
//  Created by Atsushi Igarashi on 2014/01/23.
//  Copyright (c) 2014年 Atsushi Igarashi. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "ViewPagerController.h"

@interface SMWViewPagerController : ViewPagerController
@property (assign, nonatomic, readonly) NSInteger favorMatomeNumber;
@end
