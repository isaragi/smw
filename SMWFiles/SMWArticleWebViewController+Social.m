//
//  SMWArticleWebViewController+Social.m
//  TheMatome
//
//  Created by Atsushi Igarashi on 2013/10/25.
//  Copyright (c) 2013年 Atsushi Igarashi. All rights reserved.
//
#import "PocketAPI.h"
#import "SMWArticleWebViewController+Social.h"

@implementation SMWArticleWebViewController (Social)

- (void)postToFacebook:(id)sender {
    SLComposeViewController *slc = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
    NSString *val = [@"\n#" stringByAppendingString:self.channelTitleStr];
    val = [self.titleStr stringByAppendingString:val];
    [slc setInitialText:val];
    [slc addURL:[NSURL URLWithString:self.urlStr]];
    [self presentViewController:slc animated:YES completion:nil];
}

- (void)postToTwitter:(id)sender {
    SLComposeViewController *slc = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
    NSString *val = [@"\n#" stringByAppendingString:self.channelTitleStr];
    val = [self.titleStr stringByAppendingString:val];
    [slc setInitialText:val];
    [slc addURL:[NSURL URLWithString:self.urlStr]];
    [self presentViewController:slc animated:YES completion:nil];
}

- (void)postToLine:(id)sender {
    NSString *val = [self.titleStr stringByAppendingString:self.channelTitleStr];
    val = [val stringByAppendingString:@" "];
    val = [val stringByAppendingString:self.urlStr];
    val = [val stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *LINEUrlString = [NSString stringWithFormat:@"line://msg/text/%@",val];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:LINEUrlString]];
    
}

- (void)savePocket
{
    UIAlertView *okAlert = [[UIAlertView alloc] initWithTitle:nil message:@"送信しますか？" delegate:self cancelButtonTitle:@"キャンセル" otherButtonTitles:@"OK",nil];
    okAlert.tag = kSendPocketAlertTag;
    [okAlert show];
}

-(void)goPocket
{
    NSURL *url = [NSURL URLWithString:self.urlStr];
    [[PocketAPI sharedAPI] saveURL:url handler: ^(PocketAPI *API, NSURL *URL,
                                                  NSError *error){
        if (error) {
            
        } else {
            [SVProgressHUD showSuccessWithStatus:@"送信完了☻"];
        }
    }];
}

-(void)postSocial3
{
    BOOL twitterSwitchValue = [self twitterSwitch];
    BOOL facebookSwitchValue = [self facebookSwitch];
    BOOL lineSwitchValue = [self lineSwitch];
    BOOL pocketSwitchValue = [self pocketSwitch];
    
    if (twitterSwitchValue) {
        [self postToTwitter:nil];
    } else if(facebookSwitchValue) {
        [self postToFacebook:nil];
    } else if (lineSwitchValue){
        [self postToLine:nil];
    } else if (pocketSwitchValue){
        [self savePocket];
    }
}
-(void)postSocial2
{
    BOOL twitterSwitchValue = [self twitterSwitch];
    BOOL facebookSwitchValue = [self facebookSwitch];
    BOOL lineSwitchValue = [self lineSwitch];
    BOOL pocketSwitchValue = [self pocketSwitch];
    
    if ((!twitterSwitchValue && facebookSwitchValue && lineSwitchValue)||(twitterSwitchValue && !facebookSwitchValue &&lineSwitchValue)) {
        [self postToLine:nil];
    } else if(twitterSwitchValue && facebookSwitchValue) {
        [self postToFacebook:nil];
    } else if(pocketSwitchValue ) {
        [self savePocket];
    }
}
-(void)postSocial1
{
    BOOL twitterSwitchValue = [self twitterSwitch];
    BOOL facebookSwitchValue = [self facebookSwitch];
    BOOL lineSwitchValue = [self lineSwitch];
    BOOL pocketSwitchValue = [self pocketSwitch];
    
    if ((!facebookSwitchValue &&lineSwitchValue && pocketSwitchValue)||(!twitterSwitchValue && facebookSwitchValue && lineSwitchValue)) {
        [self savePocket];
    } else if ((twitterSwitchValue && facebookSwitchValue && lineSwitchValue)||(lineSwitchValue)) {
        [self postToLine:nil];
    } else {
        [self savePocket];
    }
}


- (BOOL)twitterSwitch
{
    return [[NSUserDefaults standardUserDefaults] boolForKey:@"SendTwitterSwitchValue"];
}

- (BOOL)facebookSwitch
{
    return [[NSUserDefaults standardUserDefaults] boolForKey:@"SendFacebookSwitchValue"];
}

- (BOOL)lineSwitch
{
    return [[NSUserDefaults standardUserDefaults] boolForKey:@"SendLineSwitchValue"];
}

- (BOOL)pocketSwitch
{
    return [[NSUserDefaults standardUserDefaults] boolForKey:@"SendPocketSwitchValue"];
}

@end
