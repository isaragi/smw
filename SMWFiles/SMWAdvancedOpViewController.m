//
//  SMWAdvancedOpViewController.m
//  TheMatome
//
//  Created by Atsushi Igarashi on 2013/10/25.
//  Copyright (c) 2013年 Atsushi Igarashi. All rights reserved.
//

#import "SMWAdvancedOpViewController.h"
#import "SMWNavBarTitleLabel.h"
#import "SMWAdvancedOpCellManeger.h"

@interface SMWAdvancedOpViewController ()

@property (nonatomic, strong) NSArray *userSwitch;
@property (nonatomic, strong) SMWAdvancedOpCellManeger *cellManeger;
@property (nonatomic, strong) TMAdvancedOpSectionItem *sectionItem;
@end


@implementation SMWAdvancedOpViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    [[SMWNavBarTitleLabell new] costomTitleLabelWithTitle:@"詳細設定" andNavigation:self.navigationItem];
    //[self.navigationController setToolbarHidden:YES animated:YES];
    _cellManeger = [SMWAdvancedOpCellManeger new];
    [self switchArray];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [_cellManeger sectionCount];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
   
    return ([_cellManeger rowCountWithSectionNumber:section]);
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    TMAdvancedOpSectionItem *sectionItems = _cellManeger.sectionData[section];
    
    return sectionItems.headerName;
}

- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section{
    
    TMAdvancedOpSectionItem *sectionItems = _cellManeger.sectionData[section];
    
    return sectionItems.footerName;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *CellIdentifier = [NSString stringWithFormat:@"%lu:%lu", (unsigned long)[indexPath indexAtPosition:0],
                                (unsigned long)[indexPath indexAtPosition:1]];
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    TMAdvancedOpSectionItem *sectionItems = _cellManeger.sectionData[indexPath.section];
    
    if (indexPath.row == 0) {
        
        [self makeCell:cell];
    } else {
        
        NSString *cellText = sectionItems.cellLabelText[indexPath.row];
        cell.textLabel.text =cellText;
        [self makeCell:cell andValue:indexPath.row - 1];
    }
    
    return cell;
}

-(void)makeCell:(UITableViewCell*)cell
{
    cell.textLabel.text = @"シェイクジェスチャー";
    cell.textLabel.font = [UIFont systemFontOfSize:10];
    cell.detailTextLabel.text = @"スクロールスピード";
    cell.detailTextLabel.font = [UIFont systemFontOfSize:15];
    cell.detailTextLabel.textColor = [UIColor blackColor];
    cell.accessoryType = UITableViewCellAccessoryNone;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    UISlider *scrollControl = [[UISlider alloc] initWithFrame:CGRectMake(185.0, 0.0, 110.0 - 0, 45.0)];
    scrollControl.minimumValue = 0.01;
    scrollControl.maximumValue = 0.59;
    float sliderValue = [[NSUserDefaults standardUserDefaults] floatForKey:@"scrollSpeadValue"];
    if (!sliderValue) {
        scrollControl.value = 0.3;
    } else {
        float value = 0.6 - sliderValue;
        scrollControl.value = value;
    }
    [scrollControl addTarget:self action:@selector(sliderValueChanged:) forControlEvents:UIControlEventValueChanged];
    [cell addSubview: scrollControl];
    
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(170.0, 0.0, 20.0, 45.0)];
    label.text = @"遅";
    label.font = [UIFont systemFontOfSize:10];
    label.textColor = [UIColor grayColor];
    label.backgroundColor = [UIColor clearColor];
    [cell addSubview: label];
    UILabel *label2 = [[UILabel alloc]initWithFrame:CGRectMake(300.0, 0.0, 20.0, 45.0)];
    label2.text = @"速";
    label2.font = [UIFont systemFontOfSize:10];
    label2.textColor = [UIColor grayColor];
    label2.backgroundColor = [UIColor clearColor];
    [cell addSubview: label2];
}

- (void)sliderValueChanged:(id)sender
{
    UISlider *slider = sender;
    float value = 0.6 - slider.value;
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    [ud setFloat:value forKey:@"scrollSpeadValue"];
    [ud synchronize];
}

-(void)makeCell:(UITableViewCell*)cell andValue:(NSInteger)value
{
    UISwitch *sw = [[UISwitch alloc] init];
    sw.center = CGPointMake(265, 22);
    sw.tag = value;
    sw.on = NO;
    BOOL switchValue = [[NSUserDefaults standardUserDefaults] boolForKey:_userSwitch[value]];
    if (switchValue) {
        sw.on = YES;
    }

    [sw addTarget:self action:@selector(switchValueChanged:)forControlEvents:UIControlEventValueChanged];
    [cell addSubview:sw];
}

-(void)switchValueChanged:(UISwitch*)sw
{
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    
    [ud setBool:sw.on forKey:_userSwitch[sw.tag]];
    [ud synchronize];
}

-(void)switchArray
{
    _userSwitch = @[@"AutoReloadSwitchValue",
                         @"BrowsingHistorySwitchValue",
                         @"SearchTitleSwitchValue",
                         @"SendTwitterSwitchValue",
                         @"SendLineSwitchValue",
                         @"SendFacebookSwitchValue",
                         @"SendPocketSwitchValue"];
}

@end
