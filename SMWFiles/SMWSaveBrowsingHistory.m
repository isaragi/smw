//
//  SMWSaveBrowsingHistory.m
//  TheMatome
//
//  Created by Atsushi Igarashi.  on 2013/10/09.
//  Copyright (c) 2013年 Atsushi Igarashi. All rights reserved.
//

#import "SMWSaveBrowsingHistory.h"

@implementation SMWSaveBrowsingHistory


- (void)saveBrowsingHistory:(NSDictionary*)dic
{
    BOOL switchValue = [[NSUserDefaults standardUserDefaults] boolForKey:@"BrowsingHistorySwitchValue"];
    if (!switchValue) {
        return ;
    }
    NSMutableArray* browsingData = [[[NSUserDefaults standardUserDefaults] arrayForKey:@"BrowsingHistoryData"] mutableCopy];
    if (!browsingData) {
        browsingData = [[NSMutableArray alloc] init];
    }
    
    NSDictionary* bookmarkData = [NSDictionary dictionaryWithObjectsAndKeys:
                          [dic objectForKey:@"title"], @"title",
                          [dic objectForKey:@"date"], @"date",
                          [dic objectForKey:@"url"], @"url",
                          [dic objectForKey:@"channelTitle"], @"channelTitle",
                          [dic objectForKey:@"tweet"], @"tweet",
                          [dic objectForKey:@"hatebu"], @"hatebu", nil];
    
    [browsingData insertObject:bookmarkData atIndex:0];
    
    if (browsingData.count >= 201) {
        [browsingData removeLastObject];
    }
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:browsingData forKey:@"BrowsingHistoryData"];
    [defaults synchronize];
}

@end
