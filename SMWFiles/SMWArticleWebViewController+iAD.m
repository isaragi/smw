//
//  SMWArticleWebViewController+iAD.m
//  TheMatome
//
//  Created by Atsushi Igarashi on 2013/12/18.
//  Copyright (c) 2013年 Atsushi Igarashi. All rights reserved.
//

#import "SMWArticleWebViewController+iAD.h"

@implementation SMWArticleWebViewController (iAD)

static NSString * const KRatedCurrentVersion = @"kAppiraterRatedCurrentVersion";
//--------------------------------------------------------------//
#pragma iAD
//--------------------------------------------------------------//

-(void)chackUpRate {
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    BOOL isRemoveAd = [userDefaults boolForKey:KRatedCurrentVersion];
    if (isRemoveAd) {
        
        [UIView beginAnimations:@"animateAdBannerOff" context:NULL];
        self.bannerView.frame = CGRectOffset(self.bannerView.frame, 0, self.bannerView.frame.size.height);
        [UIView commitAnimations];
        self.bannerIsVisible = NO;
        
    } else {
        
        if ([ADBannerView instancesRespondToSelector:@selector(initWithAdType:)]) {
            self.bannerView = [[ADBannerView alloc] initWithAdType:ADAdTypeBanner];
        } else {
            self.bannerView = [[ADBannerView alloc] init];
        }
        self.bannerIsVisible = YES;
    }
}

-(void)createADBanner
{
    if (self.bannerIsVisible) {

        int randNum = rand( ) % 3;
        
        if (randNum == 1 || randNum == 2 ) {

            [self.webView addSubview:self.bannerView];
            
        } else {
            self.bannerIsVisible = NO;
            self.bannerView = nil;
        }
    }
}

- (void)layoutAnimated:(BOOL)animated {
    
    if (!self.bannerIsVisible) {
        return;
    }
    
    CGRect contentFrame = self.view.bounds;
    CGSize sizenew = [self.bannerView sizeThatFits:self.view.bounds.size];
    
    CGRect bannerFrame = self.bannerView.frame;
    
    self.bannerView.frame = CGRectMake(0, self.view.bounds.size.height, sizenew.width, sizenew.height);
    
    if (self.bannerView.bannerLoaded) {
        contentFrame.size.height -= self.bannerView.frame.size.height;
        bannerFrame.origin.y = contentFrame.size.height;
        
    } else {
        bannerFrame.origin.y = contentFrame.size.height;
    }
    
    [UIView animateWithDuration:animated ? 0.55 : 0.0 animations:^{
        self.bannerView.frame = bannerFrame;
    }];
}

- (void)bannerViewDidLoadAd:(ADBannerView *)banner {
    if (self.bannerIsVisible) {
        [self layoutAnimated:YES];
    }
}

- (void)bannerView:(ADBannerView *)banner didFailToReceiveAdWithError:(NSError *)error {
    
    if (self.bannerIsVisible) {
        [self layoutAnimated:YES];
    }
}

- (BOOL)bannerViewActionShouldBegin:(ADBannerView *)banner willLeaveApplication:(BOOL)willLeave {
    return YES;
}

- (void)bannerViewActionDidFinish:(ADBannerView *)banner {
}


@end
