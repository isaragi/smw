//
//  TMCellManege.m
//  TheMatome
//
//  Created by Atsushi Igarashi on 2013/12/24.
//  Copyright (c) 2013年 Atsushi Igarashi. All rights reserved.
//

#import "SMWAdvancedOpCellManeger.h"

@implementation TMAdvancedOpSectionItem

@end

@interface SMWAdvancedOpCellManeger ()

@property (nonatomic, strong) NSArray *sectionHeaderNameArray;
@property (nonatomic, strong) NSArray *sectionFooterNameArray;
@property (nonatomic, strong) NSArray *sectionFooterViewArray;
@property (nonatomic, strong) NSArray *cellLabelTextArray;

@end



@implementation SMWAdvancedOpCellManeger

- (id)init
{
    self = [super init];
    if (self) {
        
        [self data];
        _sectionData = [NSMutableArray new];
        
        for (int i = 0; i < [self sectionCount]; i++ ) {
            
            _sectionItem = [TMAdvancedOpSectionItem new];
            _sectionItem.headerName = _sectionHeaderNameArray[i];
            _sectionItem.footerView = _sectionFooterViewArray[i];
            _sectionItem.footerName = _sectionFooterNameArray[i];
            _sectionItem.cellLabelText = _cellLabelTextArray[i];
            
            [_sectionData addObject:_sectionItem];
        }
    
    }
    return self;
}

- (void)data
{
    _sectionHeaderNameArray = @[@"設定"];
    
    NSArray *section0Text = @[@"シェイクジェスチャー",
                              @"アプリ復帰後の自動更新",
                              @"閲覧履歴を取得",
                              @"記事の検索",
                              @"Twitterに投稿",
                              @"Lineに投稿",
                              @"Facebookに投稿",
                              @"Pocketに送信"];
    
    _cellLabelTextArray = @[section0Text];
    
    _sectionFooterNameArray = @[@"スイッチのOn/Offで必要な機能の追加をカスタマイズしましょう。"];
    
    _sectionFooterViewArray = nil;
    
}

- (NSInteger)sectionCount
{
    NSInteger headerCount = [self.sectionHeaderNameArray count];
    NSInteger footerViewCount = [self.sectionFooterViewArray count];
    NSInteger footerTitleCount = [self.sectionFooterNameArray count];
    
    NSInteger max = MAX(headerCount,footerViewCount);
    return MAX(max, footerTitleCount);
}

- (NSInteger)rowCountWithSectionNumber:(NSInteger)num
{
    
    if ([_cellLabelTextArray[num]  isKindOfClass:[NSString class]]) {
        
        return 1;
    }
    
    if ([_cellLabelTextArray[num] isKindOfClass:[NSArray class]]) {
        return  [_cellLabelTextArray[num] count];
        
    }
    return 0;
}

@end
