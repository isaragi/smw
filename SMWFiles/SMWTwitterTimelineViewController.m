//
//  AIWebViewController.m
//  
//
//  Created by Atsushi Igarashi on 13/09/08.
//  Copyright (c) 2013年 Atsushi Igarashi. All rights reserved.
//

#import "SMWTwitterTimelineViewController.h"
#import "SMWNavBarTitleLabel.h"
#import "SMWChangeColor.h"

@interface SMWTwitterTimelineViewController ()
@property (nonatomic,retain) IBOutlet UIWebView *twitterWebView;
@end

@implementation SMWTwitterTimelineViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	[self contentInset];
    _twitterWebView.delegate = self;
    [[SMWNavBarTitleLabell new] costomTitleLabelWithTitle:@"twitter" andNavigation:self.navigationItem];
    NSURL *url = [NSURL URLWithString:@"http://49.212.162.23/news-app/twitter-timeline.html"];
    NSURLRequest *req = [NSURLRequest requestWithURL:url];
    [_twitterWebView loadRequest:req];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)webViewDidStartLoad:(UIWebView*)wv
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
}

-(void)webViewDidFinishLoad:(UIWebView*)wv
{
    [self updateBackButton];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}

- (void)webView:(UIWebView*)webView didFailLoadWithError:(NSError*)error
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}

- (void)updateBackButton
{
    if ([self.twitterWebView canGoBack]) {
        if (!self.navigationItem.leftBarButtonItem) {
            self.navigationItem.leftItemsSupplementBackButton = YES;
            
            UIBarButtonItem *backItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:105 target:self action:@selector(backWasClicked:)];
            
            NSArray *ar = [NSArray arrayWithObjects:backItem, nil];
            [self.navigationItem setLeftBarButtonItems:ar animated:YES];
        }
    }
    else {
        [self.navigationItem setHidesBackButton:NO animated:YES];
    }
}

- (void)backWasClicked:(id)sender
{
    if ([self.twitterWebView canGoBack]) {
        [self.twitterWebView goBack];
    }
}

-(void)contentInset
{
    float iOSVersion = [[[UIDevice currentDevice] systemVersion] floatValue];
    if(iOSVersion >= 7.0)
    {
        return;
    }
    BOOL isTranslucentColor = [SMWChangeColor isTranslucentColor];
    
    if ( isTranslucentColor) {
        [[_twitterWebView scrollView] setContentInset:UIEdgeInsetsMake(44, 0, 0, 0)];
    }
}

@end
