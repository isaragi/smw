//
//  SMWDataCache.m
//  TheMatome
//
//  Created by Atsushi Igarashi on 13/07/21.
//  Copyright (c) 2013年 Atsushi Igarashi. All rights reserved.
//

#import "SMWDataCache.h"
#import "ChannelName.h"
#import "FeedCategory.h"

@interface CacheNode : NSObject

@property (nonatomic, strong) NSManagedObjectID *objectID;
@property (nonatomic, assign) NSUInteger accessCounter;

@end

@implementation CacheNode

@end

@interface SMWDataCache ()

@property (nonatomic, strong) NSMutableDictionary *cache;
@property (nonatomic, strong) NSEntityDescription *categoryEntityDescription;
@property (nonatomic, strong) NSEntityDescription *channelEntityDescription;
@property (nonatomic, assign) NSUInteger cacheSize;
@property (nonatomic, assign) NSUInteger accessCounter;
@property (nonatomic, strong) NSPredicate *categoryNamePredicateTemplate;


/*
 * performance check
 *
@property (nonatomic, assign) CGFloat totalCacheHitCost;
@property (nonatomic, assign) CGFloat totalCacheMissCost;
@property (nonatomic, assign) NSUInteger cacheHitCount;
@property (nonatomic, assign) NSUInteger cacheMissCount;
*/
@end
@implementation SMWDataCache

- (id)init {
    self = [super init];
    if (self != nil) {
        _cacheSize =30;
        _accessCounter = 0;
        _cache = [[NSMutableDictionary alloc] init];
    }
    return self;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    _categoryEntityDescription = nil;
    _channelEntityDescription = nil;
    //if (_cacheHitCount > 0) NSLog(@"average cache hit cost:  %f", _totalCacheHitCost/_cacheHitCount);
    //if (_cacheMissCount > 0) NSLog(@"average cache miss cost: %f", _totalCacheMissCost/_cacheMissCount);
}

- (void)setManagedObjectContext:(NSManagedObjectContext *)aContext {
    if (_managedObjectContext) {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:NSManagedObjectContextDidSaveNotification object:_managedObjectContext];
    }
    _managedObjectContext = aContext;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(managedObjectContextDidSave:) name:NSManagedObjectContextDidSaveNotification object:_managedObjectContext];
}

- (void)managedObjectContextDidSave:(NSNotification *)notification {
    CacheNode *cacheNode = nil;
    NSMutableArray *keys = [NSMutableArray array];
    for (NSString *key in _cache) {
        cacheNode = [_cache objectForKey:key];
        if ([cacheNode.objectID isTemporaryID]) {
            [keys addObject:key];
        }
    }
    [_cache removeObjectsForKeys:keys];
}

- (NSEntityDescription *)channelEntityDescription {
    if (_channelEntityDescription == nil) {
        _channelEntityDescription = [NSEntityDescription entityForName:@"ChannelName"
                                                inManagedObjectContext:_managedObjectContext];
    }
    return _channelEntityDescription;
}

- (NSEntityDescription *)categoryEntityDescription {
    if (_categoryEntityDescription == nil) {
        _categoryEntityDescription = [NSEntityDescription entityForName:@"FeedCategory"
                                                 inManagedObjectContext:_managedObjectContext];
    }
    return _categoryEntityDescription;
}

static NSString * const kCategoryNameSubstitutionVariable = @"NAME";

- (NSPredicate *)categoryNamePredicateTemplate {
    
    if (_categoryNamePredicateTemplate == nil) {
        NSExpression *leftHand = [NSExpression expressionForKeyPath:@"name"];
        NSExpression *rightHand = [NSExpression expressionForVariable:kCategoryNameSubstitutionVariable];
        _categoryNamePredicateTemplate = [[NSComparisonPredicate alloc] initWithLeftExpression:leftHand rightExpression:rightHand modifier:NSDirectPredicateModifier type:NSLikePredicateOperatorType options:0];
    }
    return _categoryNamePredicateTemplate;
}

#define USE_CACHING

- (ChannelName *)channelWithName:(NSString *)name {

    //NSTimeInterval before = [NSDate timeIntervalSinceReferenceDate];
#ifdef USE_CACHING

    CacheNode *cacheNode = [_cache objectForKey:name];
    if (cacheNode != nil) {
        
        cacheNode.accessCounter = _accessCounter++;
        ChannelName *channelName = (ChannelName *)[_managedObjectContext objectWithID:cacheNode.objectID];
        //_totalCacheHitCost += ([NSDate timeIntervalSinceReferenceDate] - before);
        //_cacheHitCount++;
        return channelName;
    }
#endif

    // cache missed, fetch from store - if not found in store there is no category object for the name and we must create one
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity:self.channelEntityDescription];
    NSPredicate *predicate = [self.categoryNamePredicateTemplate predicateWithSubstitutionVariables:[NSDictionary dictionaryWithObject:name forKey:kCategoryNameSubstitutionVariable]];
    [fetchRequest setPredicate:predicate];
    NSError *error = nil;
    NSArray *fetchResults = [_managedObjectContext executeFetchRequest:fetchRequest error:&error];
    NSAssert1(fetchResults != nil, @"Unhandled error executing fetch request in import thread: %@", [error localizedDescription]);
    
    ChannelName *channelName = nil;
    if ([fetchResults count] > 0) {
        // get category from fetch
        channelName = [fetchResults objectAtIndex:0];
    } else if ([fetchResults count] == 0) {
        // category not in store, must create a new category object
        channelName = [[ChannelName alloc] initWithEntity:self.channelEntityDescription insertIntoManagedObjectContext:_managedObjectContext];
        channelName.name = name;
    }
    
#ifdef USE_CACHING

    if ([_cache count] >= _cacheSize) {
        
        NSUInteger oldestAccessCount = UINT_MAX;
        NSString *key = nil, *keyOfOldestCacheNode = nil;
        for (key in _cache) {
            CacheNode *tmpNode = [_cache objectForKey:key];
            if (tmpNode.accessCounter < oldestAccessCount) {
                oldestAccessCount = tmpNode.accessCounter;
                keyOfOldestCacheNode = key;
            }
        }
        cacheNode = [_cache objectForKey:keyOfOldestCacheNode];
        [_cache removeObjectForKey:keyOfOldestCacheNode];
    } else {
        
        cacheNode = [[CacheNode alloc] init];
    }
    cacheNode.objectID = [channelName objectID];
    cacheNode.accessCounter = _accessCounter++;
    [_cache setObject:cacheNode forKey:name];
#endif
    //_totalCacheMissCost += ([NSDate timeIntervalSinceReferenceDate] - before);
    //_cacheMissCount++;
    return channelName;
}



#define USE_CACHING

- (FeedCategory *)categoryWithName:(NSString *)name {
    
    //NSTimeInterval before = [NSDate timeIntervalSinceReferenceDate];
#ifdef USE_CACHING
    
    CacheNode *cacheNode = [_cache objectForKey:name];
    if (cacheNode != nil) {
        
        cacheNode.accessCounter = _accessCounter++;
        FeedCategory *categoryName = (FeedCategory *)[_managedObjectContext objectWithID:cacheNode.objectID];
        //_totalCacheHitCost += ([NSDate timeIntervalSinceReferenceDate] - before);
        //_cacheHitCount++;
        return categoryName;
    }
#endif
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity:self.categoryEntityDescription];
    NSPredicate *predicate = [self.categoryNamePredicateTemplate predicateWithSubstitutionVariables:[NSDictionary dictionaryWithObject:name forKey:kCategoryNameSubstitutionVariable]];
    [fetchRequest setPredicate:predicate];
    NSError *error = nil;
    NSArray *fetchResults = [_managedObjectContext executeFetchRequest:fetchRequest error:&error];
    NSAssert1(fetchResults != nil, @"Unhandled error executing fetch request in import thread: %@", [error localizedDescription]);
    
    FeedCategory *categoryName = nil;
    if ([fetchResults count] > 0) {
        // get category from fetch
        categoryName = [fetchResults objectAtIndex:0];
    } else if ([fetchResults count] == 0) {
        // category not in store, must create a new category object
        categoryName = [[FeedCategory alloc] initWithEntity:self.categoryEntityDescription insertIntoManagedObjectContext:_managedObjectContext];
        categoryName.name = name;
    }
    
#ifdef USE_CACHING
    
    if ([_cache count] >= _cacheSize) {
        
        NSUInteger oldestAccessCount = UINT_MAX;
        NSString *key = nil, *keyOfOldestCacheNode = nil;
        for (key in _cache) {
            CacheNode *tmpNode = [_cache objectForKey:key];
            if (tmpNode.accessCounter < oldestAccessCount) {
                oldestAccessCount = tmpNode.accessCounter;
                keyOfOldestCacheNode = key;
            }
        }
        
        cacheNode = [_cache objectForKey:keyOfOldestCacheNode];
        [_cache removeObjectForKey:keyOfOldestCacheNode];
    } else {

        cacheNode = [[CacheNode alloc] init];
    }
    cacheNode.objectID = [categoryName objectID];
    cacheNode.accessCounter = _accessCounter++;
    [_cache setObject:cacheNode forKey:name];
#endif
    //_totalCacheMissCost += ([NSDate timeIntervalSinceReferenceDate] - before);
    //_cacheMissCount++;
    return categoryName;
}
@end
