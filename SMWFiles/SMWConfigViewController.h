//
//  TMTestViewController.h
//  TMConfigViewController
//
//  Created by Atsushi Igarashi on 13/07/21.
//  Copyright (c) 2013年 Atsushi Igarashi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TMConfigViewController : UIViewController <UITableViewDelegate,UITableViewDataSource>
@end