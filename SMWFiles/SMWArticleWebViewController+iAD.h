//
//  SMWArticleWebViewController+iAD.h
//  TheMatome
//
//  Created by Atsushi Igarashi on 2013/12/18.
//  Copyright (c) 2013年 Atsushi Igarashi. All rights reserved.
//

#import "SMWArticleWebViewController.h"

@interface SMWArticleWebViewController (iAD)

-(void)chackUpRate;
-(void)createADBanner;

@end
