//
//  SMWArticleWebViewController.m
//  TheMatome
//
//  Created by Atsushi Igarashi on 13/07/19.
//  Copyright (c) 2013年 Atsushi Igarashi. All rights reserved.
//

#import "SMWArticleWebViewController+Social.h"
#import "SMWArticleWebViewController+SendMail.h"
#import "SMWArticleWebViewController+HideNaviBar.h"
#import "SMWArticleWebViewController+iAD.h"
#import <AudioToolbox/AudioServices.h>
#import "SMWChangeColor.h"
#import "NJKWebViewProgress.h"
#import "SMWImageViewController.h"

@interface SMWArticleWebViewController () <NJKWebViewProgressDelegate>

- (IBAction)webActionBtnCliked:(id)sender;

@property (nonatomic, strong) NSMutableArray *bookmarkData;
@property (nonatomic, assign) BOOL isScrollingView;
@property (nonatomic, assign) BOOL finishLoad;
@property (nonatomic, strong) NJKWebViewProgress *progressProxy;
@property (nonatomic, assign) int errorCount;

@end

@implementation SMWArticleWebViewController

NSInteger const kDeleteBookmarkAlertTag = 1;
NSInteger const kSendEMailAlertTag = 2;
NSInteger const kSendPocketAlertTag = 3;
static NSInteger const kErrorCountMaxNum = 1000;

- (BOOL)canBecomeFirstResponder {
    return YES;
}

-(void)awakeFromNib
{
    [self chackUpRate];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self openWebData];
    self.bannerView.delegate = self;
    [self createADBanner];
    _isTranslucentColor = [SMWChangeColor isTranslucentColor];
    _iOSVersion = [[[UIDevice currentDevice] systemVersion] floatValue];
    _themeColor = [[[NSUserDefaults standardUserDefaults] stringForKey:@"themeColor"] copy];
    [self loadFirstPage];
    [self createProgressView];
    _finishLoad = NO;
    _isNavBarHidden = NO;
    _webView.scrollView.delegate = self;
    _webView.dataDetectorTypes = UIDataDetectorTypeNone;
    _webView.backgroundColor = [UIColor colorWithWhite:0.9 alpha:1.0];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self resetScrollOffsetAndInset];
}

-(void)openWebData
{
    _urlStr = [self.webData objectForKey:@"url"];
    _channelTitleStr = [self.webData objectForKey:@"channelTitle"];
    _titleStr = [self.webData objectForKey:@"title"];
}

-(void)loadFirstPage
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    [_webView setDelegate:self];
    _webView.delegate = _progressProxy;
    _webView.scalesPageToFit = YES;
    NSURL *url = [NSURL URLWithString:self.urlStr];
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    [_webView loadRequest:requestObj];
    _errorCount = 0;
}

-(void) viewWillDisappear:(BOOL)animated {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    _progressView.hidden = YES;
    [super viewWillDisappear:animated];
}

-(void) viewDidDisappear:(BOOL)animated {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    [_webView stopLoading];
    [super viewDidDisappear:animated];
}

- (void)dealloc
{
    self.navigationItem.leftBarButtonItem = nil;
    self.navigationItem.rightBarButtonItem = nil;
    _webView = nil;
    [_webView setDelegate:nil];
    _progressView = nil;
    _progressProxy = nil;
    _bannerView = nil;
    [_webView removeFromSuperview];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
}

-(void)webViewDidStartLoad:(UIWebView *)webView
{
    [self updateBackButton];
    _errorCount = 0;
}

-(void)webViewDidFinishLoad:(UIWebView *)webView
{
    _finishLoad = YES;
    [self updateBackButton];
}

-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{    
    if ([error code] != NSURLErrorCancelled) {
        _errorCount++;
        
        if ( _errorCount <= kErrorCountMaxNum ) {
            [SVProgressHUD showErrorWithStatus:@"電波状況が悪いようです..."];
            _errorCount = 0;
        }
    }
    
}

//--------------------------------------------------------------//
#pragma mark canGoBack
//--------------------------------------------------------------//
- (void)updateBackButton {
    if ([_webView canGoBack]) {
        if (!self.navigationItem.leftBarButtonItem) {
            self.navigationItem.leftItemsSupplementBackButton = YES;
            
            UIBarButtonItem *backItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:105 target:self action:@selector(backWasClicked:)];

            NSArray *ar = [NSArray arrayWithObjects:backItem, nil];
            [self.navigationItem setLeftBarButtonItems:ar animated:YES];
        }
    }
    else {
        [self.navigationItem setLeftBarButtonItem:nil animated:YES];
        [self.navigationItem setHidesBackButton:NO animated:YES];
    }
}

- (void)backWasClicked:(id)sender {
    if ([_webView canGoBack]) {
        _isScrollingView = NO;
        [_webView goBack];
    }
}



//--------------------------------------------------------------//
#pragma Menu Action
//--------------------------------------------------------------//

- (IBAction)webActionBtnCliked:(id)sender {
    
    UIActionSheet *mySheet = [[UIActionSheet alloc] initWithTitle:nil
                                                         delegate: self
                                                cancelButtonTitle: nil
                                           destructiveButtonTitle: nil
                                                otherButtonTitles: nil];
    
    for (NSString* buttonTitle in [self addSocialBtn]) {
        [mySheet addButtonWithTitle:buttonTitle];
    }

    if(_iOSVersion >= 7.0)
    {
        mySheet.destructiveButtonIndex = 4;
    }
    mySheet.cancelButtonIndex = [mySheet addButtonWithTitle:@"キャンセル"];
    [mySheet showInView:self.view];
}

- (NSArray*)addSocialBtn
{
    NSMutableArray *ar = [NSMutableArray arrayWithObjects:@"Safariで開く",@"Chrome for iOSで開く",@"お気に入りに追加",@"リロード",@"問題を報告", nil];
    BOOL switchValue = [[NSUserDefaults standardUserDefaults] boolForKey:@"SendTwitterSwitchValue"];
    if (switchValue) {
        [ar addObject:@"Twitterに投稿"];
    }
    switchValue = [[NSUserDefaults standardUserDefaults] boolForKey:@"SendFacebookSwitchValue"];
    if (switchValue) {
        [ar addObject:@"Facebookに投稿"];
    }
    switchValue = [[NSUserDefaults standardUserDefaults] boolForKey:@"SendLineSwitchValue"];
    if (switchValue) {
        [ar addObject:@"Lineに投稿"];
    }
    switchValue = [[NSUserDefaults standardUserDefaults] boolForKey:@"SendPocketSwitchValue"];
    if (switchValue) {
        [ar addObject:@"Pocketに送信"];
    }
    
    return ar;
}

-(void)actionSheet:(UIActionSheet*)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    int arCount = (int)[[self addSocialBtn] count];
    
    switch (buttonIndex) {
            
        case 0:
            [self openSafari];
            break;
            
        case 1:
            [self openChrome];
            break;
            
        case 2:
            [self bookmark];
            break;
            
        case 3:
            [self reloadView];
            break;
            
        case 4:
            [self sendMailAlert];
            break;
            
        case 5:
            if (!(arCount == 5)) {
                [self postSocial3];
            }
            break;

        case 6:
            if (!(arCount == 6)) {
               [self postSocial2];
            }
            break;
            
        case 7:
            if (!(arCount == 7)) {
                [self postSocial1];
            }
            break;
            
        case 8:
            if (!(arCount == 8)) {
                [self savePocket];
            }
            break;
    }
}

-(void)openSafari
{
    NSURL *url = [NSURL URLWithString:_urlStr];
    [[UIApplication sharedApplication] openURL:url];
}

-(void)openChrome
{
    NSURL *inputURL = [NSURL URLWithString:_urlStr];
    NSString *scheme = inputURL.scheme;
    
    // Replace the URL Scheme with the Chrome equivalent.
    NSString *chromeScheme = nil;
    if ([scheme isEqualToString:@"http"]) {
        chromeScheme = @"googlechrome";
    } else if ([scheme isEqualToString:@"https"]) {
        chromeScheme = @"googlechromes";
    }
    
    // Proceed only if a valid Google Chrome URI Scheme is available.
    if (chromeScheme) {
        NSString *absoluteString = [inputURL absoluteString];
        NSRange rangeForScheme = [absoluteString rangeOfString:@":"];
        NSString *urlNoScheme =
        [absoluteString substringFromIndex:rangeForScheme.location];
        NSString *chromeURLString =
        [chromeScheme stringByAppendingString:urlNoScheme];
        NSURL *chromeURL = [NSURL URLWithString:chromeURLString];
        
        // Open the URL with Chrome.
        [[UIApplication sharedApplication] openURL:chromeURL];
    }
}

- (void)bookmark
{
    _bookmarkData = [[[NSUserDefaults standardUserDefaults] arrayForKey:@"bookmarks"] mutableCopy];
    if (!_bookmarkData) {
        _bookmarkData = [[NSMutableArray alloc] init];
    }
    
    BOOL isObjectContain = [_bookmarkData containsObject:_webData];
    if (isObjectContain) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"既にお気に入りに登録しています\n削除しますか？" delegate:self cancelButtonTitle:@"キャンセル" otherButtonTitles:@"削除",nil];
        alert.tag = kDeleteBookmarkAlertTag;
        [alert show];
        
    } else {
        
        [_bookmarkData insertObject:_webData atIndex:0];
        
        if (_bookmarkData.count >= 201) {
            [_bookmarkData removeLastObject];
        }
        NSString *st =[NSString stringWithFormat:@"登録しました☻\n%lu/200",(unsigned long)_bookmarkData.count];
        [SVProgressHUD showSuccessWithStatus:st];
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject:_bookmarkData forKey:@"bookmarks"];
    }
}

- (void)deleteBookmark {
    [_bookmarkData removeObject:_webData];
    [[NSUserDefaults standardUserDefaults] setObject:_bookmarkData forKey:@"bookmarks"];
    [SVProgressHUD showSuccessWithStatus:@"削除しました☻"];
}

-(void)reloadView
{
    if ( _webView ) {
        
        _isScrollingView = NO;
        _progressView.hidden = NO;
        if ([_webView canGoBack]) {
            
            [_webView reload];
            
        } else {
            
            [self loadFirstPage];
        }
    }
}

-(void)alertView:(UIAlertView*)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if(alertView.tag == kDeleteBookmarkAlertTag){
        switch (buttonIndex) {

            case 1:
                [self deleteBookmark];
                break;
        }
    }
    if(alertView.tag == kSendEMailAlertTag){
        switch (buttonIndex) {

            case 1:
                [self sendEMail];
                break;
        }
    }
    if(alertView.tag == kSendPocketAlertTag){
        switch (buttonIndex) {
                
            case 1:
                [self goPocket];
                break;
        }
    }
}



//--------------------------------------------------------------//
#pragma ShakeGesture & autoScroller
//--------------------------------------------------------------//

- (void)motionBegan:(UIEventSubtype)motion withEvent:(UIEvent *)event
{
    if (!_finishLoad) {
        [SVProgressHUD showErrorWithStatus:@"読み込み中..."];
        return;
    }
    AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
    if (_isScrollingView) {
        [_webView.layer removeAllAnimations];
        _isScrollingView = NO;
        return;
    } else{
        _isScrollingView = YES;
        [self autoScroller];
    }
}

- (void)autoScroller{

    NSString *currPageOffset = [_webView stringByEvaluatingJavaScriptFromString:@"window.scrollY;"];
    int height = [self getPageHeight];
    if (height==0) {
        return;
    }
    int currentPageMaxScroll = height - self.view.frame.size.height;
    int currentPageOffset = [currPageOffset intValue];
    
    int targetOffset;
    if (currentPageOffset < currentPageMaxScroll) { targetOffset = currentPageOffset+50;
        if (targetOffset > currentPageMaxScroll){
            targetOffset = currentPageMaxScroll;
        }
    }else{
        _isScrollingView = NO;
        return;
    }
   
    NSString *jsCommand = [NSString stringWithFormat:@"window.scrollTo(0,%d);", targetOffset];

    [UIView beginAnimations:@"scrollPage" context:nil]; {
        [UIView setAnimationCurve:UIViewAnimationCurveLinear];
        [UIView setAnimationDuration:[self getSliderValue]];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationDidStopSelector:@selector(judgeAnimation)];
        [_webView stringByEvaluatingJavaScriptFromString:jsCommand];
    }
    [UIView commitAnimations];
}
-(void)judgeAnimation
{
    if (_isScrollingView) {
        [self autoScroller];
        
    } else {
        [_webView.layer removeAllAnimations];
    }
}

- (int)getPageHeight
{
    CGSize size = _webView.scrollView.contentSize;
    return size.height;
}

-(float)getSliderValue
{
   float scrollSpeadValue = [[NSUserDefaults standardUserDefaults] floatForKey:@"scrollSpeadValue"];
    if (!scrollSpeadValue) {
        return 0.3;
    } else {
        return scrollSpeadValue;
    }
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    if (navigationType == UIWebViewNavigationTypeLinkClicked) {
        
        if (_isScrollingView) {
            
            [_webView.layer removeAllAnimations];
            _isScrollingView = NO;
        }
        
        if ([self isImageURL:[request URL]]) {
            
            [self openImageViewWith:[request URL]];
            return NO;
        }
        
    }
    return YES;
}


//--------------------------------------------------------------//
#pragma mark - Open Image View
//--------------------------------------------------------------//

- (void)openImageViewWith:(NSURL*)url
{
    SMWImageViewController *imageVC = [self.storyboard instantiateViewControllerWithIdentifier:@"webViewModal"];
    imageVC.imageURL = url;
    [self presentViewController:imageVC animated:YES completion:nil];
}

- (BOOL)isImageURL:(NSURL *)url
{
    NSString *urlString = [url lastPathComponent];
    
    NSRange rangeJpg = [urlString rangeOfString:@".jpg"];
    NSRange rangeJpeg = [urlString rangeOfString:@".jpeg"];
    NSRange rangePng = [urlString rangeOfString:@".png"];
    NSRange rangeGif = [urlString rangeOfString:@".gif"];
    
    if (rangeJpg.location != NSNotFound || rangeJpeg.location != NSNotFound ||
        rangePng.location != NSNotFound || rangeGif.location != NSNotFound) {
        
        return YES;
    } else {
        return NO;
    }
}

//--------------------------------------------------------------//
#pragma mark - NJKWebViewProgressDelegate
//--------------------------------------------------------------//

- (void)createProgressView
{

    _progressProxy = [[NJKWebViewProgress alloc] init];
    _webView.delegate = _progressProxy;
    _progressProxy.webViewProxyDelegate = self;
    _progressProxy.progressDelegate = self;

    int progHeight = (_iOSVersion >= 7.0) ? 2 : 0;
    CGRect naviSize = self.navigationController.navigationBar.bounds;
    if (!_progressView) _progressView = [[UIProgressView alloc] initWithFrame:CGRectMake(0, naviSize.size.height - progHeight, naviSize.size.width, 0)];
    _progressView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    [_progressView setProgressViewStyle:UIProgressViewStyleBar];
    [_progressView setProgress:0];
    [self.navigationController.navigationBar addSubview:_progressView];
}

-(void)webViewProgress:(NJKWebViewProgress *)webViewProgress updateProgress:(float)progress
{
    
    if (progress == 0.0) {
        _progressView.progress = 0;
        [UIView animateWithDuration:0.27 animations:^{
            _progressView.alpha = 1.0;
        }];
    }
    if (progress == 1.0) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        [UIView animateWithDuration:0.27 delay:progress - _progressView.progress options:0 animations:^{
            _progressView.alpha = 0.0;
        } completion:nil];
    }
    
    [_progressView setProgress:progress animated:NO];
}



//--------------------------------------------------------------//
#pragma mark - View Offset
//--------------------------------------------------------------//
- (void)resetScrollOffsetAndInset {
    
    if (_isTranslucentColor == NO || _themeColor == nil ) {
        
        _webView.frame = CGRectMake(0, 0, _webView.frame.size.width, self.view.frame.size.height);
    }
    
    int iPhone3_5inchWebViewHeight = 416;
    int iPhone3_5inchTransparentWebViewHeight = 480;
    int iPhone3_5inchTransparentWebViewHeightiOS6 = 460;
    int myHeight = self.view.bounds.size.height;
    if (myHeight == iPhone3_5inchWebViewHeight || myHeight == iPhone3_5inchTransparentWebViewHeight
        || myHeight == iPhone3_5inchTransparentWebViewHeightiOS6) {
        
        _webView.frame = CGRectMake(0, 0, _webView.frame.size.width, self.view.frame.size.height);
    }
}
@end

