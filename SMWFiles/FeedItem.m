//
//  FeedItem.m
//  TheMatome
//
//  Created by Atsushi Igarashi on 2014/02/16.
//  Copyright (c) 2014年 Atsushi Igarashi. All rights reserved.
//

#import "FeedItem.h"
#import "ChannelName.h"
#import "FeedCategory.h"


@implementation FeedItem

@dynamic date;
@dynamic hatebu;
@dynamic rank;
@dynamic title;
@dynamic tweet;
@dynamic url;
@dynamic isOnlyFavorMatome;
@dynamic channelName;
@dynamic feedCategory;

@end
