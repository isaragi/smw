//
//  SMWUserFeedList.m
//  TheMatome
//
//  Created by Atsushi Igarashi on 2014/02/05.
//  Copyright (c) 2014年 Atsushi Igarashi. All rights reserved.
//

#import "SMWUserFeedList.h"
@interface SMWUserFeedList ()
@property (nonatomic, strong)NSMutableArray *userReadingURL;
@property (nonatomic, strong)NSMutableArray *userReadingKey;
@end

@implementation SMWUserFeedList
static NSInteger const fovormatome = 8;

- (id)init
{
    self = [super init];
    if (self) {
        NSInteger count = 0;
        _userReadingURL = [NSMutableArray arrayWithCapacity:self.defaultURLList.count];
        _userReadingKey = [NSMutableArray arrayWithCapacity:self.defaultKeyList.count];
        for (NSInteger i = 0; [[self userSwitchKey] count] > i; i++) {
            
            BOOL switchValue = [[NSUserDefaults standardUserDefaults] boolForKey:self.userSwitchKey[i]];
            if (switchValue) {
                
                if (i != fovormatome) {
                    count++;
                    [_userReadingURL addObject:self.defaultURLList[i]];
                    [_userReadingKey addObject:self.defaultKeyList[i]];
                    
                } else {
                    
                    _isFavorMatomeFeed = YES;
                    _userFeedCount = count;
                    
                    for (NSInteger n = 0; [[self rankingURLLists] count] > n; n++) {
                        [_userReadingURL addObject:self.rankingURLLists[n]];
                        [_userReadingKey addObject:self.matomeCategoryNameAry[n]];
                    }
                }
            }
        }
        [self insertUserTabName];
    }
    return self;
}


-(NSArray*)userSwitchKey {
    
    return @[@"kTopSwitch",
             @"kEntertainmentSwitch",
             @"kPopularSwitch",
             @"kSportsSwitch",
             @"kITSwitch",
             @"kBusinessSwitch",
             @"kWorldSwitch",
             @"kMatomeSwitch",
             @"kFavorMatomeSwitch"];
}

-(NSArray *)defaultURLList {
    
   return @[@"http://49.212.162.23/cron/news/top.rdf",
            @"http://49.212.162.23/cron/news/entertainment.rdf",
            @"http://49.212.162.23/cron/news/popular.rdf",
            @"http://49.212.162.23/cron/news/sports.rdf",
            @"http://49.212.162.23/cron/news/it.rdf",
            @"http://49.212.162.23/cron/news/business.rdf",
            @"http://49.212.162.23/cron/news/world.rdf",
            @"http://49.212.162.23/cron/index.rdf"];
}

- (NSArray *)rankingURLLists {
    
    return @[@"http://49.212.162.23/cron/one_days_rank_rss.rdf",
             @"http://49.212.162.23/cron/three_days_rank_rss.rdf",
             @"http://49.212.162.23/cron/one_weeks_rank_rss.rdf",
             ];
}

-(NSArray *)defaultKeyList {
    
    return @[@"TopFeeds",
             @"EntertainmentFeeds",
             @"PopularFeeds",
             @"SportsFeeds",
             @"ITFeeds",
             @"BusinessFeeds",
             @"WorldFeeds",
             @"SortByDateMatomeFeeds"];
}

- (NSArray *)matomeCategoryNameAry {
    
    return @[@"SortByFavoriteOfOneDaysMatome",
             @"SortByFavoriteOfThreeDaysMatome",
             @"SortByFavoriteOfOneWeeksMatome"];
}

-(NSArray*)feedCategoryNameAry {
    
    return _userReadingKey;
}

- (NSArray *)readingLists {
    
    return _userReadingURL;
}

-(void)insertUserTabName {
    
    _userTabList = [NSMutableArray arrayWithCapacity:9];
    
    if ( [_userReadingKey containsObject:@"TopFeeds"]) {
        [_userTabList addObject:@"Top"];
    }
    if ( [_userReadingKey containsObject:@"EntertainmentFeeds"]) {
        [_userTabList addObject:@"エンタメ"];
    }
    if ( [_userReadingKey containsObject:@"PopularFeeds"]) {
        [_userTabList addObject:@"バラエティ"];
    }
    if ( [_userReadingKey containsObject:@"SportsFeeds"]) {
        [_userTabList addObject:@"スポーツ"];
    }
    if ( [_userReadingKey containsObject:@"ITFeeds"]) {
        [_userTabList addObject:@"IT"];
    }
    if ( [_userReadingKey containsObject:@"BusinessFeeds"]) {
        [_userTabList addObject:@"経済"];
    }
    if ( [_userReadingKey containsObject:@"WorldFeeds"]) {
        [_userTabList addObject:@"国際"];
    }
    if ( [_userReadingKey containsObject:@"SortByDateMatomeFeeds"]) {
        
        if (_userReadingKey.count == 4 && [self isAddedFavorMatome]) {
            [_userTabList addObject:@"新着"];
        } else {
            [_userTabList addObject:@"まとめ\n新着"];
        }
    }
    if ( [self isAddedFavorMatome]) {
        
        if (_userReadingKey.count == 4 && [_userReadingKey containsObject:@"SortByDateMatomeFeeds"]) {
            [_userTabList addObject:@"人気"];
        } else {
            [_userTabList addObject:@"まとめ\n人気"];
        }
    }
}

-(BOOL)isAddedFavorMatome {
    
    if ( [_userReadingKey containsObject:@"SortByFavoriteOfOneDaysMatome"] &&
        [_userReadingKey containsObject:@"SortByFavoriteOfThreeDaysMatome"] &&
        [_userReadingKey containsObject:@"SortByFavoriteOfOneWeeksMatome"]) {
        return YES;
    }
    return NO;
}

@end
