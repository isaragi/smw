    //
//  TMConfAboutViewController.m
//  TheMatome
//
//  Created by Atsushi Igarashi on 13/08/09.
//  Copyright (c) 2013年 Atsushi Igarashi. All rights reserved.
//

#import "SMWAboutViewController.h"
#import "Appirater.h"
#import "AAMFeedbackViewController.h"

#import "SMWNavBarTitleLabel.h"
#import "SMWAboutViewCellManeger.h"
#import "SVProgressHUD.h"
#import <AudioToolbox/AudioServices.h>

@interface SMWAboutViewController ()

@property (nonatomic, weak) IBOutlet UITableView *aboutTableView;
@property (nonatomic, strong) SMWAboutViewCellManeger *cellManeger;
@end

@implementation SMWAboutViewController

static NSString * const MAIL_ADRESS = @"sakutto.matome.yakko@gmail.com";

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[SMWNavBarTitleLabell new] costomTitleLabelWithTitle:@"About" andNavigation:self.navigationItem];
    //[self.navigationController setToolbarHidden:YES animated:YES];
    _cellManeger = [SMWAboutViewCellManeger new];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return  [_cellManeger sectionCount];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return  [_cellManeger rowCountWithSectionNumber:section];

}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {

    TMAboutViewSectionItem * items = _cellManeger.celldata[section];
    
    if ([items.headerName isEqualToString:@""]) {
        
        return nil;
    }
    
    return items.headerName;
}

- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section {
    
    TMAboutViewSectionItem * items = _cellManeger.celldata[section];
    
    if ( [items.footerName isEqualToString:@""]) {
        
        return nil;
    }
    
    return items.footerName;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    
    TMAboutViewSectionItem * items = _cellManeger.celldata[section];
    
    if ([items.footerView isEqual:(id)[NSNull null]]) {
        
        return nil;
    }
    
    return items.footerView;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *CellIdentifier = [NSString stringWithFormat:@"%lu:%lu", (unsigned long)[indexPath indexAtPosition:0],
                                (unsigned long)[indexPath indexAtPosition:1]];
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];

        TMAboutViewSectionItem * items = _cellManeger.celldata[indexPath.section];
        
        if ([items.cellLabelText isKindOfClass:[NSArray class]]) {
            
            cell.textLabel.text = items.cellLabelText[indexPath.row];
            
        } else {
         
            cell.textLabel.text = items.cellLabelText;
        }
      
    }
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.selectionStyle = UITableViewCellSelectionStyleBlue;
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if(indexPath.section == 0) {
            
        AAMFeedbackViewController *vc = [[AAMFeedbackViewController alloc]init];
        vc.toRecipients = [NSArray arrayWithObject:MAIL_ADRESS];
        vc.ccRecipients = nil;vc.bccRecipients = nil;
        UINavigationController *nvc = [[UINavigationController alloc]initWithRootViewController:vc];
        [self presentViewController:nvc animated:YES completion:nil];
        
    }
    else if (indexPath.section == 1) {
        
        if(indexPath.row == 0) {
            
            [self performSegueWithIdentifier:@"UsageHintCellPushID" sender:nil];
        }
        if(indexPath.row == 1) {
            
            [Appirater rateApp];
        }
    }
    else if (indexPath.section == 2) {
        
        if(indexPath.row == 0) {
            
            [self performSegueWithIdentifier:@"LicenseCellPush" sender:nil];
        }
        
        if(indexPath.row == 1) {
            
            [self performSegueWithIdentifier:@"UserPolicyCellPush" sender:nil];
        }
    }

}

-(void)specialThanks:(UIButton*)button{
    AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
    [SVProgressHUD showSuccessWithStatus:@"このアプリをダウンロードして頂き誠にありがとうございます。(*´･x･`)ゞ"];
}

@end
