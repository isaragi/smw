//
//  SMWNavBarTitleLabell.h
//  TheMatome
//
//  Created by Atsushi Igarashi.  on 2013/10/09.
//  Copyright (c) 2013年 Atsushi Igarashi. All rights reserved.
//

#import <Foundation/Foundation.h>

@class SMWArticleWebViewController;

@interface SMWNavBarTitleLabell : NSObject

- (void)costomTitleLabelWithTitle:(NSString *)str andNavigation:(UINavigationItem *)navi;

- (void)costomTitleString:(NSString*)str andWebView:(SMWArticleWebViewController*)webView labelSize:(int)size;

@end