//
//  SMWArticleWebViewController.h
//  TheMatome
//
//  Created by Atsushi Igarashi on 13/07/19.
//  Copyright (c) 2013年 Atsushi Igarashi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SVProgressHUD.h"
#import <iAd/iAd.h>

@interface SMWArticleWebViewController : UIViewController <UIWebViewDelegate, UIActionSheetDelegate, UIScrollViewDelegate, ADBannerViewDelegate>

extern NSInteger const kDeleteBookmarkAlertTag;
extern NSInteger const kSendEMailAlertTag;
extern NSInteger const kSendPocketAlertTag;

@property (nonatomic, weak) IBOutlet UIWebView *webView;
@property (nonatomic, strong, readonly) NSString *urlStr;
@property (nonatomic, strong, readonly) NSString *titleStr;
@property (nonatomic, strong, readonly) NSString *channelTitleStr;
@property (nonatomic, strong) NSDictionary *webData;
@property (nonatomic, strong) UIProgressView *progressView;
@property (nonatomic, strong) UIView *statusBarBackground;
@property (nonatomic, assign) CGFloat startContentOffset;
@property (nonatomic, assign) CGFloat lastContentOffset;
@property (nonatomic, assign) BOOL isNavBarHidden;
@property (nonatomic, strong) ADBannerView *bannerView;
@property (nonatomic, assign) BOOL bannerIsVisible;
@property (nonatomic, assign, readonly) float iOSVersion;
@property (nonatomic, assign, readonly) BOOL isTranslucentColor;
@property (nonatomic, strong, readonly) NSString *themeColor;

@end
