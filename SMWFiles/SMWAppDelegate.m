//
//  SMWAppDelegate.m
//  TheMatome
//
//  Created by Atsushi Igarashi on 13/07/19.
//  Copyright (c) 2013年 Atsushi Igarashi. All rights reserved.
//

#import "SMWAppDelegate.h"
#import "Appirater.h"
#import "SMWHomeViewController.h"
#import "SMWChannelViewController.h"
#import "SMWSearchBarViewController.h"
#import "PocketAPI.h"
#import "SMWBackgroundDownload.h"

@implementation SMWAppDelegate {
    SMWBackgroundDownload *_backgroundDownload;
}

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

static NSString *const kMyApp2chAppid = @"718067801";
static NSString *const kMyPocketAPIKey = @"41557-ea523da7ecfb2b638ff5d06a";

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    if ([application respondsToSelector:@selector(registerUserNotificationSettings:)]) {
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeAlert|UIUserNotificationTypeSound categories:nil];
        [application registerUserNotificationSettings:settings];
    }

    [[UIApplication sharedApplication] setMinimumBackgroundFetchInterval:UIApplicationBackgroundFetchIntervalMinimum];
    [self resetWithFlag:NO];
    [self setUserDefaults];
    [Appirater setAppId:kMyApp2chAppid];
    [Appirater appLaunched:YES];
    [[PocketAPI sharedAPI] setConsumerKey:kMyPocketAPIKey];
    return YES;
}

-(void)application:(UIApplication *)application performFetchWithCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler
{
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    [ud setBool:YES forKey:@"BackgroundFetching"];
    
    [ud synchronize];

    if (!_backgroundDownload) {
        _backgroundDownload = [SMWBackgroundDownload new];
    }
    _backgroundDownload.completionHandler = completionHandler;
    [_backgroundDownload fetch];
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    [Appirater appEnteredForeground:YES];
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Saves changes in the application's managed object context before the application terminates.
    [self saveContext];
}

-(BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication
        annotation:(id)annotation
{
    
    if([[PocketAPI sharedAPI] handleOpenURL:url]){
        return YES;
    }else{
        // if you handle your own custom url-schemes, do it here
        return NO;
    }
    
}

- (void)saveContext
{
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}
//--------------------------------------------------------------//
#pragma mark - Core Data stack
//--------------------------------------------------------------//

// Returns the path to the application's documents directory.
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

// Returns the managed object context for the application.
// If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
//
- (NSManagedObjectContext *)managedObjectContext {
	
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
	
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [NSManagedObjectContext new];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    
    // observe the ParseOperation's save operation with its managed object context
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(mergeChanges:)
                                                 name:NSManagedObjectContextDidSaveNotification
                                               object:nil];
    
    return _managedObjectContext;
}

// Returns the managed object model for the application.
// If the model doesn't already exist, it is created by merging all of the models found in the application bundle.
//
- (NSManagedObjectModel *)managedObjectModel {
	
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"FeedItems" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    
    return _managedObjectModel;
}

// Returns the persistent store coordinator for the application.
// If the coordinator doesn't already exist, it is created and the application's store added to it
//
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
	
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    // find the earthquake data in our Documents folder
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"FeedItems.sqlite"];
    
    NSError *error;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel: [self managedObjectModel]];
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate.
        // You should not use this function in a shipping application, although it may be useful
        // during development. If it is not possible to recover from the error, display an alert
        // panel that instructs the user to quit the application by pressing the Home button.
        //
        
        // Typical reasons for an error here include:
        // The persistent store is not accessible
        // The schema for the persistent store is incompatible with current managed object model
        // Check the error message to determine what the actual problem was.
        //
		NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
		abort();
    }
    
    return _persistentStoreCoordinator;
}

// this is called via observing "NSManagedObjectContextDidSaveNotification" from our APLParseOperation
- (void)mergeChanges:(NSNotification *)notification {
    
    if (notification.object != self.managedObjectContext) {
        [self performSelectorOnMainThread:@selector(updateMainContext:) withObject:notification waitUntilDone:NO];
    }
}

// merge changes to main context,fetchedRequestController will automatically monitor the changes and update tableview.
- (void)updateMainContext:(NSNotification *)notification {
    
    assert([NSThread isMainThread]);
    [self.managedObjectContext mergeChangesFromContextDidSaveNotification:notification];
}


//--------------------------------------------------------------//
#pragma mark - Set UserDefaults
//--------------------------------------------------------------//
- (void)resetWithFlag:(BOOL)flag {
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    [ud setBool:flag forKey:@"NowLoading"];
    [ud setBool:flag forKey:@"BackgroundFetching"];
    [ud synchronize];
}
-(void)setUserDefaults
{
    NSUserDefaults *userValue = [NSUserDefaults standardUserDefaults];
    BOOL done = [userValue boolForKey:@"UserInitialSetting"];
    if (done) {
        return;
    }
    
    [userValue setBool:YES forKey:@"AutoReloadSwitchValue"];
    [userValue setBool:YES forKey:@"CerrySoundSwitchValue"];
    
    [self setReadingList:userValue];
    [userValue setObject:[NSDate date] forKey:@"LastDeleteCache"];
    
    [userValue setBool:YES forKey:@"UserInitialSetting"];
    [userValue synchronize];
}

-(void)setReadingList:(NSUserDefaults*)user {
    
    [user setBool:NO forKey:@"kMatomeSwitch"];
    [user setBool:NO forKey:@"kFavorMatomeSwitch"];
    
    for (int i = 0; [[self userSwitchKey] count] > i; i++) {
        [user setBool:YES forKey:self.userSwitchKey[i]];
    }
}

-(NSArray*)userSwitchKey {
    return @[@"kTopSwitch",@"kEntertainmentSwitch",@"kPopularSwitch",@"kSportsSwitch",
             @"kITSwitch",@"kBusinessSwitch",@"kWorldSwitch"];
}

@end
