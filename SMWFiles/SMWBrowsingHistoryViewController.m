//
//  SMWBrowsingHistoryViewController.m
//  TheMatome
//
//  Created by Atsushi Igarashi on 2013/10/25.
//  Copyright (c) 2013年 Atsushi Igarashi. All rights reserved.
//

#import "SMWBrowsingHistoryViewController.h"
#import "SMWBrowsingHistoryCell.h"
#import "SMWArticleWebViewController.h"
#import "SVProgressHUD.h"
#import "SMWNavBarTitleLabel.h"
#import "SMWChangeColor.h"

@interface SMWBrowsingHistoryViewController ()

@property (nonatomic, retain) NSMutableArray *browsingHistoryData;
- (IBAction)allDelete:(id)sender;

@end

@implementation SMWBrowsingHistoryViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[SMWNavBarTitleLabell new] costomTitleLabelWithTitle:@"履歴一覧" andNavigation:self.navigationItem];
    //[self.navigationController setToolbarHidden:YES animated:YES];
    _browsingHistoryData = [[[NSUserDefaults standardUserDefaults] arrayForKey:@"BrowsingHistoryData"] mutableCopy];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


//--------------------------------------------------------------//
#pragma mark - Table view data source
//--------------------------------------------------------------//

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if ([_browsingHistoryData count] == 0) {
        [self performSelector:@selector(showAlert) withObject:nil afterDelay:0.5f];
    }
    return [_browsingHistoryData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"BrowsingHistoryCell";

    SMWBrowsingHistoryCell *cell = (SMWBrowsingHistoryCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];

    NSDictionary* browsingHistory = [_browsingHistoryData objectAtIndex:indexPath.row];
    [cell configureWithCellData:browsingHistory];
    cell.selectionStyle = UITableViewCellSelectionStyleBlue;
    [[SMWChangeColor new] richBlackCellWithTableView:self.tableView cell:cell refreshControl:nil];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        [_browsingHistoryData removeObjectAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObjects:indexPath, nil] withRowAnimation:UITableViewRowAnimationBottom];
        [[NSUserDefaults standardUserDefaults] setObject:_browsingHistoryData forKey:@"BrowsingHistoryData"];
    }
}


//--------------------------------------------------------------//
#pragma mark - Table view delegate
//--------------------------------------------------------------//
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"BrowsingHistoryWebPush"]) {
        
        if([sender isKindOfClass:[UITableViewCell class]]) {
            NSIndexPath * indexPath = [self.tableView indexPathForCell:sender];
            NSDictionary* historyData = [_browsingHistoryData objectAtIndex:indexPath.row];

            SMWArticleWebViewController *webView = [segue destinationViewController];
            webView.webData = historyData;
            
            NSString *chanStr = [historyData objectForKey:@"channelTitle"];
            [[SMWNavBarTitleLabell new] costomTitleString:chanStr andWebView:webView labelSize:165];
        }
    }
}

-(void)showAlert
{
    [SVProgressHUD showSuccessWithStatus:@"閲覧履歴はありません☻"];
}

- (IBAction)allDelete:(id)sender
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"全て削除しますか？" delegate:self cancelButtonTitle:@"キャンセル" otherButtonTitles:@"削除",nil];
    [alert show];
}

-(void)alertView:(UIAlertView*)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    switch (buttonIndex) {
        case 1:
            [self historyAllDelete];
            break;
    }
}
-(void)historyAllDelete
{
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    [ud removeObjectForKey:@"BrowsingHistoryData"];
    [ud synchronize];
    _browsingHistoryData = [[[NSUserDefaults standardUserDefaults] arrayForKey:@"BrowsingHistoryData"] mutableCopy];
    [self.tableView reloadData];
}

@end
