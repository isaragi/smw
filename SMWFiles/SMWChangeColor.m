//
//  SMWChangeColor.m
//  TheMatome
//
//  Created by Atsushi Igarashi.  on 2013/10/10.
//  Copyright (c) 2013年 Atsushi Igarashi. All rights reserved.
//

#import "SMWChangeColor.h"

@implementation SMWChangeColor
{
    NSString * _themeColor;
    UISegmentedControl *_segment;
    UINavigationItem *_navigationItem;
    UINavigationController *_navigationController;
    UIRefreshControl *_refreshControl;
    UIToolbar *_toolBar;
}

- (id)initWithSegment:(UISegmentedControl*)seg refreshControl:(UIRefreshControl*)ref navigarionItem:(UINavigationItem*)navItem navigationController:(UINavigationController*)navCon
{
    self = [super init];
    if (self) {
        _themeColor = [[NSUserDefaults standardUserDefaults] stringForKey:@"themeColor"];
        _segment = seg;
        _refreshControl = ref;
        _navigationItem = navItem;
        _navigationController = navCon;
    }
    return self;
}

- (id)initWithToolBar:(UIToolbar*)toolBar
{
    self = [super init];
    if (self) {
        _themeColor = [[NSUserDefaults standardUserDefaults] stringForKey:@"themeColor"];
        _toolBar = toolBar;
    }
    return self;
}

- (id)init {
    
    self = [super init];
    if (self) {
        _themeColor = [[NSUserDefaults standardUserDefaults] stringForKey:@"themeColor"];
    }
    return self;
}

- (void)changeColor
{
    [self changeNavColoriOS7];
}


-(void)naviColor:(UIColor*)color andSubColor:(UIColor*)subColor
{
    _navigationController.navigationBar.tintColor = color;
    [[UINavigationBar appearance] setTintColor:color];
    _navigationController.toolbar.tintColor     = color;
    
    _segment.tintColor = color;
    [[UISegmentedControl appearance] setTintColor:subColor];
    _refreshControl.tintColor = subColor;
    _toolBar.tintColor = color;
}

-(void)clearNaviColor:(UIColor*)color andSubColor:(UIColor*)subColor
{
    _navigationController.navigationBar.alpha        = 0.8;
    _navigationController.navigationBar.tintColor = color;
    _navigationController.navigationBar.translucent  = YES;
    [[UINavigationBar appearance] setTintColor:color];
    
    _segment.tintColor = color;
    [[UISegmentedControl appearance] setTintColor:subColor];
    _refreshControl.tintColor = subColor;
    
    [[UIToolbar appearance] setTintColor:color];
    _navigationController.toolbar.alpha         = 0.8;
    _navigationController.toolbar.tintColor     = color;
    _navigationController.toolbar.translucent   = YES;
    _toolBar.alpha = 0.8;
    _toolBar.tintColor = color;
    _toolBar.translucent = YES;
}

- (void)returnDefaults
{
    _navigationController.navigationBar.alpha        = 1.0;
    _navigationController.navigationBar.tintColor    = nil;
    _navigationController.navigationBar.translucent  = NO;
    [[UINavigationBar appearance] setTintColor:nil];
    
    _segment.tintColor = nil;
    [[UISegmentedControl appearance] setTintColor:nil];
    
    _navigationController.toolbar.tintColor          =nil;
    _navigationController.toolbar.alpha              =1.0;
    _navigationController.toolbar.translucent        =NO;
    
    _toolBar.alpha = 1.0;
    _toolBar.tintColor = nil;
    _toolBar.translucent = NO;
}

- (void)changeNavColoriOS7
{
    [self returnDefaultsiOS7];
    
    if (_themeColor == nil || [_themeColor isEqualToString: @"DefaultsColor"] )
    {
        [[UISegmentedControl appearance] setBackgroundColor:[UIColor whiteColor]];
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
        
    } else if ([_themeColor isEqualToString: @"BlcakColor"] ) {
        
        UIColor* color = [UIColor blackColor];
        UIColor* segmentColor = [UIColor darkGrayColor];
        [self colorForNavigationiOS7:color andSubColor:segmentColor];
        
    } else if ([_themeColor isEqualToString: @"TranslucentBlcakColor"] ) {
        
        UIColor* color = [UIColor darkGrayColor];
        [self translucentColorForNavigartioniOS7:color];
        
    } else if ([_themeColor isEqualToString: @"PinkColor"] ) {
        
        UIColor* color = [UIColor colorWithRed:1.0 green:0.5 blue:0.7 alpha:1.0];
        UIColor* segmentColor = [UIColor colorWithRed:1.0 green:0.65 blue:0.8 alpha:1.0];
        [self colorForNavigationiOS7:color andSubColor:segmentColor];
        
    } else if ([_themeColor isEqualToString: @"TranslucentPinkColor"] ) {
        
        UIColor* color = [UIColor colorWithRed:1.0 green:0.75 blue:0.9 alpha:1.0];
        [self translucentColorForNavigartioniOS7:color];
        
    } else if ([_themeColor isEqualToString: @"BlueColor"] ) {
        
        UIColor* color = [UIColor colorWithRed:0.2 green:0.3 blue:0.8 alpha:1.0];
        UIColor* segmentColor = [UIColor colorWithRed:0.7 green:0.8 blue:1.0 alpha:1.0];
        [self colorForNavigationiOS7:color andSubColor:segmentColor];
        
    } else if ([_themeColor isEqualToString: @"TranslucentBlueColor"] ) {
        
        UIColor* color = [UIColor colorWithRed:0.7 green:0.8 blue:1.0 alpha:1.0];
        [self translucentColorForNavigartioniOS7:color];
        
    } else if ([_themeColor isEqualToString: @"GreenColor"] ) {
        
        UIColor* color = [UIColor colorWithRed:0.2 green:0.6 blue:0.6 alpha:1.0];
        UIColor* segmentColor = [UIColor colorWithRed:0.7 green:0.95 blue:0.75 alpha:1.0];
        [self colorForNavigationiOS7:color andSubColor:segmentColor];
        
    } else if ([_themeColor isEqualToString: @"TranslucentGreenColor"] ) {
        
        UIColor* color = [UIColor colorWithRed:0.55 green:0.95 blue:0.8 alpha:1.0];
        [self translucentColorForNavigartioniOS7:color];
        
    } else if ([_themeColor isEqualToString: @"NightBlackColor"] ) {

        UIColor* color = [UIColor blackColor];
        UIColor *subColor = [UIColor grayColor];
        [self colorForNavigationiOS7:color andSubColor:subColor];
        [[UISegmentedControl appearance] setBackgroundColor:[UIColor colorWithRed:0.15 green:0.15 blue:0.15 alpha:1.0]];
    }
     
}
-(void)colorForNavigationiOS7:(UIColor*)color andSubColor:(UIColor*)subColor
{
    _navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [[UISegmentedControl appearance] setTintColor:subColor];
    _refreshControl.tintColor = subColor;
    _navigationController.navigationBar.barTintColor = color;
    
    _navigationController.toolbar.translucent  = NO;
    _navigationController.toolbar.tintColor  = [UIColor whiteColor];
    _navigationController.toolbar.barTintColor     = color;
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    _toolBar.barTintColor = color;
    _toolBar.tintColor = [UIColor whiteColor];
}

-(void)translucentColorForNavigartioniOS7:(UIColor*)color
{
    [[UINavigationBar appearance] setTintColor:color];
    _navigationController.navigationBar.tintColor = color;
    _navigationController.navigationBar.translucent  = YES;
    [[UISegmentedControl appearance] setTintColor:color];
    _refreshControl.tintColor = color;
    _navigationController.toolbar.tintColor = color;
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    _toolBar.tintColor = color;
}

- (void)returnDefaultsiOS7
{
    _navigationController.navigationBar.tintColor    = nil;
    _navigationController.navigationBar.translucent  = YES;
    _navigationController.navigationBar.barTintColor = nil;
    [[UINavigationBar appearance] setTintColor:nil];
    [[UISegmentedControl appearance] setBackgroundColor:nil];
    _refreshControl.tintColor = nil;
    [[UISegmentedControl appearance] setTintColor:nil];
    [[UINavigationBar appearance] setTitleTextAttributes:nil];
}

/*
 *
 *
*/
- (void)richBlackCellWithTableView:(UITableView*)tableView cell:(UITableViewCell*)cell refreshControl:(UIRefreshControl*)ref
{
    NSString *themeColor = [[NSUserDefaults standardUserDefaults] stringForKey:@"themeColor"];
    if ([themeColor isEqualToString: @"NightBlackColor"] ) {
        
        UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"blackCellBg@2x.png"]];
        tableView.backgroundView = imageView;
        
        tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        UIImage *imageBg = [UIImage imageNamed:@"BlackStyleCell.png"];
        UIImageView *imageBgView = [[UIImageView alloc] initWithImage:imageBg];
        cell.backgroundView = imageBgView;
        cell.selectionStyle = UITableViewCellSelectionStyleGray;
        ref.layer.zPosition = cell.backgroundView.layer.zPosition + 1;
        ref.tintColor = [UIColor colorWithRed:0.9 green:0.9 blue:0.9 alpha:1.0];
        
    } else {
        cell.selectionStyle = UITableViewCellSelectionStyleDefault;
        tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        cell.backgroundView = nil;
        tableView.backgroundView = nil;
    }
    
}
- (void)nightBlckColorWillChangeLabel:(UILabel*)title dateLabel:(UILabel*)date channelLabel:(UILabel*)channelTitle
{
    NSString *themeColor = [[[NSUserDefaults standardUserDefaults] stringForKey:@"themeColor"] mutableCopy];
    if ([themeColor isEqualToString: @"NightBlackColor"] ) {
        
        UIColor *darkWhite = [UIColor colorWithRed:0.8 green:0.8 blue:0.8 alpha:1.0];
        title.textColor = darkWhite;
        title.font = [UIFont systemFontOfSize:15.0];
        date.textColor = [UIColor grayColor];
        channelTitle.textColor = [UIColor grayColor];
   
    } else {
        
        title.textColor = nil;
        title.font = [UIFont boldSystemFontOfSize:15.0];
        date.textColor = [UIColor darkGrayColor];
        channelTitle.textColor = [UIColor darkGrayColor];
    }
}

+ (BOOL)isTranslucentColor
{
    NSString *themeColor = [[NSUserDefaults standardUserDefaults] stringForKey:@"themeColor"];
    float iOSVersion = [[[UIDevice currentDevice] systemVersion] floatValue];
    
    if(iOSVersion >= 7.0) {
        
        if ([themeColor isEqualToString: @"TranslucentBlcakColor"] || [themeColor isEqualToString: @"TranslucentPinkColor"] ||
            [themeColor isEqualToString: @"TranslucentBlueColor"] || [themeColor isEqualToString: @"TranslucentGreenColor"] ||
            [themeColor isEqualToString:@"DefaultsColor"] || !themeColor ) {
            
            return YES;
        }
        return NO;
    } else {
        if ([themeColor isEqualToString: @"TranslucentBlcakColor"] || [themeColor isEqualToString: @"TranslucentPinkColor"] ||
            [themeColor isEqualToString: @"TranslucentBlueColor"] || [themeColor isEqualToString: @"TranslucentGreenColor"] ) {
            return YES;
        }
    }
    return NO;
}

- (UIColor *)currentColor {
    
    if (_themeColor == nil || [_themeColor isEqualToString: @"DefaultsColor"] )
    {
        return [UIColor colorWithRed:0.0 green:122.0/255.0 blue:1.0 alpha:1.0];
        
    } else if ([_themeColor isEqualToString: @"BlcakColor"] ) {
        
         return [UIColor darkGrayColor];
        
    } else if ([_themeColor isEqualToString: @"TranslucentBlcakColor"] ) {
        
        return [UIColor darkGrayColor];
    
    } else if ([_themeColor isEqualToString: @"PinkColor"] ) {
        
        return [UIColor colorWithRed:1.0 green:0.75 blue:0.9 alpha:1.0];
        
    } else if ([_themeColor isEqualToString: @"TranslucentPinkColor"] ) {
        
        return  [UIColor colorWithRed:1.0 green:0.65 blue:0.8 alpha:1.0];
        
    } else if ([_themeColor isEqualToString: @"BlueColor"] ) {
        
        return [UIColor colorWithRed:0.7 green:0.8 blue:1.0 alpha:1.0];
        
    } else if ([_themeColor isEqualToString: @"TranslucentBlueColor"] ) {
        
        return [UIColor colorWithRed:0.7 green:0.8 blue:1.0 alpha:1.0];

    } else if ([_themeColor isEqualToString: @"GreenColor"] ) {
        
        return [UIColor colorWithRed:0.55 green:0.95 blue:0.8 alpha:1.0];
        
    } else if ([_themeColor isEqualToString: @"TranslucentGreenColor"] ) {
        
        return [UIColor colorWithRed:0.55 green:0.95 blue:0.8 alpha:1.0];
        
    } else if ([_themeColor isEqualToString: @"NightBlackColor"] ) {
        
        return [UIColor grayColor];
    }
    return nil;
}

+ (BOOL)isNightBlackColor {

    NSString *themeColor = [[NSUserDefaults standardUserDefaults] stringForKey:@"themeColor"];
    
    if ([themeColor isEqualToString: @"NightBlackColor"] ) {
        
        return YES;
    }
    return NO;
}
    
@end
