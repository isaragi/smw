//
//  AIWebViewController.h
//  
//
//  Created by Atsushi Igarashi on 13/09/08.
//  Copyright (c) 2013年 Atsushi Igarashi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SMWTwitterTimelineViewController : UIViewController <UIWebViewDelegate>

@end
