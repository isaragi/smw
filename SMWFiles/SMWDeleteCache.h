//
//  SMWDeleteCache.h
//  News＋
//
//  Created by Atsushi Igarashi on 2014/02/28.
//  Copyright (c) 2014年 Atsushi Igarashi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SMWDeleteCache : NSObject
-(void)deleteCache;
@end
