//
//  SMWImageViewController.h
//  TheMatome
//
//  Created by Atsushi Igarashi on 2013/12/25.
//  Copyright (c) 2013年 Atsushi Igarashi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SMWImageViewController : UIViewController

@property (nonatomic, strong) NSURL* imageURL;

@end
