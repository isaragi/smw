//
//  TMConfigViewController.m
//  TheMatome
//
//  Created by Atsushi Igarashi on 13/07/21.
//  Copyright (c) 2013年 Atsushi Igarashi. All rights reserved.
//

#import "SMWConfigViewController.h"
#import "SMWNavBarTitleLabel.h"
#import "SMWChangeColor.h"
#import "SMWDeleteCache.h"
#import "SVProgressHUD.h"

@interface TMConfigViewController ()

@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic, strong) NSUserDefaults *purchase;

- (IBAction)done:(id)sender;
@end

@implementation TMConfigViewController

//static NSString * const KProductManagerRemoveAd = @"ProductManagerRemoveAd";

- (void)viewDidLoad
{
    [super viewDidLoad];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    [self resetScrollOffsetAndInset];
    [self colorDidChangeWithTheFirstReading:YES];
}

-(void)viewWillAppear:(BOOL)animated
{
    [self.tableView reloadData];
    [self colorDidChangeWithTheFirstReading:NO];
    //[self checkPurchase];
    //[self.navigationController setToolbarHidden:YES animated:YES];
    [super viewWillAppear:animated];
}

-(void)colorDidChangeWithTheFirstReading:(BOOL)firstReading  {
    
    BOOL isColorChange = [[NSUserDefaults standardUserDefaults] boolForKey:@"isColorChange"];
    if (isColorChange || firstReading ) {
        
        [[SMWNavBarTitleLabell new] costomTitleLabelWithTitle:@"メニュー" andNavigation:self.navigationItem];
        SMWChangeColor *changeColor = [[SMWChangeColor alloc] initWithSegment:nil refreshControl:nil navigarionItem:self.navigationItem navigationController:self.navigationController];
        [changeColor changeColor];
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


//--------------------------------------------------------------//
#pragma mark - Table view data source
//--------------------------------------------------------------//
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (section) {
        case(0):
            return [self checkSwitchValue];
            break;
        case(1):
            return 4;
            break;
        case(2):
            return 1;
            break;
    }
    
    return 0;
}
-(int)checkSwitchValue
{
    BOOL searchTitleSwitch = [[NSUserDefaults standardUserDefaults] boolForKey:@"SearchTitleSwitchValue"];
    BOOL historySwitch = [[NSUserDefaults standardUserDefaults] boolForKey:@"BrowsingHistorySwitchValue"];
    if (!searchTitleSwitch && !historySwitch) {
        return 2;
    } else if(searchTitleSwitch && historySwitch){
        return 4;
    }
    return 3;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    
    switch (section) {
        case(0):
            return @"オプション";
            break;
        case(1):
            return @"設定";
            break;
        case(2):
            return @"About";
            break;
    }
    return nil;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *CellIdentifier = [NSString stringWithFormat:@"%lu:%lu", (unsigned long)[indexPath indexAtPosition:0],
                                (unsigned long)[indexPath indexAtPosition:1]];
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    cell = [self checkSwitchValue:cell];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        cell.selectionStyle = UITableViewCellSelectionStyleBlue;
        switch ([indexPath indexAtPosition:0]) {
            case(0):
                switch([indexPath indexAtPosition:1]) {
                    case(0):
                        cell.textLabel.text = @"サイト別";
                        break;
                    case(1):
                        cell.textLabel.text = @"お気に入り";
                        break;
                    case(2):
                        cell.textLabel.text = [self checkStr];
                        break;
                    case(3):
                        cell.textLabel.text = @"記事の検索";
                        break;
                }
                break;
            case(1):
                switch([indexPath indexAtPosition:1]) {
                    case(0):
                        cell.textLabel.text = @"カラーテーマ";
                        break;
                    case(1):
                        cell.textLabel.text = @"キャッシュのクリア";
                        break;
                    case(2):
                        cell.textLabel.text = @"購読リスト";
                        break;
                    case(3):
                        cell.textLabel.text = @"詳細設定";
                        break;
                }
                break;
            case(2):
                switch([indexPath indexAtPosition:1]) {
                    case(0):
                        cell.textLabel.text = @"このアプリについて";
                        break;
                }
        }
    }
    
    return cell;
}

-(NSString*)checkStr
{
    BOOL searchTitleSwitch = [[NSUserDefaults standardUserDefaults] boolForKey:@"SearchTitleSwitchValue"];
    BOOL historySwitch = [[NSUserDefaults standardUserDefaults] boolForKey:@"BrowsingHistorySwitchValue"];
    if (!searchTitleSwitch && !historySwitch) {
        return nil;
    } else if(searchTitleSwitch && !historySwitch){
        return @"記事の検索";
    }
    return @"閲覧履歴";
}
-(UITableViewCell*)checkSwitchValue:(UITableViewCell*)cell
{
    BOOL searchTitleSwitch = [[NSUserDefaults standardUserDefaults] boolForKey:@"SearchTitleSwitchValue"];
    BOOL historySwitch = [[NSUserDefaults standardUserDefaults] boolForKey:@"BrowsingHistorySwitchValue"];
    if (!searchTitleSwitch && !historySwitch) {
        return cell;
    }
    return nil;
}


//--------------------------------------------------------------//
#pragma mark - Table view delegate
//--------------------------------------------------------------//
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if(indexPath.section == 0) {
        if(indexPath.row == 0) {
            
            [self performSegueWithIdentifier:@"ConfChannelCellPushID" sender:nil];
            
        } else if(indexPath.row == 1) {

            [self performSegueWithIdentifier:@"ConfBookmarkCellPushID" sender:nil];
            
        } else if(indexPath.row == 2) {
            
            BOOL searchTitleSwitch = [[NSUserDefaults standardUserDefaults] boolForKey:@"SearchTitleSwitchValue"];
            BOOL historySwitch = [[NSUserDefaults standardUserDefaults] boolForKey:@"BrowsingHistorySwitchValue"];
            if ((!searchTitleSwitch && historySwitch) || (searchTitleSwitch && historySwitch)) {
                [self performSegueWithIdentifier:@"BrowsingHistoryCellPushID" sender:nil];
            } else if(searchTitleSwitch && !historySwitch){
                [self performSegueWithIdentifier:@"SearchTitleCellPushID" sender:nil];
            }
        } else if(indexPath.row == 3) {
            
            [self performSegueWithIdentifier:@"SearchTitleCellPushID" sender:nil];
        }
    }
    else if (indexPath.section == 1) {
        
        if(indexPath.row == 0) {
            
            [self performSegueWithIdentifier:@"ConfThemeColorPushID" sender:nil];
            
        } else if(indexPath.row == 1) {
            
            [self clearCacheAlert];
        } else if(indexPath.row == 2) {
            
            [self performSegueWithIdentifier:@"ReadingListCellPushID" sender:nil];
            
        } else if(indexPath.row == 3) {
            
            [self performSegueWithIdentifier:@"AdvancedOptionsCellPushID" sender:nil];
        }
        
    } else if(indexPath.section == 2){ 
        if(indexPath.row == 0) {
            
            [self performSegueWithIdentifier:@"ConfAbputCellPushID" sender:nil];
            
        }
    }
}
/*
-(void)checkPurchase
{
    _purchase = [NSUserDefaults standardUserDefaults];
    BOOL removeAd = [_purchase boolForKey:KProductManagerRemoveAd];
    if (removeAd) {
        self.navigationController.toolbar.alpha = 0;
        [self.navigationController setToolbarHidden:YES animated:YES];
    } else {
        [self.navigationController setToolbarHidden:NO animated:YES];
    }
}
*/
- (void)resetScrollOffsetAndInset {
    float iOSVersion = [[[UIDevice currentDevice] systemVersion] floatValue];
    if(iOSVersion >= 7.0)
    {
        return;
    }
    BOOL isTranslucentColor = [SMWChangeColor isTranslucentColor];

    if ( isTranslucentColor == YES ) {
        
        UIScrollView *scrollView = (UIScrollView *)self.tableView;
        UIEdgeInsets inset;
        inset.left = 0;
        inset.right = 0;
        inset.top = 44;
        inset.bottom = 44;
        scrollView.contentInset = inset;
        CGSize size = scrollView.contentSize;
        size.height = self.view.frame.size.height;
        scrollView.contentSize = size;
    }
}

//--------------------------------------------------------------//
#pragma mark - Action
//--------------------------------------------------------------//
- (IBAction)done:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

//--------------------------------------------------------------//
#pragma mark - Action clearCache
//--------------------------------------------------------------//

-(void)clearCacheAlert
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"キャッシュのクリア" message:@"読み込んだデータを空にしますか？" delegate:self cancelButtonTitle:@"キャンセル" otherButtonTitles:@"OK", nil];
    [alert show];
}

-(void)alertView:(UIAlertView*)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    switch (buttonIndex) {
        case 0:
            return;
            break;
        case 1:
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeGradient];
            [self performSelector:@selector(deleteCache) withObject:nil afterDelay:0.5f];
            break;
    }
}

-(void)deleteCache {
    
    SMWDeleteCache *cache = [[SMWDeleteCache alloc] init];
    [cache deleteCache];
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    [ud setBool:NO forKey:@"BackgroundFetching"];
    [ud synchronize];
    [SVProgressHUD showSuccessWithStatus:@"完了☻"];
}

@end