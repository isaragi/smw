//
//  SMWParseOperation.h
//  TheMatome
//
//  Created by Atsushi Igarashi on 13/07/19.
//  Copyright (c) 2013年 Atsushi Igarashi. All rights reserved.
//

extern NSString * const kFeedsErrorNotificationName;
extern NSString * const kFeedsMessageErrorKey;

@interface SMWParseOperation : NSOperation

- (id)initWithData:(NSData *)parseData sharedPSC:(NSPersistentStoreCoordinator *)psc;

@property (nonatomic, assign) BOOL isFavorMatome;

@end
