//
//  ChannelName.h
//  TheMatome
//
//  Created by Atsushi Igarashi on 2014/02/16.
//  Copyright (c) 2014年 Atsushi Igarashi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class FeedItem;

@interface ChannelName : NSManagedObject

@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSSet *feedItems;
@end

@interface ChannelName (CoreDataGeneratedAccessors)

- (void)addFeedItemsObject:(FeedItem *)value;
- (void)removeFeedItemsObject:(FeedItem *)value;
- (void)addFeedItems:(NSSet *)values;
- (void)removeFeedItems:(NSSet *)values;

@end
