//
//  SMWReadingListViewController.m
//  TheMatome
//
//  Created by Atsushi Igarashi on 2014/02/12.
//  Copyright (c) 2014年 Atsushi Igarashi. All rights reserved.
//

#import "SMWReadingListViewController.h"
#import "SMWNavBarTitleLabel.h"

@interface SMWReadingListViewController ()
@property (weak, nonatomic) IBOutlet UISwitch *topSwitch;
@property (weak, nonatomic) IBOutlet UISwitch *entertainmentSwitch;
@property (weak, nonatomic) IBOutlet UISwitch *popularSwitch;
@property (weak, nonatomic) IBOutlet UISwitch *sportsSwitch;
@property (weak, nonatomic) IBOutlet UISwitch *itSwitch;
@property (weak, nonatomic) IBOutlet UISwitch *businessSwitch;
@property (weak, nonatomic) IBOutlet UISwitch *worldSwitch;
@property (weak, nonatomic) IBOutlet UISwitch *matomeSwitch;
@property (weak, nonatomic) IBOutlet UISwitch *favorMatomeSwitch;

@property (weak, nonatomic) UISwitch *valueSwitch;
@end

@implementation SMWReadingListViewController
static NSInteger const kEULAAleartNumTag = 1;
static NSInteger const kMinimumFeedsAleartNumTag = 0;

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[SMWNavBarTitleLabell new] costomTitleLabelWithTitle:@"読み込み設定" andNavigation:self.navigationItem];
    for (int i = 0; [[self userSwitchKey] count] > i; i++) {
        BOOL switchValue = [[NSUserDefaults standardUserDefaults] boolForKey:self.userSwitchKey[i]];
        UISwitch *sw = (UISwitch*)self.userSwitchObject[i];
        if (!switchValue) {
            sw.on = NO;
        }
    }
}

-(NSArray*)userSwitchKey {
    return @[@"kTopSwitch",@"kEntertainmentSwitch",@"kPopularSwitch",@"kSportsSwitch",
             @"kITSwitch",@"kBusinessSwitch",@"kWorldSwitch",@"kMatomeSwitch",@"kFavorMatomeSwitch"];
}

-(NSArray*)userSwitchObject {
    return @[_topSwitch,_entertainmentSwitch,_popularSwitch,_sportsSwitch,
             _itSwitch,_businessSwitch,_worldSwitch,_matomeSwitch,_favorMatomeSwitch];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)saveSwitchValue:(UISwitch*)switchValue WithSwitchName:(NSString*)name {
    
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    [ud setBool:switchValue.on forKey:name];
    [ud synchronize];
}

- (IBAction)topValueChanged:(id)sender {
    
    UISwitch *sw = (UISwitch*)sender;
    [self minimumNumber:sw];
    [self saveSwitchValue:sw WithSwitchName:@"kTopSwitch"];
    [self readingListChange];
}

- (IBAction)entertainmentValueChanged:(id)sender {
    
    UISwitch *sw = (UISwitch*)sender;
    [self minimumNumber:sw];
    [self saveSwitchValue:sw WithSwitchName:@"kEntertainmentSwitch"];
    [self readingListChange];
}

- (IBAction)popularValueChanged:(id)sender {
    
    UISwitch *sw = (UISwitch*)sender;
    [self minimumNumber:sw];
    [self saveSwitchValue:sw WithSwitchName:@"kPopularSwitch"];
    [self readingListChange];
}

- (IBAction)sportsValueChanged:(id)sender {
    
    UISwitch *sw = (UISwitch*)sender;
    [self minimumNumber:sw];
    [self saveSwitchValue:sw WithSwitchName:@"kSportsSwitch"];
    [self readingListChange];
}

- (IBAction)itValueChanged:(id)sender {
    
    UISwitch *sw = (UISwitch*)sender;
    [self minimumNumber:sw];
    [self saveSwitchValue:sw WithSwitchName:@"kITSwitch"];
    [self readingListChange];
}

- (IBAction)businessValueChanged:(id)sender {
    
    UISwitch *sw = (UISwitch*)sender;
    [self minimumNumber:sw];
    [self saveSwitchValue:sw WithSwitchName:@"kBusinessSwitch"];
    [self readingListChange];
}

- (IBAction)worldValueChanged:(id)sender {
    
    UISwitch *sw = (UISwitch*)sender;
    [self minimumNumber:sw];
    [self saveSwitchValue:sw WithSwitchName:@"kWorldSwitch"];
    [self readingListChange];
}

- (IBAction)matomeValueChanged:(id)sender {
    
    UISwitch *sw = (UISwitch*)sender;
    [self minimumNumber:sw];
    if (sw.on) {
        _valueSwitch = sw;
        [self checkEULA];
    } else {
        [self saveSwitchValue:sw WithSwitchName:@"kMatomeSwitch"];
    }
    
    [self readingListChange];
}

- (IBAction)favorMatomeValueChanged:(id)sender {
    
    UISwitch *sw = (UISwitch*)sender;
    [self minimumNumber:sw];
    if (sw.on) {
        _valueSwitch = sw;
        [self checkEULA];
    } else {
        [self saveSwitchValue:sw WithSwitchName:@"kFavorMatomeSwitch"];
    }
    
    [self readingListChange];

}

- (void)readingListChange {
    NSUserDefaults *userdefaults = [NSUserDefaults standardUserDefaults];
    [userdefaults setBool:YES forKey:@"isReadingListChange"];
    [userdefaults removeObjectForKey:@"bgDownloadCompTime"];
    [userdefaults removeObjectForKey:@"downloadData"];
    [userdefaults synchronize];
}

-(void)fewAleat
{
    NSString *message = @"一つ以上は購読しましょう。";
    UIAlertView *alert =[[UIAlertView alloc]initWithTitle:nil
                                                  message:message
                                                 delegate:self
                                        cancelButtonTitle:nil
                                        otherButtonTitles:@"OK",nil];
    alert.tag = kMinimumFeedsAleartNumTag;
    [alert show];
    
}
-(void)checkEULA
{
    NSString *message = @"この記事には、過激な表現や成人向けのコンテンツが表示される可能性があります。また、17歳以下の方はご利用頂けません。";
    UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"警告"
                                                      message:message
                                                     delegate:self
                                            cancelButtonTitle:@"いいえ"
                                            otherButtonTitles:@"承認",nil];
    alert.tag = kEULAAleartNumTag;
    [alert show];

}

-(void)alertView:(UIAlertView*)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if(alertView.tag == kMinimumFeedsAleartNumTag){
        switch (buttonIndex) {

            case 0:
                _valueSwitch.on = YES;
                [self allSaveSwitch];
                break;
        }
    }
    
    if(alertView.tag == kEULAAleartNumTag){
        
        switch (buttonIndex) {
            case 0:
                _valueSwitch.on = NO;
                break;
            case 1:
                _valueSwitch.on = YES;
                break;
        }
        [self allSaveSwitch];
    }
}

- (void)minimumNumber:(UISwitch*)value {
    
    NSInteger feedsNumber = 0;
    for (int i = 0; [[self userSwitchKey] count] > i; i++) {
        BOOL switchValue = [[NSUserDefaults standardUserDefaults] boolForKey:self.userSwitchKey[i]];
        
        if (switchValue) {
            feedsNumber += 1;
        }
    }
    
    if (!value.on) {
        if (feedsNumber <= 1) {
            _valueSwitch = value;
            [self fewAleat];
        }
    }
    
}
-(void)allSaveSwitch {
    
    for (int i = 0; [[self userSwitchKey] count] > i; i++) {
        
        UISwitch *sw = (UISwitch*)self.userSwitchObject[i];
        [self saveSwitchValue:sw WithSwitchName:self.userSwitchKey[i]];
    }
}

@end
