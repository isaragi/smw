//
//  SMWBrowsingHistoryCell.m
//  TheMatome
//
//  Created by Atsushi Igarashi on 2013/10/25.
//  Copyright (c) 2013年 Atsushi Igarashi. All rights reserved.
//

#import "SMWBrowsingHistoryCell.h"
#import "SMWDateFormat.h"
#import "SMWChangeColor.h"

@interface SMWBrowsingHistoryCell ()
@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
@property (nonatomic, weak) IBOutlet UILabel *dateLabel;
@property (nonatomic, weak) IBOutlet UILabel *channelTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *hatebuLabel;
@property (weak, nonatomic) IBOutlet UILabel *tweetLabel;
@end

@implementation SMWBrowsingHistoryCell

-(void)configureWithCellData:(NSDictionary *)data
{
    self.titleLabel.text = [data objectForKey:@"title"];
    self.dateLabel.text = [NSString stringWithFormat:@"%@", [[SMWDateFormat new].dateFormatter stringFromDate:[data objectForKey:@"date"]]];
    self.channelTitleLabel.text = [data objectForKey:@"channelTitle"];
    self.tweetLabel.text = [NSString stringWithFormat:@"%@ tweet",[[data objectForKey:@"tweet"] stringValue]];
    self.hatebuLabel.text = [NSString stringWithFormat:@"%@ user",[[data objectForKey:@"hatebu"] stringValue]];
    
    [[SMWChangeColor new] nightBlckColorWillChangeLabel:self.titleLabel
                                             dateLabel:self.dateLabel
                                          channelLabel:self.channelTitleLabel];
}

@end
