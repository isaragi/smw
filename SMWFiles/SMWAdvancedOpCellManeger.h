//
//  SMWAdvancedOpCellManeger.h
//  TheMatome
//
//  Created by Atsushi Igarashi on 2013/12/24.
//  Copyright (c) 2013年 Atsushi Igarashi. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface TMAdvancedOpSectionItem : NSObject

@property (nonatomic, strong) NSString *footerName;
@property (nonatomic, strong) UIView *footerView;
@property (nonatomic, strong) NSString *headerName;
@property (nonatomic, strong) id cellLabelText;

@end

@interface SMWAdvancedOpCellManeger : NSObject

@property (nonatomic, strong, readonly)NSMutableArray *sectionData;

@property (nonatomic, strong) TMAdvancedOpSectionItem *sectionItem;

- (NSInteger)sectionCount;

- (NSInteger)rowCountWithSectionNumber:(NSInteger)num;

@end
