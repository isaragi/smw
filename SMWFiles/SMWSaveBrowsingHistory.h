//
//  SMWSaveBrowsingHistory.h
//  TheMatome
//
//  Created by Atsushi Igarashi.  on 2013/10/09.
//  Copyright (c) 2013年 Atsushi Igarashi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SMWSaveBrowsingHistory : NSObject

- (void)saveBrowsingHistory:(NSDictionary*)dic;

@end
