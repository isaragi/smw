//
//  SMWAboutViewCellManeger.h
//  TheMatome
//
//  Created by Atsushi Igarashi on 2013/12/25.
//  Copyright (c) 2013年 Atsushi Igarashi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TMAboutViewSectionItem : NSObject

@property (nonatomic, strong) NSString *footerName;
@property (nonatomic, strong) UIView *footerView;
@property (nonatomic, strong) NSString *headerName;
/**
 cellLabelTextにはrowの数が１つなら文字列、複数なら文字列の入った配列が入るので取り出すときは判定する
 */
@property (nonatomic, strong) id cellLabelText;

@end


@interface SMWAboutViewCellManeger : NSObject

@property (nonatomic, strong, readonly) NSMutableArray *celldata;

- (NSInteger)sectionCount;

- (NSInteger)rowCountWithSectionNumber:(NSInteger)num;

@end
