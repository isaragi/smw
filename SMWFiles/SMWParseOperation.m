//
//  SMWParseOperation.m
//  TheMatome
//
//  Created by Atsushi Igarashi on 13/07/19.
//  Copyright (c) 2013年 Atsushi Igarashi. All rights reserved.
//
#import "SMWParseOperation.h"
#import "SMWAppDelegate.h"
#import "FeedItem.h"
#import "FeedCategory.h"
#import "ChannelName.h"
#import "SMWDataCache.h"

@interface SMWParseOperation () <NSXMLParserDelegate>

@property (copy, readonly) NSData *feedData;
@property (nonatomic, strong) FeedItem *currentFeedItemObject;
@property (nonatomic, strong) FeedCategory *feedCategoryObject;
@property (nonatomic, strong) SMWDataCache *dataCache;
@property (nonatomic, strong) NSMutableArray *currentParseBatch;
@property (nonatomic, strong) NSMutableString *currentParsedCharacterData;
@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, strong) NSPersistentStoreCoordinator *sharedPSC;
@property (nonatomic, strong) NSString *persistentStorePath;
@end

@implementation SMWParseOperation
{
    NSDateFormatter *_dateFormatter;
    BOOL _accumulatingParsedCharacterData;
    BOOL _didAbortParsing;
    NSUInteger _parsedFeedsCounter;
}

NSString * const kFeedsErrorNotificationName = @"FeedItemErrorNotif";
NSString * const kFeedsMessageErrorKey = @"FeedItemsMsgErrorKey";
//static double lookuptime;

- (id)initWithData:(NSData *)parseData sharedPSC:(NSPersistentStoreCoordinator *)psc {
    
    self = [super init];
    if (self) {
        //lookuptime =0;
        _isItem = NO;
        _feedData = [parseData copy];
        [self dateFormat];
        
        self.currentParseBatch = [[NSMutableArray alloc] init];
        self.currentParsedCharacterData = [[NSMutableString alloc] init];
        self.sharedPSC = psc;
    }
    return self;
}

- (void)dealloc {
    
     //NSLog(@"lookup time %f", lookuptime);
}

+ (BOOL)automaticallyNotifiesObserversForKey:(NSString*)key {
	if ([key isEqualToString:@"isExecuting"] || [key isEqualToString:@"isFinished"]) {
		return YES;
	}
	return [super automaticallyNotifiesObserversForKey:key];
}

- (void)addFeedItemsToList:(NSArray *)FeedItems {
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *ent = [NSEntityDescription entityForName:@"FeedItem" inManagedObjectContext:self.managedObjectContext];
    fetchRequest.entity = ent;
    
    fetchRequest.propertiesToFetch = [NSArray arrayWithObjects:@"channelName.name", @"feedCategory.name", @"title", @"date", nil];
    [fetchRequest setResultType:NSDictionaryResultType];
    
    NSError *error = nil;
    FeedItem *feedItem = nil;
    for (feedItem in FeedItems) {
        
        [self duplicationCheck:feedItem withFetchRequest:fetchRequest];
        
        fetchRequest.predicate = [NSPredicate predicateWithFormat:@"channelName.name = %@ AND feedCategory.name = %@ AND title = %@", feedItem.channelName.name, feedItem.feedCategory.name, feedItem.title];
        NSArray *fetchedItems = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
        
        if (fetchedItems.count == 0) {
            [self.managedObjectContext insertObject:feedItem];
            [self addRelationshipItemsToList:feedItem];
        
        }
    }
    
    if ([self.managedObjectContext hasChanges]) {
        
        if (![self.managedObjectContext save:&error]) {

            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

// The main function for this NSOperation, to start the parsing.
- (void)main {

    self.managedObjectContext = [[NSManagedObjectContext alloc] init];
    self.managedObjectContext.persistentStoreCoordinator = self.sharedPSC;
    
    NSXMLParser *parser = [[NSXMLParser alloc] initWithData:self.feedData];
    [parser setDelegate:self];
    [parser parse];
    
    if ([self.currentParseBatch count] > 0) {
        [self addFeedItemsToList:self.currentParseBatch];
    }
}

//--------------------------------------------------------------//
#pragma mark - Parser constants
//--------------------------------------------------------------//
BOOL  _isItem;

static NSUInteger const kMaximumNumberOfFeedsToParse = 1200;
static NSUInteger const kSizeOfFeedBatch = 10;

static NSString * const kChannelElementName = @"channel";
static NSString * const kChannelTitleElementName = @"channeltitle";
static NSString * const kItemElementName = @"item";
static NSString * const kLinkElementName = @"link";
static NSString * const kTitleElementName = @"title";
static NSString * const kDateElementName = @"dc:date";
static NSString * const kTweetElementName = @"tweet";
static NSString * const kHatenaElementName = @"hatena";
static NSString * const kRankElementName = @"rank";

//--------------------------------------------------------------//
#pragma mark - NSXMLParser delegate methods
//--------------------------------------------------------------//
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict {
    
    if (_parsedFeedsCounter >= kMaximumNumberOfFeedsToParse) {
        
        _didAbortParsing = YES;
        [parser abortParsing];
    }
    
    if ([elementName isEqualToString:kChannelElementName]) {
        
        FeedCategory *category;
        category = (FeedCategory *)[self initRelationshipObject:category withEntityName:@"FeedCategory"];
        
        self.feedCategoryObject = category;
    }
    
    if ([elementName isEqualToString:kItemElementName]) {
        
        _isItem = YES;
        NSEntityDescription *ent = [NSEntityDescription entityForName:@"FeedItem" inManagedObjectContext:self.managedObjectContext];
        FeedItem *feeditem =[[FeedItem alloc] initWithEntity:ent insertIntoManagedObjectContext:nil];
        self.currentFeedItemObject = feeditem;
        self.currentFeedItemObject.feedCategory = self.feedCategoryObject;
    }
    
    else if ([elementName isEqualToString:kTitleElementName] ||
             [elementName isEqualToString:kLinkElementName] ||
             [elementName isEqualToString:kDateElementName] ||
             [elementName isEqualToString:kChannelTitleElementName] ||
             [elementName isEqualToString:kTweetElementName] ||
             [elementName isEqualToString:kHatenaElementName] ||
             [elementName isEqualToString:kRankElementName])
    {
        _accumulatingParsedCharacterData = YES;
        [self.currentParsedCharacterData setString:@""];
    }
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {

    if ([elementName isEqualToString:kItemElementName]) {
       
        _isItem = NO;
        
        [self.currentParseBatch addObject:self.currentFeedItemObject];
        _parsedFeedsCounter++;
        if ([self.currentParseBatch count] >= kSizeOfFeedBatch) {
            [self addFeedItemsToList:self.currentParseBatch];
            self.currentParseBatch = [NSMutableArray array];
        }
    }
    else if ([elementName isEqualToString:kChannelTitleElementName]) {
        
        if (_isItem) {
            [self insertChannelNameWithFeedItem:self.currentFeedItemObject];
        }
    }
    
    else if ([elementName isEqualToString:kTitleElementName]) {
        if (_isItem) {
            self.currentFeedItemObject.title = [NSString stringWithString:self.currentParsedCharacterData];
            
        } else {
             self.feedCategoryObject.name = [NSString stringWithString:self.currentParsedCharacterData];
        }
    }
    
    else if ([elementName isEqualToString:kLinkElementName]) {
        if (_isItem) {
            self.currentFeedItemObject.url = [NSString stringWithString:self.currentParsedCharacterData];
        }
    }
    
    else if ([elementName isEqualToString:kDateElementName]) {
        if (_isItem) {
            self.currentFeedItemObject.date = [_dateFormatter dateFromString:self.currentParsedCharacterData];
        }
    }
    
    else if ([elementName isEqualToString:kTweetElementName]) {
        if (_isItem) {
            self.currentFeedItemObject.tweet = [NSNumber numberWithInt:[self.currentParsedCharacterData intValue]];
        }
    }
    
    else if ([elementName isEqualToString:kHatenaElementName]) {
        if (_isItem) {
            self.currentFeedItemObject.hatebu = [NSNumber numberWithInt:[self.currentParsedCharacterData intValue]];
        }
    }
    
    else if ([elementName isEqualToString:kRankElementName]) {
        if (_isItem) {
            self.currentFeedItemObject.rank = [NSNumber numberWithInt:[self.currentParsedCharacterData intValue]];
        }
    }
    
    _accumulatingParsedCharacterData = NO;
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {
    if (_accumulatingParsedCharacterData) {
        
        [self.currentParsedCharacterData appendString:string];
    }
}

- (void)handleFeedItemsError:(NSError *)parseError {
    
    assert([NSThread isMainThread]);
    [[NSNotificationCenter defaultCenter] postNotificationName:kFeedsErrorNotificationName object:self userInfo:@{kFeedsMessageErrorKey: parseError}];
}

- (void)parser:(NSXMLParser *)parser parseErrorOccurred:(NSError *)parseError {
    
    if ([parseError code] != NSXMLParserDelegateAbortedParseError && !_didAbortParsing) {
        [self performSelectorOnMainThread:@selector(handleFeedItemsError:) withObject:parseError waitUntilDone:NO];
    }
}

- (void)insertChannelNameWithFeedItem:(FeedItem*)feeditem {
    
    ChannelName *channel;
    channel = (ChannelName *)[self initRelationshipObject:channel withEntityName:@"ChannelName"];
    channel.name = [NSString stringWithString:self.currentParsedCharacterData];
    feeditem.channelName = channel;
}

- (id)initRelationshipObject:(id)object withEntityName:(NSString*)name {
    
    NSEntityDescription *ent = [NSEntityDescription entityForName:name
                                           inManagedObjectContext:self.managedObjectContext];
    
    Class class = NSClassFromString(name);
    object = [[class alloc] initWithEntity:ent
                     insertIntoManagedObjectContext:nil];
    
    return object;
}

-(void)addRelationshipItemsToList:(FeedItem*)feeditem {
    
    //double before = [NSDate timeIntervalSinceReferenceDate];
    NSString *categoryName = feeditem.feedCategory.name;
    FeedCategory *category = [[self dataCache] categoryWithName:categoryName];
    [category addFeedItemsObject:feeditem];
    
    NSString *channelName = feeditem.channelName.name;
    ChannelName *channel = [[self dataCache] channelWithName:channelName];
    [channel addFeedItemsObject:feeditem];
    //double delta = [NSDate timeIntervalSinceReferenceDate] - before;
    //lookuptime += delta;
}

- (SMWDataCache *)dataCache {
    if (_dataCache == nil) {
        _dataCache = [[SMWDataCache alloc] init];
        _dataCache.managedObjectContext = self.managedObjectContext;
    }
    return _dataCache;
}

- (void)duplicationCheck:(FeedItem *)item withFetchRequest:(NSFetchRequest*)request  {
    
    NSError *error;
    if (_isFavorMatome) {
        
        request.predicate = [NSPredicate predicateWithFormat:@"channelName.name = %@ AND date = %@ AND title = %@", item.channelName.name, item.date, item.title];
        NSArray *fetchedItems = [self.managedObjectContext executeFetchRequest:request error:&error];
        
        if (fetchedItems.count == 0) {

            item.isOnlyFavorMatome = [NSNumber numberWithBool:YES];
        }
    }
}

-(void)dateFormat {
    
    _dateFormatter = [[NSDateFormatter alloc] init];
    [_dateFormatter setTimeStyle:NSDateFormatterFullStyle];
    [_dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"JST"]];
    [_dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"ja_JP"]];
    [_dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZZZZ"];
}
@end