//
//  SMWChannelViewPagerController.h
//  TheMatome
//
//  Created by Atsushi Igarashi on 2014/02/15.
//  Copyright (c) 2014年 Atsushi Igarashi. All rights reserved.
//

#import "SMWChannelVPGController.h"

@interface SMWChannelViewPagerController : SMWChannelVPGController
@property (assign, nonatomic, readonly) NSInteger favorMatomeNumber;
-(void)inserteNilForViewPegerAry;
@end
