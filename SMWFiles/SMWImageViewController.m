//
//  SMWImageViewController.m
//  TheMatome
//
//  Created by Atsushi Igarashi on 2013/12/25.
//  Copyright (c) 2013年 Atsushi Igarashi. All rights reserved.
//

#import "SMWImageViewController.h"
#import "SMWChangeColor.h"

@interface SMWImageViewController ()
- (IBAction)imageClose:(id)sender;
@property (weak, nonatomic) IBOutlet UIWebView *imageView;
@property (weak, nonatomic) IBOutlet UIToolbar *toolBar;

@end

@implementation SMWImageViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    _imageView.backgroundColor = [UIColor colorWithWhite:0.9 alpha:1.0];
    SMWChangeColor *changeToolBarColor = [[SMWChangeColor alloc] initWithToolBar:_toolBar];
    [changeToolBarColor changeColor];
    NSURLRequest *req = [NSURLRequest requestWithURL:_imageURL];
    [_imageView loadRequest:req];
    _imageView.scalesPageToFit = YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)imageClose:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
