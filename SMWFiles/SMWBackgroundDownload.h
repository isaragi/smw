//
//  SMWBackgroundDownload.h
//  News＋
//
//  Created by Atsushi Igarashi on 2014/05/16.
//  Copyright (c) 2014年 Atsushi Igarashi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SMWBackgroundDownload : NSObject <NSURLSessionDownloadDelegate>

- (void)fetch;
@property (copy, nonatomic) void (^completionHandler)(UIBackgroundFetchResult);
@end
