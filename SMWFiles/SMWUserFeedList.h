//
//  SMWUserFeedList.h
//  TheMatome
//
//  Created by Atsushi Igarashi on 2014/02/05.
//  Copyright (c) 2014年 Atsushi Igarashi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SMWUserFeedList : NSObject

- (NSArray *)readingLists;

- (NSArray *)feedCategoryNameAry;

@property (nonatomic, strong, readonly) NSMutableArray * userTabList;
@property (nonatomic, assign, readonly) BOOL isFavorMatomeFeed;
@property (nonatomic, assign, readonly) NSInteger userFeedCount;
@end
