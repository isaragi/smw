//
//  TMConfThemeColorTableViewController.m
//  TheMatome
//
//  Created by Atsushi Igarashi on 13/08/01.
//  Copyright (c) 2013年 Atsushi Igarashi. All rights reserved.
//

#import "SMWChangeColorTableViewController.h"
#import "SMWConfigViewController.h"
#import "SVProgressHUD.h"
#import "SMWNavBarTitleLabel.h"
#import "SMWChangeColor.h"
#import "SMWNavBarTitleLabel.h"
#import "SMWViewAnimation.h"
#import <AudioToolbox/AudioServices.h>

@interface SMWChangeColorTableViewCellManeger : NSObject

@property (nonatomic, strong) NSArray* cellLabelTect;
@property (nonatomic, strong) NSArray* themeColorKey;

@end

@implementation SMWChangeColorTableViewCellManeger

- (id)init
{
    self = [super init];
    if (self) {
        [self cellDataManager];
    }
    return self;
}

-(void)cellDataManager
{
    _themeColorKey = @[@"DefaultsColor", @"BlcakColor", @"TranslucentBlcakColor",@"PinkColor", @"TranslucentPinkColor", @"BlueColor",@"TranslucentBlueColor", @"GreenColor", @"TranslucentGreenColor",@"NightBlackColor"];
    
    
    _cellLabelTect = @[@"デフォルト", @"ブラック", @"クリア×ブラック",@"ピンク",@"クリア×ピンク",
                  @"ブルー",@"クリア×ブルー",@"グリーン",@"クリア×グリーン",@"ナイトブラック"];
}

@end


@interface SMWChangeColorTableViewController ()
@property (nonatomic, weak) IBOutlet UITableView *themeColorTableView;
@property (nonatomic, strong) NSString *themeColorStr;
@property (nonatomic, strong) SMWChangeColorTableViewCellManeger *cellData;
@property (nonatomic, strong) SMWViewAnimation *viewAnimation;
@property (nonatomic, assign) SystemSoundID sound;
@end

@implementation SMWChangeColorTableViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    //[self.navigationController setToolbarHidden:YES animated:YES];
    _cellData = [SMWChangeColorTableViewCellManeger new];
    _viewAnimation = [SMWViewAnimation new];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self changeColor];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)changeColor {
    
    SMWChangeColor *changeColor = [[SMWChangeColor alloc] initWithSegment:nil refreshControl:nil navigarionItem:self.navigationItem navigationController:self.navigationController];
    [changeColor changeColor];
    [[SMWNavBarTitleLabell new] costomTitleLabelWithTitle:@"テーマ" andNavigation:self.navigationItem];
}
- (void)dealloc
{
    AudioServicesDisposeSystemSoundID (_sound);
}
//--------------------------------------------------------------//
#pragma mark - Table view data source
//--------------------------------------------------------------//

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSAssert([_cellData.themeColorKey count] == [_cellData.cellLabelTect count], @"Value is not match!");
    return [_cellData.cellLabelTect count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"ConfigThemeColorCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    _themeColorStr = [[[NSUserDefaults standardUserDefaults] stringForKey:@"themeColor"] copy];
    
    NSString *colorKey = _cellData.themeColorKey[indexPath.row];
    NSString *labelText = _cellData.cellLabelTect[indexPath.row];
    
    cell.textLabel.text = labelText;
    
    if (_themeColorStr == nil && indexPath.row == 0 ) {
        
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
        
    } else if ([_themeColorStr isEqualToString: colorKey] ){
        
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
        
    } else{
        
        cell.accessoryType = UITableViewCellEditingStyleNone;
    }
    
    return cell;
    
}

//--------------------------------------------------------------//
#pragma mark - Table view delegate
//--------------------------------------------------------------//

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSString *colorKey = (NSString*)[_cellData.themeColorKey objectAtIndex:indexPath.row];
    
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    
    cell.accessoryType = UITableViewCellAccessoryCheckmark;
    
    [self themeColor:colorKey];
    
    UIImageView *sc = [_viewAnimation screenshot];
    [self.view.superview addSubview:sc];
    self.tableView.hidden = YES;
    self.navigationController.navigationBar.hidden = YES;
    [self pushSound];
    [UIView animateWithDuration:kPushedBackAnimationDuration delay:0.25 options:UIViewAnimationOptionCurveEaseInOut animations:^{
     [sc.layer addAnimation:[_viewAnimation animationGroupPushedBackward] forKey:nil];
        sc.alpha = 0.7;
    } completion:^(BOOL finished) {
        
        [UIView animateWithDuration:kBringForwardAnimationDuration delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{

            [sc.layer addAnimation:[_viewAnimation animationGroupBringForward] forKey:nil];
            sc.alpha = 1.0;

        } completion:^(BOOL finished) {
            [sc removeFromSuperview];
            self.tableView.hidden = NO;
            self.navigationController.navigationBar.hidden = NO;
            [self.tableView reloadData];
            [self changeColor];
            [self performSelector:@selector(successAlert) withObject:nil afterDelay:0.2f];
        }];

    }];
}

-(void)successAlert
{
    NSUserDefaults *userdefaults = [NSUserDefaults standardUserDefaults];
    [userdefaults setBool:YES forKey:@"isColorChange"];
    [userdefaults synchronize];
    [SVProgressHUD showSuccessWithStatus:@"完了☻"];
}

- (void)themeColor:(NSString*)themeColorStr
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:themeColorStr forKey:@"themeColor"];
    [defaults synchronize];
}

- (IBAction)done:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)pushSound {
    NSString *path = [[NSBundle mainBundle] pathForResource:@"push-sound" ofType:@"caf"];
    NSURL *url = [NSURL fileURLWithPath:path];
    AudioServicesCreateSystemSoundID((__bridge CFURLRef)url, &_sound);
    AudioServicesPlaySystemSound(_sound);
}

@end
