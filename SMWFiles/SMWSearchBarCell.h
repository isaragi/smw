//
//  TMSearchTitleCell.h
//  TheMatome
//
//  Created by Atsushi Igarashi on 2013/10/26.
//  Copyright (c) 2013年 Atsushi Igarashi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SMWSearchBarCell : UITableViewCell

- (void)configureWithCellData:(NSArray*)data;

@end
