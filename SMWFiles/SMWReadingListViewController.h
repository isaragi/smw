//
//  SMWReadingListViewController.h
//  TheMatome
//
//  Created by Atsushi Igarashi on 2014/02/12.
//  Copyright (c) 2014年 Atsushi Igarashi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SMWReadingListViewController : UITableViewController

@end
