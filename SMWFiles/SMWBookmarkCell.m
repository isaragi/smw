//
//  TMDetailBookmarkCell.m
//  TheMatome
//
//  Created by Atsushi Igarashi on 13/07/25.
//  Copyright (c) 2013年 Atsushi Igarashi. All rights reserved.
//

#import "SMWBookmarkCell.h"
#import "SMWDateFormat.h"
#import "SMWChangeColor.h"

@interface SMWBookmarkCell ()
@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
@property (nonatomic, weak) IBOutlet UILabel *dateLabel;
@property (nonatomic, weak) IBOutlet UILabel *channelTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *hatebuLabel;
@property (weak, nonatomic) IBOutlet UILabel *tweetLabel;
@end

@implementation SMWBookmarkCell

-(void)changeCellColor
{
    [[SMWChangeColor new] nightBlckColorWillChangeLabel:self.titleLabel
                                             dateLabel:self.dateLabel
                                          channelLabel:self.channelTitleLabel];
}

- (NSString*)bookmarkTitle
{
    return _titleLabel.text;
}

- (void)setBookmarkTitle:(NSString*)bookmarkTitle
{
    _titleLabel.text = bookmarkTitle;
}

- (NSString*)bookmarkChannelTitle
{
    return _channelTitleLabel.text;
}

- (void)setBookmarkChannelTitle:(NSString *)bookmarkChannelTitle
{
    _channelTitleLabel.text = bookmarkChannelTitle;
}

- (NSString*)bookmarkDate
{
    return _dateLabel.text;
}

- (void)setBookmarkDate:(NSDate *)bookmarkDate
{
    _dateLabel.text = [NSString stringWithFormat:@"%@", [[SMWDateFormat new].dateFormatter stringFromDate:bookmarkDate]];
}

- (NSString *)hatebu
{
    return _hatebuLabel.text;
}

- (void)setHatebu:(NSNumber *)hatebu
{
    _hatebuLabel.text = [NSString stringWithFormat:@"%@ user",[hatebu stringValue]];
}

- (NSString *)tweet
{
    return _tweetLabel.text;
}

- (void)setTweet:(NSNumber *)tweet
{
    _tweetLabel.text = [NSString stringWithFormat:@"%@ tweet",[tweet stringValue]];
}

@end
