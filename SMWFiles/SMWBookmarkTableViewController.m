//
//  TMConfDetailBookmarkTableViewController.m
//  TheMatome
//
//  Created by Atsushi Igarashi on 13/07/25.
//  Copyright (c) 2013年 Atsushi Igarashi. All rights reserved.
//

#import "SMWBookmarkTableViewController.h"
#import "SMWBookmarkCell.h"
#import "SMWArticleWebViewController.h"
#import "SVProgressHUD.h"
#import "SMWNavBarTitleLabel.h"
#import "SMWSaveBrowsingHistory.h"
#import "SMWChangeColor.h"

@interface SMWBookmarkTableViewController ()

@property (nonatomic, strong) NSMutableArray *bookmarkData;
- (IBAction)allDelete:(id)sender;

@end


@implementation SMWBookmarkTableViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[SMWNavBarTitleLabell new] costomTitleLabelWithTitle:@"お気に入り" andNavigation:self.navigationItem];
    //[self.navigationController setToolbarHidden:YES animated:YES];
}

- (void)viewWillAppear:(BOOL)animated
{
    _bookmarkData = [[[NSUserDefaults standardUserDefaults] arrayForKey:@"bookmarks"] mutableCopy];
    [self.tableView reloadData];
    [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    if ([_bookmarkData count] == 0) {
        [self performSelector:@selector(showAlert) withObject:nil afterDelay:0.5f];
        }
    return [_bookmarkData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellIdentifier = @"BookmarkCell";
    SMWBookmarkCell *cell = (SMWBookmarkCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[SMWBookmarkCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    NSDictionary* bookmark = [_bookmarkData objectAtIndex:indexPath.row];

    cell.bookmarkTitle = [bookmark objectForKey:@"title"];
    cell.bookmarkChannelTitle = [bookmark objectForKey:@"channelTitle"];
    cell.bookmarkDate = [bookmark objectForKey:@"date"];
    cell.tweet = [bookmark objectForKey:@"tweet"];
    cell.hatebu = [bookmark objectForKey:@"hatebu"];

    cell.selectionStyle = UITableViewCellSelectionStyleBlue;
    
    [cell changeCellColor];
    [[SMWChangeColor new] richBlackCellWithTableView:self.tableView cell:cell refreshControl:nil];
    
    return cell;
}


//--------------------------------------------------------------//
#pragma mark - Table view delegate
//--------------------------------------------------------------//
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSDictionary*   bookmark = [_bookmarkData objectAtIndex:indexPath.row];
    [self performSegueWithIdentifier:@"BookmarkWebPush" sender:bookmark];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"BookmarkWebPush"]) {
        
        NSDictionary* bookmark = sender;
        [[SMWSaveBrowsingHistory new] saveBrowsingHistory:bookmark];
        
        SMWArticleWebViewController *webView = [segue destinationViewController];
        webView.webData = bookmark;
        
        NSString *chanStr = [bookmark objectForKey:@"channelTitle"];
        [[SMWNavBarTitleLabell new] costomTitleString:chanStr andWebView:webView labelSize:165];
    }
}

-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        [_bookmarkData removeObjectAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObjects:indexPath, nil] withRowAnimation:UITableViewRowAnimationBottom];
        [[NSUserDefaults standardUserDefaults] setObject:_bookmarkData forKey:@"bookmarks"];
    }
}

-(void)showAlert
{
    [SVProgressHUD showSuccessWithStatus:@"ここにお気に入りを追加出来ます☻"];
}


- (IBAction)allDelete:(id)sender
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"全て削除しますか？" delegate:self cancelButtonTitle:@"キャンセル" otherButtonTitles:@"削除",nil];
    [alert show];
}

-(void)alertView:(UIAlertView*)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    switch (buttonIndex) {
        case 1:
            [self bookmarkAllDelete];
            break;
    }
}
-(void)bookmarkAllDelete
{
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    [ud removeObjectForKey:@"bookmarks"];
    [ud synchronize];
    _bookmarkData = [[[NSUserDefaults standardUserDefaults] arrayForKey:@"bookmarks"] mutableCopy];
    [self.tableView reloadData];
}

@end
