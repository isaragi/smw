//
//  FeedItem.h
//  TheMatome
//
//  Created by Atsushi Igarashi on 2014/02/16.
//  Copyright (c) 2014年 Atsushi Igarashi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class ChannelName, FeedCategory;

@interface FeedItem : NSManagedObject

@property (nonatomic, retain) NSDate * date;
@property (nonatomic, retain) NSNumber * hatebu;
@property (nonatomic, retain) NSNumber * rank;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSNumber * tweet;
@property (nonatomic, retain) NSString * url;
@property (nonatomic, retain) NSNumber * isOnlyFavorMatome;
@property (nonatomic, retain) ChannelName *channelName;
@property (nonatomic, retain) FeedCategory *feedCategory;

@end
