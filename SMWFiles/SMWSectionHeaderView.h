 //
 //  SMWSectionHeaderView.h
 //  TheMatome
 //
 //  Created by Atsushi Igarashi on 13/07/21.
 //  Copyright (c) 2013年 Atsushi Igarashi. All rights reserved.
 //


#import <Foundation/Foundation.h>

@protocol SectionHeaderViewDelegate;


@interface SMWSectionHeaderView : UITableViewHeaderFooterView

@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
@property (nonatomic, weak) IBOutlet UILabel *itemNumberLabel;
@property (nonatomic, weak) IBOutlet UIButton *disclosureButton;
@property (nonatomic, weak) IBOutlet UILabel *backgroundLabel;
@property (nonatomic, weak) IBOutlet id <SectionHeaderViewDelegate> delegate;

@property (nonatomic) NSInteger section;

-(void)toggleOpenWithUserAction:(BOOL)userAction;

@end



@protocol SectionHeaderViewDelegate <NSObject>

@optional
-(void)sectionHeaderView:(SMWSectionHeaderView*)sectionHeaderView sectionOpened:(NSInteger)section;
-(void)sectionHeaderView:(SMWSectionHeaderView*)sectionHeaderView sectionClosed:(NSInteger)section;

@end

