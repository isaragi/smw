//
//  TMChannelItemCell.m
//  TheMatome
//
//  Created by Atsushi Igarashi on 13/07/26.
//  Copyright (c) 2013年 Atsushi Igarashi. All rights reserved.
//

#import "SMWChannelCell.h"
#import "FeedItem.h"
#import "ChannelName.h"
#import "SMWDateFormat.h"
#import "SMWChangeColor.h"

@interface SMWChannelCell ()

@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
@property (nonatomic, weak) IBOutlet UILabel *dateLabel;
@property (nonatomic, weak) IBOutlet UILabel *channelTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *hatebuLabel;
@property (weak, nonatomic) IBOutlet UILabel *tweetLabel;

@end

@implementation SMWChannelCell

- (void)chConfigureWithFeeditem:(FeedItem *)feeditem {
    
    self.titleLabel.text = feeditem.title;
    self.dateLabel.text = [NSString stringWithFormat:@"%@", [[SMWDateFormat new].dateFormatter stringFromDate:feeditem.date]];
    self.channelTitleLabel.text = feeditem.channelName.name;
    self.tweetLabel.text = [NSString stringWithFormat:@"%@ tweet",[feeditem.tweet stringValue]];
    self.hatebuLabel.text = [NSString stringWithFormat:@"%@ user",[feeditem.hatebu stringValue]];
    
    [[SMWChangeColor new] nightBlckColorWillChangeLabel:self.titleLabel
                                             dateLabel:self.dateLabel
                                          channelLabel:self.channelTitleLabel];}

@end
