//
//  SMWUsageHintViewController.m
//  TheMatome
//
//  Created by Atsushi Igarashi on 2013/10/27.
//  Copyright (c) 2013年 Atsushi Igarashi. All rights reserved.
//

#import "SMWUsageHintViewController.h"
#import "SMWNavBarTitleLabel.h"
#import "SMWChangeColor.h"

@interface SMWUsageHintViewController ()
@property (nonatomic, strong) UITextView *textView;

@end

@implementation SMWUsageHintViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[SMWNavBarTitleLabell new] costomTitleLabelWithTitle:@"使い方のヒント" andNavigation:self.navigationItem];
    [self textViewHeight];
    //[self.navigationController setToolbarHidden:YES animated:YES];
}

-(void)textViewHeight
{
    NSString *st0 = @"\n● バックグラウンド読み込みについて\n\n　アプリを習慣的に使うことにより、OSがユーザーに最適なタイミングを学習し、その他の影響（電波状況など）を考慮した上でバックグラウンドで読み込んてくれます。バックグラウンドでの読み込みが必要ない場合は、【設定 → 一般 → Appのバックグラウンド更新】から本アプリのスイッチをOffにしてください。通知の必要がない場合も、【設定 → 通知センター】で本アプリの通知をOffにしてください。\n\n\n";
    
    NSString *st1 = @"● シェイクジェスチャーについて\n\n　記事閲覧画面でデバイスを軽く振ると、自動スクロールのOn / Offを切り替えられます。また、【（本アプリの）メニュー → 詳細設定】にてスクロールのスピードを調節出来ます。\n\n\n";

    
    NSString *st2 = @"● 閲覧履歴\n\n　取得したくない場合は詳細設定から履歴をOffにしてください。200件まで記録し、201件からは古いものから削除されて行きます。一括削除も個別削除も出来ます。\n\n\n";

    
    NSString *st3 = @"● 記事検索\n\n　スペースを挟んで単語を2つ以上入力する事によりAND検索になります。アルファベットの大文字小文字はどちらにもヒットするようになっています。タイムリーな記事のみの検索となります。\n\n\n";
    
    
    NSString *st4 = @"● 便利なOS標準機能\n\n　左上【戻る】ボタンを押さずとも、画面左はじから右へスワイプする事によって戻れるようになりました。（iOS7からの新機能）\n　画面最上部をタップする事により一番上へ瞬時に戻れます。\n\n\n";
    
    
    NSString *st5 = @"● SNSへの投稿について\n\n　当アプリではユーザーネーム、パスワードなどの情報は一切取得しておりませんので、アプリ内に保存も一切しておりません。\n\n\n";
    
    
    NSString *st6 = @"● Pocketの認証解除\n\n　一度認証したものを解除したくなった場合は、Pocket側 (http://getpocket.com/) から【Options → Applications】の”Sakutto matome yakko”を削除で解除する事ができます。\n\n\n";

    
    NSString *st8 = @"● 動作環境\n\n iOS8.0〜\n\n　\n　*推奨環境 iPhone 5 以降のモデル\n";
    
    NSMutableString *str = [NSMutableString string];
    [str appendString:st0];
    [str appendString:st1];
    [str appendString:st2];
    [str appendString:st3];
    [str appendString:st4];
    [str appendString:st5];
    [str appendString:st6];
    [str appendString:st8];

    float iOSVersion = [[[UIDevice currentDevice] systemVersion] floatValue];
    UIScreen* screen = [UIScreen mainScreen];
    
    int naviHeight = 0;
    BOOL isTranslucentColor = [SMWChangeColor isTranslucentColor];
    
    if(iOSVersion < 7.0) {
        
        if ( isTranslucentColor == NO )
        {
            naviHeight += -44;
        }
        _textView = [[UITextView alloc] initWithFrame:CGRectMake(0.0, 0, screen.applicationFrame.size.width, screen.applicationFrame.size.height + naviHeight )];
        _textView.text = str;
        
        if ( isTranslucentColor ==YES ) {
            
            NSString *str1 = @"\n\n";
            str1= [str1 stringByAppendingString:str];
            _textView.text = str1;
        }
    } else {
        naviHeight = 20;
        
        _textView = [[UITextView alloc] initWithFrame:CGRectMake(0.0, 0, screen.applicationFrame.size.width, screen.applicationFrame.size.height +naviHeight )];
        _textView.text = str;
        
    }
    
    _textView.font = [UIFont systemFontOfSize:14];
    _textView.dataDetectorTypes = UIDataDetectorTypeLink;
    _textView.editable = NO;
    [self.view addSubview:_textView];
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
