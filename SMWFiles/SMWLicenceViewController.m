//
//  SMWLicenceViewController.m
//  News++
//
//  Created by Atsushi Igarashi on 2014/02/18.
//  Copyright (c) 2014年 Atsushi Igarashi. All rights reserved.
//

#import "SMWLicenceViewController.h"
#import "SMWNavBarTitleLabel.h"

@interface SMWLicenceViewController ()

@end

@implementation SMWLicenceViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[SMWNavBarTitleLabell new] costomTitleLabelWithTitle:@"Licence" andNavigation:self.navigationItem];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
