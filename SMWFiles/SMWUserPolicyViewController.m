//
//  SMWUserPolicyViewController.m
//  News＋
//
//  Created by Atsushi Igarashi on 2014/02/28.
//  Copyright (c) 2014年 Atsushi Igarashi. All rights reserved.
//

#import "SMWUserPolicyViewController.h"
#import "SMWNavBarTitleLabel.h"
#import "SVProgressHUD.h"
#import "SMWChangeColor.h"

@interface SMWUserPolicyViewController () <UIWebViewDelegate>

@property (assign, nonatomic) NSInteger errorCount;
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@end

@implementation SMWUserPolicyViewController

static NSInteger const kErrorCountMax = 1000;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _webView.delegate = self;
    [self contentInset];
    [[SMWNavBarTitleLabell new] costomTitleLabelWithTitle:@"利用規約" andNavigation:self.navigationItem];
	NSURL *url = [NSURL URLWithString:@"http://49.212.162.23/news-app/user-policy2.html"];
    NSURLRequest *req = [NSURLRequest requestWithURL:url];
    [_webView loadRequest:req];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)webViewDidStartLoad:(UIWebView*)wv
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
}

-(void)webViewDidFinishLoad:(UIWebView*)wv
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}

- (void)webView:(UIWebView*)webView didFailLoadWithError:(NSError*)error
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    
    if ([error code] != NSURLErrorCancelled) {
        _errorCount++;
        if ( _errorCount <= kErrorCountMax ) {
            [SVProgressHUD showErrorWithStatus:@"電波状況をお確かめ下さい"];
            _errorCount = 0;
        }
    }
}

-(void)contentInset
{
    float iOSVersion = [[[UIDevice currentDevice] systemVersion] floatValue];
    if(iOSVersion >= 7.0)
    {
        return;
    }
    BOOL isTranslucentColor = [SMWChangeColor isTranslucentColor];
    
    if ( isTranslucentColor) {
        [[_webView scrollView] setContentInset:UIEdgeInsetsMake(44, 0, 0, 0)];
    }
}
@end
