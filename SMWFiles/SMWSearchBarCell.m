//
//  TMSearchTitleCell.m
//  TheMatome
//
//  Created by Atsushi Igarashi on 2013/10/26.
//  Copyright (c) 2013年 Atsushi Igarashi. All rights reserved.
//

#import "SMWSearchBarCell.h"
#import "SMWDateFormat.h"
#import "SMWChangeColor.h"
#import "ChannelName.h"
#import "FeedItem.h"
@interface SMWSearchBarCell ()

@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
@property (nonatomic, weak) IBOutlet UILabel *dateLabel;
@property (nonatomic, weak) IBOutlet UILabel *channelTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *hatebuLabel;
@property (weak, nonatomic) IBOutlet UILabel *tweetLabel;

@end

@implementation SMWSearchBarCell

-(void)configureWithCellData:(NSArray *)data
{
    FeedItem *item = (FeedItem *)data;
    self.titleLabel.text = [data valueForKey:@"title"];
    self.dateLabel.text = [NSString stringWithFormat:@"%@", [[SMWDateFormat new].dateFormatter stringFromDate:[data valueForKey:@"date"]]];
    self.channelTitleLabel.text = item.channelName.name;
    self.tweetLabel.text = [NSString stringWithFormat:@"%@ tweet",[[data valueForKey:@"tweet"] stringValue]];
    self.hatebuLabel.text = [NSString stringWithFormat:@"%@ user",[[data valueForKey:@"hatebu"] stringValue]];
    
    [[SMWChangeColor new] nightBlckColorWillChangeLabel:self.titleLabel
                                             dateLabel:self.dateLabel
                                          channelLabel:self.channelTitleLabel];
}

@end
