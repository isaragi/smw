//
//  SMWViewPagerController.m
//  TheMatome
//
//  Created by Atsushi Igarashi on 2014/01/23.
//  Copyright (c) 2014年 Atsushi Igarashi. All rights reserved.
//

#import "SMWViewPagerController.h"
#import "SMWHomeViewController.h"
#import "SMWChangeColor.h"
#import "SMWConfigViewController.h"
#import "SMWUserFeedList.h"
#import "SMWGreetingAnimation.h"

@interface SMWViewPagerController () <ViewPagerDataSource, ViewPagerDelegate>

@property (nonatomic, assign) NSUInteger numberOfTabs;
@property (nonatomic, strong) SMWUserFeedList *tabData;

@property (nonatomic, strong) SMWHomeViewController *homeView;
@property (nonatomic, strong) SMWChangeColor *color;
@property (strong, nonatomic)UIButton *settingButton;
@property (weak, nonatomic) IBOutlet UIImageView *greetingImage;
@end


@implementation SMWViewPagerController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self applicationWillEnterForegroundNotification];
    self.dataSource = self;
    self.delegate = self;
    [self greetingAnimation];
    [self loadContent];    
}

-(void)viewWillAppear:(BOOL)animated
{
    [self userReadingList];
    [self changeColorWithFlag:NO];
    [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)loadContent {
    
    _tabData = [SMWUserFeedList new];
    self.numberOfTabs = [_tabData.userTabList count];
    
    if (_tabData.isFavorMatomeFeed) {
        _favorMatomeNumber = _tabData.userFeedCount;
        
    } else {
        _favorMatomeNumber = 100;
    }
    [self changeColorWithFlag:YES];
}

#pragma mark - Interface Orientation Changes
- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    
    // Update changes after screen rotates
    [self performSelector:@selector(setNeedsReloadOptions) withObject:nil afterDelay:duration];
}

#pragma mark - ViewPagerDataSource
- (NSUInteger)numberOfTabsForViewPager:(ViewPagerController *)viewPager {
    return self.numberOfTabs;
}

- (UIView *)viewPager:(ViewPagerController *)viewPager viewForTabAtIndex:(NSUInteger)index {
    
    UILabel *label = [UILabel new];
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont boldSystemFontOfSize:12.0];
    label.text = _tabData.userTabList[index];
    label.numberOfLines = 2;
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = ([SMWChangeColor isTranslucentColor]) ? [UIColor blackColor] : [UIColor whiteColor];
    [label sizeToFit];
    
    return label;
}

- (UIViewController *)viewPager:(ViewPagerController *)viewPager contentViewControllerForTabAtIndex:(NSUInteger)index {
        
    _homeView = [self.storyboard instantiateViewControllerWithIdentifier:@"contentViewController"];
    _homeView.feedNumber = index;
    _homeView.viewPager = self;

    return _homeView;
}

#pragma mark - ViewPagerDelegate
- (CGFloat)viewPager:(ViewPagerController *)viewPager valueForOption:(ViewPagerOption)option withDefault:(CGFloat)value {
    
    CGFloat width = [UIScreen mainScreen].bounds.size.width;
    
    switch (option) {
        case ViewPagerOptionStartFromSecondTab:
            return 0.0;
        case ViewPagerOptionCenterCurrentTab:
            return 1.0;
        case ViewPagerOptionTabLocation:
            return 1.0;
        case ViewPagerOptionTabHeight:
            return 44.0;
        case ViewPagerOptionTabOffset:
            return 36.0;
        case ViewPagerOptionTabWidth:
            return ([self isFewTabs]) ? width / self.numberOfTabs : 62.0;
        case ViewPagerOptionFixFormerTabsPositions:
            return 0.0;
        case ViewPagerOptionFixLatterTabsPositions:
            return 0.0;
        default:
            return value;
    }
}

-(BOOL)isFewTabs {
    if (self.numberOfTabs <= 4) {
        
        return YES;
    }
    return NO;
}

- (UIColor *)viewPager:(ViewPagerController *)viewPager colorForComponent:(ViewPagerComponent)component withDefault:(UIColor *)color {
    
    BOOL isNightBlockColor = [SMWChangeColor isNightBlackColor];
    UIColor *currentColor = [_color currentColor];
    
    switch (component) {
        case ViewPagerIndicator:
            return currentColor;
            
        case ViewPagerTabsView:
            return [UIColor clearColor];

        case ViewPagerContent:
            return (isNightBlockColor) ? [UIColor darkGrayColor]:[UIColor whiteColor];

        default:
            return color;
    }
}

-(void)userReadingList {
    
    BOOL isReadingListChange = [[NSUserDefaults standardUserDefaults] boolForKey:@"isReadingListChange"];
    
    if (isReadingListChange) {
        [self loadContent];
        [self reloadData];
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isReadingListChange"];
    }
}

-(void)changeColorWithFlag:(BOOL)firstLoading  {
    
    BOOL isColorChange = [[NSUserDefaults standardUserDefaults] boolForKey:@"isColorChange"];
    if (isColorChange || firstLoading ) {
        
        _color = [SMWChangeColor new];
        [self reloadData];
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isColorChange"];
        [self settingIcon];
    }
}

- (void)settingIcon
{
    BOOL isNightBlockColor = [SMWChangeColor isNightBlackColor];
    UIImage *settingIcon = [UIImage imageNamed:(isNightBlockColor)?@"pushed-setting-icon.png":@"setting-icon.png"];
    UIImage *pushedIcon = [UIImage imageNamed:(isNightBlockColor)?@"setting-icon.png":@"pushed-setting-icon.png"];
    CGRect screenSize = [[UIScreen mainScreen] bounds];
    if (!_settingButton) {
        _settingButton = [[UIButton alloc] initWithFrame:CGRectMake(screenSize.size.width-40,screenSize.size.height-40, 38, 38)];
        [_settingButton addTarget:self action:@selector(menuButtonPushed:) forControlEvents:UIControlEventTouchUpInside];
    }else{
        [_settingButton removeFromSuperview];
    }
    [_settingButton setBackgroundImage:settingIcon forState:UIControlStateNormal];
    [_settingButton setImage:pushedIcon forState:UIControlStateHighlighted];
    [self.view addSubview:_settingButton];
}

-(void)menuButtonPushed:(id)sender {
    TMConfigViewController *configView = [self.storyboard instantiateViewControllerWithIdentifier:@"configViewModal"];
    [self presentViewController:configView animated:YES completion:nil];
}

-(void)applicationWillEnterForegroundNotification
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(willEnterForegroundLoad)
                                                 name:UIApplicationWillEnterForegroundNotification
                                               object:nil];
}
-(void)willEnterForegroundLoad
{
    BOOL isAutoReload = [[NSUserDefaults standardUserDefaults] boolForKey:@"AutoReloadSwitchValue"];
    if (!isAutoReload) {
        return ;
    }
    if(self.navigationController.visibleViewController == self) {
        
        [_homeView foregroundReload];
    }
}

-(void)greetingAnimation {

    SMWGreetingAnimation *animation = [SMWGreetingAnimation new];
    [animation animationWithImageView:_greetingImage];
}

@end
