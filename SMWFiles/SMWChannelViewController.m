//
//  TMChannelFeedsViewController.m
//  TheMatome
//
//  Created by Atsushi Igarashi on 13/07/26.
//  Copyright (c) 2013年 Atsushi Igarashi. All rights reserved.
//

#import "SMWAppDelegate.h"
#import "SMWChannelViewPagerController.h"
#import "SMWChannelViewController.h"
#import "SMWChannelCell.h"
#import "SMWParseOperation.h"
#import "SMWArticleWebViewController.h"
#import "FeedItem.h"
#import "ChannelName.h"
#import <CFNetwork/CFNetwork.h>
#import "SMWSectionInfo.h"
#import "SMWSectionHeaderView.h"
#import "SVProgressHUD.h"
#import "SMWNavBarTitleLabel.h"
#import "SMWSaveBrowsingHistory.h"
#import "SMWChangeColor.h"
#import "SMWUserFeedList.h"

@interface TMChannelViewController () <UIActionSheetDelegate, NSFetchedResultsControllerDelegate, SectionHeaderViewDelegate>

@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic, strong) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property (nonatomic, strong) NSURLConnection *feedConnection;
@property (nonatomic, strong) NSMutableData *feedItemData;
@property (nonatomic, strong) NSOperationQueue *parseQueue;
@property (nonatomic, strong) NSMutableArray* sectionInfoArray;
@property (nonatomic, assign) NSInteger openSectionIndex;
@property (nonatomic, strong) SMWSectionHeaderView *sectionHeaderView;

@property (nonatomic, assign) BOOL isNowLoading;
@property (nonatomic, strong) SMWUserFeedList *userList;
@property (nonatomic, strong) NSArray *userLists;
@property (nonatomic, assign) NSInteger roadingCount;
@property (nonatomic, assign) NSInteger categoryNumber;
@property (nonatomic, strong) NSMutableArray* duplicationArray;
@property (nonatomic, strong) NSString *currentLoadingCategoryKey;
@end

@implementation TMChannelViewController

static int const headerHeight = 52;
static NSTimeInterval const kchRefreshTimeInterval = 60 * 30;
static NSString * const kchannelLastFeedsUpdateKey = @"LastFeedUpdate";
static NSString * const SectionHeaderViewIdentifier = @"SectionHeaderViewIdentifier";

-(BOOL)canBecomeFirstResponder {
    
    return YES;
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    //[self.navigationController setToolbarHidden:YES animated:YES];
    [self makeAccordionCell];
    _categoryNumber = _feedNumber;
    [self initialUserList];
    [self refeshController];
    [self lastUpdate];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self accordionCell];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)dealloc {
    
    [_feedConnection cancel];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)makeAccordionCell
{
    self.tableView.sectionHeaderHeight = headerHeight;
    self.openSectionIndex = NSNotFound;
    UINib *sectionHeaderNib = [UINib nibWithNibName:@"SectionHeaderView" bundle:nil];
    [self.tableView registerNib:sectionHeaderNib forHeaderFooterViewReuseIdentifier:SectionHeaderViewIdentifier];
}

-(void)accordionCell
{
    if ((self.sectionInfoArray == nil) || ([self.sectionInfoArray count] != [self numberOfSectionsInTableView:self.tableView])) {
        if (self.fetchedResultsController) {
            // For each play, set up a corresponding SectionInfo object to contain the default height for each row.
            NSMutableArray *infoArray = [[NSMutableArray alloc] init];
            NSArray *fetchedObjects = [self.fetchedResultsController fetchedObjects];
            _sectionInfoArray = nil;
            for (FeedItem *item in fetchedObjects) {
                
                SMWSectionInfo *sectionInfo = [[SMWSectionInfo alloc] init];
                sectionInfo.feedItem = item;
                sectionInfo.open = NO;
                [infoArray addObject:sectionInfo];
            }
            self.sectionInfoArray = infoArray;
        }
    }
}

- (NSManagedObjectContext *)managedObjectContext {
	
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    SMWAppDelegate *appDelegate = (SMWAppDelegate *)[[UIApplication sharedApplication] delegate];
    self.managedObjectContext = [appDelegate managedObjectContext];
    return _managedObjectContext;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
	
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    SMWAppDelegate *appDelegate = (SMWAppDelegate *)[[UIApplication sharedApplication] delegate];
    self.persistentStoreCoordinator = [appDelegate persistentStoreCoordinator];
    return _persistentStoreCoordinator;
}


//--------------------------------------------------------------//
#pragma mark - NSURLConnectionDelegate
//--------------------------------------------------------------//

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    
    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
    if (([httpResponse statusCode]/100) == 2) {
        self.feedItemData = [NSMutableData data];
    }
    else {
        NSString * errorString = NSLocalizedString(@"HTTP Error", @"Error message displayed when receving a connection error.");
        NSDictionary *userInfo = @{NSLocalizedDescriptionKey : errorString};
        NSError *error = [NSError errorWithDomain:@"HTTP" code:[httpResponse statusCode] userInfo:userInfo];
        [self handleError:error];
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    
    [self.feedItemData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    [SVProgressHUD showErrorWithStatus:@"ロード失敗..."];
    if ([error code] == kCFURLErrorNotConnectedToInternet) {
        
        NSString * errorString = NSLocalizedString(@"No Connection Error", @"Error message displayed when not connected to the Internet.");
        NSDictionary *userInfo = @{NSLocalizedDescriptionKey : errorString};
        NSError *noConnectionError = [NSError errorWithDomain:NSCocoaErrorDomain code:kCFURLErrorNotConnectedToInternet userInfo:userInfo];
        [self handleError:noConnectionError];
    }
    else {

        [self handleError:error];
    }
    
    self.feedConnection = nil;
    _isNowLoading = NO;
    [self intervalOfLoadingOrEndWithFlag];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    
    self.feedConnection = nil;
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    
    SMWParseOperation *parseOperation = [[SMWParseOperation alloc] initWithData:self.feedItemData sharedPSC:self.persistentStoreCoordinator];
    
    parseOperation.isFavorMatome = ([[self getCurrentCategoryKey] isEqualToString:@"FavorRank"])? YES : NO;
    
    [self.parseQueue addOperation:parseOperation];
    
    self.feedItemData = nil;
    
    [self saveLastReadingTime];
}

-(void)saveLastReadingTime {
    
    NSString *currentCategory = [self getCurrentCategoryKey];
    NSString *lastFeedUpdate = [currentCategory stringByAppendingString:kchannelLastFeedsUpdateKey];
	[[NSUserDefaults standardUserDefaults] setObject:[NSDate date] forKey:lastFeedUpdate];
}

- (NSString *)getCurrentCategoryKey {
    
    NSString *currentCategory = _currentLoadingCategoryKey;
    
    if ([currentCategory isEqualToString:@"SortByFavoriteOfOneDaysMatome"] ||
        [currentCategory isEqualToString:@"SortByFavoriteOfThreeDaysMatome"] ||
        [currentCategory isEqualToString:@"SortByFavoriteOfOneWeeksMatome"]) {
        
        currentCategory = @"FavorRank";
    }
    return currentCategory;
}

- (void)handleError:(NSError *)error {
    
    NSString *errorMessage = [error localizedDescription];
    NSString *alertTitle = NSLocalizedString(@"Error Title", @"Title for alert displayed when download or parse error occurs.");
    NSString *okTitle = NSLocalizedString(@"OK ", @"OK Title for alert displayed when download or parse error occurs.");
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:alertTitle message:errorMessage delegate:nil cancelButtonTitle:okTitle otherButtonTitles:nil];
    [alertView show];
}

- (void)feedItemsError:(NSNotification *)notif {
    
    assert([NSThread isMainThread]);
    if (notif.name == kFeedsErrorNotificationName) {
        [self handleError:[[notif userInfo] valueForKey:kFeedsMessageErrorKey]];
    }
}


//--------------------------------------------------------------//
#pragma mark - UITableViewDelegate
//--------------------------------------------------------------//
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return [[self.fetchedResultsController sections] count];
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    NSInteger numberOfRows = 0;
    if ([[self.fetchedResultsController sections] count] > 0) {
        
        id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
        numberOfRows = [sectionInfo numberOfObjects];
        
        if (self.sectionInfoArray && [[self.fetchedResultsController sections] count] <= [self.sectionInfoArray count]) {
            SMWSectionInfo *SMWSectionInfo = (self.sectionInfoArray)[section];
            return SMWSectionInfo.open ? numberOfRows : 0;
            
        } if (self.sectionInfoArray) {
            [self accordionCell];
            
        } else {
            return 0;
        } 
    }
    return numberOfRows;
}

-(UIView*)tableView:(UITableView*)tableView viewForHeaderInSection:(NSInteger)section {
        
    SMWSectionHeaderView *sectionHeaderView = [self.tableView dequeueReusableHeaderFooterViewWithIdentifier:SectionHeaderViewIdentifier];
    id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
        
    sectionHeaderView.titleLabel.text = [NSString stringWithFormat:NSLocalizedString(@"%@", @"sectionName"), [sectionInfo name]];
    sectionHeaderView.itemNumberLabel.text = [NSString stringWithFormat:NSLocalizedString(@"%d items", @"items numberOfObjects"),[sectionInfo numberOfObjects]];
    
    sectionHeaderView.section = section;
    sectionHeaderView.delegate = self;
        
    CGRect topFrame = CGRectMake(0, 0.5 , 320, 0.5);
    UIView *topBorder = [[UIView alloc] initWithFrame:topFrame];
    topBorder.backgroundColor = [UIColor whiteColor];
    [sectionHeaderView addSubview:topBorder];
        
    CGRect bottomFrame = CGRectMake(0, headerHeight - 0.5 , 320, 0.5);
    UIView *bottomView = [[UIView alloc] initWithFrame:bottomFrame];
    bottomView.backgroundColor = [UIColor colorWithRed:0.85 green:0.85 blue:0.85 alpha:1.0];
    [sectionHeaderView addSubview:bottomView];

    return sectionHeaderView;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *kChFeedItemCellID = @"ConfigChannelItemCell";
    
  	SMWChannelCell *cell = (SMWChannelCell *)[tableView dequeueReusableCellWithIdentifier:kChFeedItemCellID];
    
    FeedItem *feedItem = (FeedItem *)[self.fetchedResultsController objectAtIndexPath:indexPath];
    
    [cell chConfigureWithFeeditem:feedItem];
    [[SMWChangeColor new] richBlackCellWithTableView:self.tableView cell:cell refreshControl:nil];
    
	return cell;
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    
    [self.tableView reloadData];
}

- (NSFetchedResultsController *)fetchedResultsController {
    
    if (_fetchedResultsController == nil) {
        
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        
        NSEntityDescription *entity = [NSEntityDescription entityForName:@"FeedItem"
                                                  inManagedObjectContext:self.managedObjectContext];
        [fetchRequest setEntity:entity];
                
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"channelName.name" ascending:YES];
        NSSortDescriptor *sortDescriptor2 = [[NSSortDescriptor alloc] initWithKey:@"date" ascending:NO];
        NSString *sectionNameKeyPath = @"channelName.name";
        
        NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor, sortDescriptor2, nil];
        
        [fetchRequest setSortDescriptors:sortDescriptors];
        
        NSString *categoryName = [self categoryName];

        if (self.isRankingMatomeFeedLoading) {
            
            NSPredicate *pred = [NSCompoundPredicate predicateWithFormat:@"isOnlyFavorMatome = %@",[NSNumber numberWithBool:YES]];
            [fetchRequest setPredicate:pred];
            
        } else {
            
            NSPredicate *pred = [NSCompoundPredicate predicateWithFormat:@"feedCategory.name = %@",categoryName];
            [fetchRequest setPredicate:pred];
        }

        NSFetchedResultsController *aFetchedResultsController =
        [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                            managedObjectContext:self.managedObjectContext
                                              sectionNameKeyPath:sectionNameKeyPath
                                                       cacheName:nil];
        self.fetchedResultsController = aFetchedResultsController;
        
        self.fetchedResultsController.delegate = self;

        NSError *error = nil;
        
        if (![self.fetchedResultsController performFetch:&error]) {
            
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            //abort();
        }
    }
	return _fetchedResultsController;
}


- (void)intervalOfLoadingOrEndWithFlag {
    
    if (_isNowLoading) {
        
        [self feedAction];
        
    } else {
        
        [self dismissIndicator];
    }
}

-(void)dismissIndicator {
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    [SVProgressHUD dismiss];
    [SVProgressHUD showSuccessWithStatus:@"ロード完了☻"];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if (object == self.parseQueue && [keyPath isEqualToString:@"operationCount"]) {
        
        if (self.parseQueue.operationCount == 0) {
            [self performSelectorOnMainThread:@selector(intervalOfLoadingOrEndWithFlag) withObject:nil waitUntilDone:NO];
        }
    }
    else {
        [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
    }
}


//--------------------------------------------------------------//
#pragma mark - Table view delegate
//--------------------------------------------------------------//
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    FeedItem *feedItem = (FeedItem *)[self.fetchedResultsController objectAtIndexPath:indexPath];
    [self performSegueWithIdentifier:@"ConfigfeedItemCellPush" sender:feedItem];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"ConfigfeedItemCellPush"]) {
        FeedItem *feedItem = sender;
        
        NSDictionary* webdata = [NSDictionary dictionaryWithObjectsAndKeys:
                             feedItem.title, @"title",
                             feedItem.date, @"date",
                             feedItem.url, @"url",
                             feedItem.channelName.name, @"channelTitle",
                             feedItem.tweet, @"tweet",
                             feedItem.hatebu, @"hatebu", nil];
        
        [[SMWSaveBrowsingHistory new] saveBrowsingHistory:webdata];
        
        SMWArticleWebViewController *webView = [segue destinationViewController];
        webView.webData = webdata;

        NSString *chanStr = feedItem.channelName.name;
        [[SMWNavBarTitleLabell new] costomTitleString:chanStr andWebView:webView labelSize:165];
         
    }
}

//--------------------------------------------------------------//
#pragma mark - Action Refresh
//--------------------------------------------------------------//
-(void)refeshController
{
    self.refreshControl = [[UIRefreshControl alloc] init];
    [self.refreshControl addTarget:self action:@selector(actionRefresh:) forControlEvents:UIControlEventValueChanged];
}

-(IBAction)actionRefresh:(id)sender {
    
    SMWChangeColor *change = [[SMWChangeColor new] initWithSegment:nil refreshControl:self.refreshControl navigarionItem:nil navigationController:nil];
    
    [change changeColor];
    _isNowLoading = YES;
    _roadingCount = 0;
    [self readyStertAction];
    [self.tableView reloadData];
    [self.refreshControl endRefreshing];
}
-(void)readyStertAction
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeGradient];
    [self feedAction];
}

//--------------------------------------------------------------//
#pragma mark - Action readAction
//--------------------------------------------------------------//

-(void)lastUpdate
{
    if ([self isTimeOfRefresh]) {
        
        _isNowLoading = YES;
        [self readyStertAction];
    }
}

-(BOOL)isTimeOfRefresh {
    
    NSString *currentCategory = (self.isRankingMatomeFeedLoading) ? @"FavorRank" : [self categoryName];
    NSString *lastFeedUpdate = [currentCategory stringByAppendingString:kchannelLastFeedsUpdateKey];
    NSDate *lastUpdate = [[NSUserDefaults standardUserDefaults] objectForKey:lastFeedUpdate];
    [self checkUpCache];
    
    if ( (-[lastUpdate timeIntervalSinceNow] > kchRefreshTimeInterval) || !lastUpdate) {
        return YES;
    }
    return NO;
}

-(void)checkUpCache {
    
    BOOL isClearCache = [[NSUserDefaults standardUserDefaults] boolForKey:@"clearCache"];
    if (isClearCache) {
         [_viewPager inserteNilForViewPegerAry];
        for (int i = 0; i < [[self deleteNameAry] count] ; i++) {
            
            NSString *str = self.deleteNameAry[i];
            NSString *lastFeedUpdate = [str stringByAppendingString:kchannelLastFeedsUpdateKey];
            [[NSUserDefaults standardUserDefaults] setObject:nil forKey:lastFeedUpdate];
        }
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"clearCache"];
    }
}

-(NSArray*)deleteNameAry {
    
    return@[@"TopFeeds",@"EntertainmentFeeds",@"PopularFeeds",@"SportsFeeds",@"ITFeeds",
            @"BusinessFeeds",@"WorldFeeds",@"SortByDateMatomeFeeds",@"FavorRank"];
}

-(void)feedAction
{
    NSString *feedURLString = [self feedURL];
    
    if (!_isNowLoading) {
        [self intervalOfLoadingOrEndWithFlag];
        return;
    }
    
    [self.parseQueue cancelAllOperations];
    [self.parseQueue removeObserver:self forKeyPath:@"operationCount"];

    self.parseQueue = [NSOperationQueue new];
    
    NSURLRequest *feedItemURLRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:feedURLString]];
    self.feedConnection = [[NSURLConnection alloc] initWithRequest:feedItemURLRequest delegate:self];
    
    NSAssert(self.feedConnection != nil, @"Failure to create URL connection.");
    
    [self.parseQueue addObserver:self forKeyPath:@"operationCount" options:0 context:NULL];
    
    // observe for any errors that come from our parser
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(feedItemsError:) name:kFeedsErrorNotificationName object:nil];
}

-(void)initialUserList {
    
    _userList = [SMWUserFeedList new];
    _userLists = [_userList readingLists];
}

-(NSString *)feedURL {
    
    if (!_userList) {
        
        [self initialUserList];
    }
    if (_userLists.count > _roadingCount) {
        _currentLoadingCategoryKey = _userList.feedCategoryNameAry[self.roadingCount];
        return _userLists[_roadingCount++];
    }
    
    _isNowLoading = NO;
    return nil;
}

-(BOOL)isRankingMatomeFeedLoading {
    
    if (_categoryNumber == _viewPager.favorMatomeNumber) {
        
        return YES;
    }
    return NO;
}

-(NSString *)categoryName {
    
    if (_categoryNumber == _viewPager.favorMatomeNumber) {
        
        return nil;
        }

    return _userList.feedCategoryNameAry[_categoryNumber];
}

-(void)foregroundReload {
    
    _roadingCount = 0;
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    [SVProgressHUD showWithStatus:@"更新をチェック中..." maskType:SVProgressHUDMaskTypeGradient];
    if ([self isTimeOfRefresh]) {
        
        _isNowLoading = YES;
        [self performSelector:@selector(updatingNotice) withObject:nil afterDelay:1.5f];
        
    } else {
        [self performSelector:@selector(dismissIndicator) withObject:nil afterDelay:1.5f];
    }
    
}
-(void)updatingNotice {
    [SVProgressHUD showWithStatus:@"更新中..." maskType:SVProgressHUDMaskTypeGradient];
    [self feedAction];
}

//--------------------------------------------------------------//
#pragma mark - Section header delegate
//--------------------------------------------------------------//

-(void)sectionHeaderView:(SMWSectionHeaderView*)sectionHeaderView sectionOpened:(NSInteger)sectionOpened {
    
	SMWSectionInfo *sectionInfo = (self.sectionInfoArray)[sectionOpened];
    
	sectionInfo.open = YES;
    
    /*
     Create an array containing the index paths of the rows to insert: These correspond to the rows for each quotation in the current section.
     */
    id <NSFetchedResultsSectionInfo> sectionInfo2 = [[self.fetchedResultsController sections] objectAtIndex:sectionOpened];
    
    NSInteger countOfRowsToInsert = [sectionInfo2 numberOfObjects];
    NSMutableArray *indexPathsToInsert = [[NSMutableArray alloc] init];
    for (NSInteger i = 0; i < countOfRowsToInsert; i++) {
        [indexPathsToInsert addObject:[NSIndexPath indexPathForRow:i inSection:sectionOpened]];
    }
    
    /*
     Create an array containing the index paths of the rows to delete: These correspond to the rows for each quotation in the previously-open section, if there was one.
     */
    NSMutableArray *indexPathsToDelete = [[NSMutableArray alloc] init];
    
    NSInteger previousOpenSectionIndex = self.openSectionIndex;
    if (previousOpenSectionIndex != NSNotFound) {
        
		SMWSectionInfo *previousOpenSection = (self.sectionInfoArray)[previousOpenSectionIndex];
        
        previousOpenSection.open = NO;
        [previousOpenSection.headerView toggleOpenWithUserAction:NO];
        
        id <NSFetchedResultsSectionInfo> sectionInfo3 = [[self.fetchedResultsController sections] objectAtIndex:previousOpenSectionIndex];
        
        NSInteger countOfRowsToDelete = [sectionInfo3 numberOfObjects];
        for (NSInteger i = 0; i < countOfRowsToDelete; i++) {
            [indexPathsToDelete addObject:[NSIndexPath indexPathForRow:i inSection:previousOpenSectionIndex]];
        }
    }
    
    // Style the animation so that there's a smooth flow in either direction.
    UITableViewRowAnimation insertAnimation;
    UITableViewRowAnimation deleteAnimation;
    if (previousOpenSectionIndex == NSNotFound || sectionOpened < previousOpenSectionIndex) {
        insertAnimation = UITableViewRowAnimationTop;
        deleteAnimation = UITableViewRowAnimationBottom;
    }
    else {
        insertAnimation = UITableViewRowAnimationBottom;
        deleteAnimation = UITableViewRowAnimationTop;
    }
    
    // Apply the updates.
    [self.tableView beginUpdates];
    [self.tableView insertRowsAtIndexPaths:indexPathsToInsert withRowAnimation:insertAnimation];
    [self.tableView deleteRowsAtIndexPaths:indexPathsToDelete withRowAnimation:deleteAnimation];
    [self.tableView endUpdates];
    self.openSectionIndex = sectionOpened;
}

-(void)sectionHeaderView:(SMWSectionHeaderView*)sectionHeaderView sectionClosed:(NSInteger)sectionClosed {
    
    /*
     Create an array of the index paths of the rows in the section that was closed, then delete those rows from the table view.
     */
	SMWSectionInfo *sectionInfo = (self.sectionInfoArray)[sectionClosed];
    
    sectionInfo.open = NO;
    NSInteger countOfRowsToDelete = [self.tableView numberOfRowsInSection:sectionClosed];
    
    if (countOfRowsToDelete > 0) {
        NSMutableArray *indexPathsToDelete = [[NSMutableArray alloc] init];
        for (NSInteger i = 0; i < countOfRowsToDelete; i++) {
            [indexPathsToDelete addObject:[NSIndexPath indexPathForRow:i inSection:sectionClosed]];
        }
        [self.tableView deleteRowsAtIndexPaths:indexPathsToDelete withRowAnimation:UITableViewRowAnimationTop];
    }
    self.openSectionIndex = NSNotFound;
}

@end
