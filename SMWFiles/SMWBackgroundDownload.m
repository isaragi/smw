//
//  SMWBackgroundDownload.m
//  News＋
//
//  Created by Atsushi Igarashi on 2014/05/16.
//  Copyright (c) 2014年 Atsushi Igarashi. All rights reserved.
//

#import "SMWBackgroundDownload.h"
#import "SMWUserFeedList.h"
#import "SMWAppDelegate.h"
#import "SMWTitleXMLPaese.h"

@implementation SMWBackgroundDownload {
    SMWUserFeedList *_userList;
    NSArray *_userLists;
    NSInteger _count;
    NSString *_currentLoadingCategoryKey;
    NSURLSessionConfiguration *_configration;
    NSURLSession *_session;
    //NSDate *_startDate;
    NSMutableArray *_downloadData;
    //NSOperationQueue *_parseQueue;
}

static NSString * const kLastFeedUpdate = @"LastFeedUpdate";
static NSString * const kBackGroundNotificationKey = @"BackGroundRoading";

- (void)userInfo {
    _userList = [SMWUserFeedList new];
    _userLists = [_userList readingLists];
}

- (void)userInfoNil {
    _count = 0;
    _userList = nil;
    _userLists = nil;
    _downloadData = nil;
    [_session finishTasksAndInvalidate];
    [_session invalidateAndCancel];
    _configration = nil;
}

-(void)fetch {
    
    if (_count == 0) {
        [self performSelector:@selector(fetchTimeout) withObject:nil afterDelay:30];
        /*
        _startDate = [NSDate date];
        [[UIApplication sharedApplication] cancelAllLocalNotifications];
        UILocalNotification *localNotif = [[UILocalNotification alloc] init];
        localNotif.fireDate = [NSDate date];
        localNotif.timeZone = [NSTimeZone defaultTimeZone];
        NSTimeInterval interval = [[NSDate date] timeIntervalSinceDate:_startDate];
        localNotif.alertBody = [NSString stringWithFormat:@"読み込みスタート %lf(sec)", interval];
        localNotif.alertAction = @"Open";
        localNotif.soundName = UILocalNotificationDefaultSoundName;
        [[UIApplication sharedApplication] scheduleLocalNotification:localNotif];
         */
    }
    if (!_userLists && !_userList) {
        [self userInfo];
    }
//    NSLog(@"start");
    
    // 3秒間スリープする
    //[NSThread sleepForTimeInterval:1.5f];
    
//    NSLog(@"end");
    NSString *fetchUrl = [self feedURL];
    
    if (!_configration) {
        NSString *identifier = @"backgroundFetch";
        _configration = [NSURLSessionConfiguration backgroundSessionConfigurationWithIdentifier:identifier];
        _session = [NSURLSession sessionWithConfiguration:_configration delegate:self delegateQueue:nil];
    }
    NSURLRequest* request = [NSURLRequest requestWithURL:[NSURL URLWithString:fetchUrl]];
    NSURLSessionDownloadTask* downloadTask = [_session downloadTaskWithRequest:request];
    [downloadTask resume];
}

- (void)fetchCompletion:(BOOL)completion
{
    if (completion) {
        NSDictionary *dic = [NSDictionary dictionaryWithObject:_downloadData forKey:@"downloadDataKey"];
        [[NSNotificationCenter defaultCenter] postNotificationName:kBackGroundNotificationKey object:self userInfo:dic];
        NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
        [ud setObject:_downloadData forKey:@"downloadData"];
        [ud setObject:[NSDate date] forKey:@"bgDownloadCompTime"];
        [ud synchronize];
        _completionHandler(UIBackgroundFetchResultNewData);

    } else {
        [[NSNotificationCenter defaultCenter] postNotificationName:kBackGroundNotificationKey object:self userInfo:nil];
        _completionHandler(UIBackgroundFetchResultFailed);
    }
    //NSLog(@"%d",completion);
}

-(NSString *)feedURL {
    
    _currentLoadingCategoryKey = _userList.feedCategoryNameAry[_count];
    return _userLists[_count];
}

//===================================
    #pragma mark Download delegate
//===================================

-(void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didFinishDownloadingToURL:(NSURL *)location
{
    NSData *data = [NSData dataWithContentsOfURL:location];
    if (!_downloadData) {
        _downloadData = [NSMutableArray new];
    }
    if (!data) {
        return;
    }
    if ([_currentLoadingCategoryKey  isEqualToString: @"SortByDateMatomeFeeds"]) {

        SMWTitleXMLPaese *titlePaese = [SMWTitleXMLPaese new];
        NSXMLParser *parser = [[NSXMLParser alloc] initWithData:data];
        parser.delegate = titlePaese;
        [parser parse];
    }
    [_downloadData addObject:data];
}

-(void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didWriteData:(int64_t)bytesWritten totalBytesWritten:(int64_t)totalBytesWritten totalBytesExpectedToWrite:(int64_t)totalBytesExpectedToWrite
{
    //NSLog(@"[bytesWritten] %lld, [totalBytesWritten] %lld, [totalBytesExpectedToWrite] %lld", bytesWritten, totalBytesWritten, totalBytesExpectedToWrite);
    //double progress = (double)totalBytesWritten / (double)totalBytesExpectedToWrite;
    //NSLog(@"[progress] %f", progress);
}

-(void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didResumeAtOffset:(int64_t)fileOffset expectedTotalBytes:(int64_t)expectedTotalBytes
{
    //NSLog(@"再開！");
}
- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didCompleteWithError:(NSError *)error
{
    if(error){
        [self userInfoNil];
        [self fetchCompletion:NO];
        NSLog(@"%@", [error localizedDescription]);
        
    }else{
        [self performSelectorOnMainThread:@selector(intervalOfLoading) withObject:nil waitUntilDone:NO];
        //NSLog(@"done!");
    }
    // セッションを必要としなくなった場合，未処理のタスクをキャンセルするために invalidateAndCancel を呼ぶことでセッションを無効とします．
   // [session invalidateAndCancel];
}

//===================================
#pragma mark Save Userdefaults
//===================================
-(void)saveLastReadingTime {
    
    NSString *currentCategory = [self getCurrentCategoryKey];
    NSString *lastFeedUpdate = [currentCategory stringByAppendingString:kLastFeedUpdate];
	[[NSUserDefaults standardUserDefaults] setObject:[NSDate date] forKey:lastFeedUpdate];
}

- (NSString *)getCurrentCategoryKey {
    
    NSString *currentCategory = _currentLoadingCategoryKey;
    
    if ([currentCategory isEqualToString:@"SortByFavoriteOfOneDaysMatome"] ||
        [currentCategory isEqualToString:@"SortByFavoriteOfThreeDaysMatome"] ||
        [currentCategory isEqualToString:@"SortByFavoriteOfOneWeeksMatome"]) {
        
        currentCategory = @"FavorRank";
    }
    return currentCategory;
}

- (void)intervalOfLoading {
    
    BOOL isBackgroundFetching = [[NSUserDefaults standardUserDefaults] boolForKey:@"BackgroundFetching"];
    if (!isBackgroundFetching) {
        [[NSNotificationCenter defaultCenter] postNotificationName:kBackGroundNotificationKey object:self userInfo:nil];
        return;
    }
    _count++;
    if (_count <_userLists.count) {
        [self fetch];
        
    } else {
        [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(fetchTimeout) object:nil];
        NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
        [ud setBool:NO forKey:@"clearCache"];
        [ud setBool:NO forKey:@"BackgroundFetching"];
        [ud synchronize];
        [self scheduleNotification];
        [self fetchCompletion:YES];
        [self userInfoNil];
    }
}

- (void)scheduleNotification
{
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    NSString *title = [ud stringForKey:@"MatomeTitle"];
    
    //NSTimeInterval interval = [[NSDate date] timeIntervalSinceDate:_startDate];
    //NSString * normalTitle = [NSString stringWithFormat:@"バックグラウンドで読み込まれました。最新のニュースをすぐにチェックできます！ %lf(sec)", interval];
    NSString * normalTitle = @"バックグラウンドで読み込まれました。最新のニュースをすぐにチェックできます！";
    if (title) {
        NSArray *titleArray = [ud arrayForKey:@"MatomeTitleArray"];
        if (titleArray.count > 7 || !titleArray) {
            titleArray = [NSArray array];
        }
        titleArray = [titleArray arrayByAddingObject:title];
        [ud setObject:titleArray forKey:@"MatomeTitleArray"];
        [ud synchronize];
        //title = [NSString stringWithFormat:@"%@ %lf(sec)", title, interval];
    }
    NSString* notifTitle = (title)? title : normalTitle;
    
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    UILocalNotification *localNotif = [[UILocalNotification alloc] init];
    localNotif.fireDate = [NSDate date];
    localNotif.timeZone = [NSTimeZone defaultTimeZone];
    
    localNotif.alertBody = notifTitle;
    localNotif.alertAction = @"Open";
    localNotif.soundName = UILocalNotificationDefaultSoundName;
    [[UIApplication sharedApplication] scheduleLocalNotification:localNotif];
}

- (void)fetchTimeout {
    [self userInfoNil];
    /*
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    UILocalNotification *localNotif = [[UILocalNotification alloc] init];
    localNotif.fireDate = [NSDate date];
    localNotif.timeZone = [NSTimeZone defaultTimeZone];
    NSTimeInterval interval = [[NSDate date] timeIntervalSinceDate:_startDate];
    localNotif.alertBody = [NSString stringWithFormat:@"失敗！　 %lf(sec)", interval];
    localNotif.alertAction = @"Open";
    localNotif.soundName = UILocalNotificationDefaultSoundName;
    [[UIApplication sharedApplication] scheduleLocalNotification:localNotif];
     */
    BOOL isBackgroundFetching = [[NSUserDefaults standardUserDefaults] boolForKey:@"BackgroundFetching"];
    if (isBackgroundFetching) {
        NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
        [ud setBool:NO forKey:@"BackgroundFetching"];
        [ud synchronize];
        [self fetchCompletion:NO];
        
    }
}


@end
