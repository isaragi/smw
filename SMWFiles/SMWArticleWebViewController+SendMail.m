//
//  SMWArticleWebViewController+SendMail.m
//  TheMatome
//
//  Created by Atsushi Igarashi on 2013/10/28.
//  Copyright (c) 2013年 Atsushi Igarashi. All rights reserved.
//

#import "SMWArticleWebViewController+SendMail.h"


@implementation SMWArticleWebViewController (SendMail)

static NSString const *MAIL_ADRESS = @"sakutto.matome.yakko@gmail.com";

- (void)sendMailAlert
{
    NSString *ms = @"不適切な内容があるなどの報告をして頂けます。OKを押すとメール送信画面が立ち上がります。";
    UIAlertView *mailAlert = [[UIAlertView alloc] initWithTitle:nil message:ms delegate:self cancelButtonTitle:@"キャンセル" otherButtonTitles:@"OK",nil];
    mailAlert.tag = kSendEMailAlertTag;
    [mailAlert show];
}

-(void)sendEMail
{
    if([MFMailComposeViewController canSendMail]) {
        MFMailComposeViewController *mailCont = [[MFMailComposeViewController alloc] init];
        mailCont.mailComposeDelegate = self;
        
        [mailCont setSubject:@"問題の報告"];
        [mailCont setToRecipients:[NSArray arrayWithObject:MAIL_ADRESS]];
        [mailCont setMessageBody:[self mailBody] isHTML:NO];
        
        [self presentViewController:mailCont animated:YES completion:nil];
    }
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
    [controller dismissViewControllerAnimated:YES completion:nil];
}

- (NSString*)mailBody
{
    NSString *body = [NSString stringWithFormat:
                      @"%@\n\n\n\n下記情報を消さずにこのまま送信して下さい。\n\n・タイトル\n%@\n\n・記事タイトル\n%@\n\n・URL\n%@",
                      @"問題点を簡単に説明して頂けると助かります。",
                      self.channelTitleStr,
                      self.titleStr,
                      self.urlStr, nil];
    
    return body;
}

@end
