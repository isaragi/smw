//
//  SMWChangeColor.h
//  TheMatome
//
//  Created by Atsushi Igarashi.  on 2013/10/10.
//  Copyright (c) 2013年 Atsushi Igarashi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SMWChangeColor : NSObject

- (id)initWithSegment:(UISegmentedControl*)seg refreshControl:(UIRefreshControl*)ref navigarionItem:(UINavigationItem*)navItem navigationController:(UINavigationController*)navCon;

- (void)changeColor;

- (void)richBlackCellWithTableView:(UITableView*)tableView cell:(UITableViewCell*)cell refreshControl:(UIRefreshControl*)ref;

- (void)nightBlckColorWillChangeLabel:(UILabel*)title dateLabel:(UILabel*)date channelLabel:(UILabel*)channelTitle;

+ (BOOL)isTranslucentColor;

- (id)initWithToolBar:(UIToolbar*)toolBar;

- (UIColor *)currentColor;

+ (BOOL)isNightBlackColor;
@end
