//
//  TMChannelItemCell.h
//  TheMatome
//
//  Created by Atsushi Igarashi on 13/07/26.
//  Copyright (c) 2013年 Atsushi Igarashi. All rights reserved.
//

#import <UIKit/UIKit.h>
@class FeedItem;

@interface SMWChannelCell : UITableViewCell

- (void)chConfigureWithFeeditem:(FeedItem *)feeditem;
@end
