//
//  SMWNavBarTitleLabell.m
//  TheMatome
//
//  Created by Atsushi Igarashi on 2013/10/09.
//  Copyright (c) 2013年 Atsushi Igarashi. All rights reserved.
//

#import "SMWNavBarTitleLabel.h"
#import "SMWArticleWebViewController.h"
#import "SMWChangeColor.h"

@implementation SMWNavBarTitleLabell
{
    NSString * _themeColor;
    BOOL _isTranslucentColor;
}

- (id)init
{
    self = [super init];
    if (self) {
        _themeColor = [[NSUserDefaults standardUserDefaults] stringForKey:@"themeColor"];
        _isTranslucentColor = [SMWChangeColor isTranslucentColor];
    }
    return self;
}


- (void)costomTitleLabelWithTitle:(NSString *)str andNavigation:(UINavigationItem *)navi
{
    float iOSVersion = [[[UIDevice currentDevice] systemVersion] floatValue];
    
    if(!iOSVersion >= 7.0) {
        return;
    }
    
    if ( ( _isTranslucentColor == NO && ![_themeColor isEqualToString: @"DefaultsColor"] ) && _themeColor ) {
        
        UIView *titleView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 100, 40)];
        titleView.backgroundColor = [UIColor clearColor];
        titleView.opaque = NO;
        navi.titleView = titleView;
        UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 10, 100, 20)];
        titleLabel.font = [UIFont boldSystemFontOfSize:17.0f];
        titleLabel.text = str;
        titleLabel.textAlignment = NSTextAlignmentCenter;
        titleLabel.textColor = [UIColor whiteColor];
        titleLabel.backgroundColor = [UIColor clearColor];
        titleLabel.adjustsFontSizeToFitWidth = YES;
        [titleView addSubview:titleLabel];
    } else {
        navi.titleView = nil;
        navi.title = str;
    }
    
}

- (void)costomTitleString:(NSString*)str andWebView:(SMWArticleWebViewController*)webView labelSize:(int)size
{
    UIView *titleView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, size, 40)];
    titleView.backgroundColor = [UIColor clearColor];
    titleView.opaque = NO;
    webView.navigationItem.titleView = titleView;

    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 10, size, 20)];
    titleLabel.font = [UIFont boldSystemFontOfSize:15.5f];
    titleLabel.text = str;
    titleLabel.textAlignment = NSTextAlignmentCenter;
    float iOSVersion = [[[UIDevice currentDevice] systemVersion] floatValue];
    
    if(iOSVersion < 7.0)
    {
        titleLabel.textColor = [UIColor whiteColor];
        titleLabel.shadowColor = [UIColor colorWithWhite:0.3f alpha:1.0f];
    } else {
        
        if ( ( _isTranslucentColor == NO && ![_themeColor isEqualToString: @"DefaultsColor"] ) && _themeColor ) {
            
            titleLabel.textColor = [UIColor whiteColor];
        }
    }
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.adjustsFontSizeToFitWidth = YES;
    [titleView addSubview:titleLabel];

}

@end
