//
//  SMWDateFormat.m
//  TheMatome
//
//  Created by Atsushi Igarashi on 2013/10/08.
//  Copyright (c) 2013年 Atsushi Igarashi. All rights reserved.
//

#import "SMWDateFormat.h"

@implementation SMWDateFormat


- (NSDateFormatter *)dateFormatter {
    
    static NSDateFormatter *dateFormatter = nil;
    if (dateFormatter == nil) {
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"MM/dd HH:mm"];
    }
    return dateFormatter;
}

@end
