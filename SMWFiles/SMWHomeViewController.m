//
//  TMViewController.m
//  TheMatome
//
//  Created by Atsushi Igarashi on 13/07/19.
//  Copyright (c) 2013年 Atsushi Igarashi. All rights reserved.
//
#import "SMWAppDelegate.h"
#import "SMWHomeViewController.h"
#import "SMWHomeTableViewCell.h"
#import "SMWViewPagerController.h"
#import "SMWUserFeedList.h"
#import "FeedItem.h"
#import "ChannelName.h"
#import "SMWParseOperation.h"
#import "SMWArticleWebViewController.h"
#import <CFNetwork/CFNetwork.h>
#import "SVProgressHUD.h"
#import "SMWNavBarTitleLabel.h"
#import "SMWSaveBrowsingHistory.h"
#import "SMWChangeColor.h"
#import "SMWDeleteCache.h"

@interface SMWHomeViewController ()  <UIActionSheetDelegate, NSFetchedResultsControllerDelegate>

@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic, strong) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property (nonatomic, strong) NSManagedObjectModel *managedObjectModel;
@property (nonatomic, strong) NSString *sortDescripterKey;
@property (nonatomic, strong) NSString *feedURLString;
@property (nonatomic, strong) NSURLConnection *feedConnection;
@property (nonatomic, strong) NSMutableData *feedItemData;
@property (nonatomic, strong) NSOperationQueue *parseQueue;

@property (nonatomic, assign) NSInteger page;
@property (nonatomic, strong) UIActivityIndicatorView *tableFooterIndicator;
@property (nonatomic, assign) NSInteger feedCount;

@property (nonatomic, assign) int segmentedChangeValue;
@property (nonatomic, assign) int sortSegmentValue;
@property (nonatomic, assign) int sortRangeOfDate;
@property (nonatomic, strong) UISegmentedControl *sortRangeSegment;
@property (nonatomic, strong) UIView *segmentView;

@property (nonatomic, strong) SMWUserFeedList *userList;
@property (nonatomic, strong) NSArray *userLists;
@property (nonatomic, assign) NSInteger roadingCount;
@property (nonatomic, strong) NSString *currentLoadingCategoryKey;
@property (nonatomic, assign) NSInteger categoryNumber;
@end

@implementation SMWHomeViewController

static int const onceReadCount = 15;
static NSTimeInterval const kRefreshTimeInterval = 60 * 30;
static NSString * const kLastFeedUpdateKey = @"LastFeedUpdate";
static NSTimeInterval const kDeleteCacheTimeInterval = 60 * 60;
static NSString * const kLastDeleteCacheKey = @"LastDeleteCache";
static NSString * const kBackGroundNotificationKey = @"BackGroundRoading";

- (void)viewDidLoad {
    
    [super viewDidLoad];
    [self readMoreScroll];
    [self refeshController];
    [self initialUserList];
    _sortDescripterKey = @"date";
    _categoryNumber = _feedNumber;
    [self changeColorWithFlag:YES];
    _roadingCount = 0;
    [self refreshUserList];
    [self lastUpdateTime];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    
    BOOL isClearCache = [ud boolForKey:@"clearCache"];
    if (isClearCache) {
        _roadingCount = 0;
        [self lastUpdateTime];
        [ud setBool:NO forKey:@"clearCache"];
        [ud synchronize];
    }
    [self changeColorWithFlag:NO];
    [super viewWillAppear:animated];
}

-(void)refreshUserList {
    
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    BOOL isReadingListChange = [ud boolForKey:@"isReadingListChange"];
    
    if (isReadingListChange) {
        self.fetchedResultsController = nil;
        [self initialUserList];
        [ud setBool:NO forKey:@"isReadingListChange"];
        [ud synchronize];
    }
}

-(void)initialUserList {
    
    _userList = [SMWUserFeedList new];
    _userLists = [_userList readingLists];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)dealloc {
    
    [_feedConnection cancel];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

//--------------------------------------------------------------//
#pragma mark - NSURLConnectionDelegate
//--------------------------------------------------------------//
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    
    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
    if (([httpResponse statusCode]/100) == 2) {
        self.feedItemData = [NSMutableData data];
    }
    else {
        NSString * errorString = NSLocalizedString(@"HTTP Error", @"Error message displayed when receving a connection error.");
        NSDictionary *userInfo = @{NSLocalizedDescriptionKey : errorString};
        NSError *error = [NSError errorWithDomain:@"HTTP" code:[httpResponse statusCode] userInfo:userInfo];
        [self handleError:error];
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    
    [self.feedItemData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    [SVProgressHUD showErrorWithStatus:@"ロード失敗..."];
    if ([error code] == kCFURLErrorNotConnectedToInternet) {

        NSString * errorString = NSLocalizedString(@"No Connection Error", @"Error message displayed when not connected to the Internet.");
        NSDictionary *userInfo = @{NSLocalizedDescriptionKey : errorString};
        NSError *noConnectionError = [NSError errorWithDomain:NSCocoaErrorDomain code:kCFURLErrorNotConnectedToInternet userInfo:userInfo];
        [self handleError:noConnectionError];
    }
    else {
        // Otherwise handle the error generically.
        [self handleError:error];
    }
    
    self.feedConnection = nil;
    [self nowLoadingWithFlag:NO];
    [self intervalOfLoadingOrEndWithFlag];
}

- (void)nowLoadingWithFlag:(BOOL)flag {
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    [ud setBool:flag forKey:@"NowLoading"];
    [ud synchronize];
}

-(BOOL)isNowLoading {
    return [[NSUserDefaults standardUserDefaults] boolForKey:@"NowLoading"];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    
    self.feedConnection = nil;
    SMWParseOperation *parseOperation = [[SMWParseOperation alloc] initWithData:self.feedItemData
                                                                sharedPSC:self.persistentStoreCoordinator];
    
    parseOperation.isFavorMatome = ([[self getCurrentCategoryKey] isEqualToString:@"FavorRank"])? YES : NO;

    [self.parseQueue addOperation:parseOperation];
    
    self.feedItemData = nil;
    [self saveLastReadingTime];
}

-(void)saveLastReadingTime {
    
    NSString *currentCategory = [self getCurrentCategoryKey];
    NSString *lastFeedUpdate = [currentCategory stringByAppendingString:kLastFeedUpdateKey];
    NSUserDefaults * ud = [NSUserDefaults standardUserDefaults];
	[ud setObject:[NSDate date] forKey:lastFeedUpdate];
    [ud synchronize];
}

- (NSString *)getCurrentCategoryKey {
    
    NSString *currentCategory = _currentLoadingCategoryKey;
    
    if ([currentCategory isEqualToString:@"SortByFavoriteOfOneDaysMatome"] ||
        [currentCategory isEqualToString:@"SortByFavoriteOfThreeDaysMatome"] ||
        [currentCategory isEqualToString:@"SortByFavoriteOfOneWeeksMatome"]) {
        
        currentCategory = @"FavorRank";
    }
    return currentCategory;
}

- (void)handleError:(NSError *)error {
    
    NSString *errorMessage = [error localizedDescription];
    NSString *alertTitle = NSLocalizedString(@"Error Title", @"Title for alert displayed when download or parse error occurs.");
    NSString *okTitle = NSLocalizedString(@"OK ", @"OK Title for alert displayed when download or parse error occurs.");
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:alertTitle message:errorMessage delegate:nil cancelButtonTitle:okTitle otherButtonTitles:nil];
    [alertView show];
}

- (void)feedItemsError:(NSNotification *)notif {
    
    assert([NSThread isMainThread]);
    if (notif.name == kFeedsErrorNotificationName) {
        [self handleError:[[notif userInfo] valueForKey:kFeedsMessageErrorKey]];
    }
}


//--------------------------------------------------------------//
#pragma mark - UITableViewDelegate
//--------------------------------------------------------------//

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return [[self.fetchedResultsController sections] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    NSInteger numberOfRows = 0;
	
    if ([[self.fetchedResultsController sections] count] > 0) {
        
        id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
        numberOfRows = [sectionInfo numberOfObjects];
        _feedCount = numberOfRows;
        
        if (numberOfRows >= onceReadCount) {
            
            if (_page * onceReadCount >= numberOfRows) {
                return numberOfRows;
                
            } else {
                return _page * onceReadCount;
            }
        }
    }
    return numberOfRows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *kFeedItemCellID = @"TMFeedItemCellID";
  	SMWHomeTableViewCell *cell = (SMWHomeTableViewCell *)[tableView dequeueReusableCellWithIdentifier:kFeedItemCellID];

    FeedItem *feeditem = (FeedItem *)[self.fetchedResultsController objectAtIndexPath:indexPath];
    [cell configureWithFeeditem:feeditem];
    [[SMWChangeColor new] richBlackCellWithTableView:self.tableView cell:cell refreshControl:self.refreshControl];
	return cell;
}


//--------------------------------------------------------------//
#pragma mark - Sortsegment in tableHeader
//--------------------------------------------------------------//

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    if (self.isRankingMatomeFeedLoading) {
        
        return 32.0;
    }
    return 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    if (!self.isRankingMatomeFeedLoading) {
        
        _sortRangeSegment.hidden = YES;
        return nil;
        
    } else {
        
        [self createSeg];
        [self checkCurentSegment];
        return self.segmentView;
    }
    return nil;
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    
    [self.tableView reloadData];
}

- (NSFetchedResultsController *)fetchedResultsController {
    
    if (_fetchedResultsController == nil) {
        
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        
        NSEntityDescription *entity = [NSEntityDescription entityForName:@"FeedItem"
                                                  inManagedObjectContext:self.managedObjectContext];
        [fetchRequest setEntity:entity];
        
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:_sortDescripterKey ascending:NO];
        NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor, nil];
        
        [fetchRequest setSortDescriptors:sortDescriptors];
        
        NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
        NSDateComponents *offsetComponents = [[NSDateComponents alloc] init];
        [offsetComponents setDay:-_sortRangeOfDate];  // 1 or 2 or 3 or 7 days back from today
        NSDate *someDaysAgo = [gregorian dateByAddingComponents:offsetComponents toDate:[NSDate date] options:0];

        NSPredicate *pred = [NSCompoundPredicate predicateWithFormat:@"feedCategory.name = %@ AND date > %@",[self categoryName],someDaysAgo];
        [fetchRequest setPredicate:pred];
        
        NSFetchedResultsController *aFetchedResultsController = [[NSFetchedResultsController alloc]
                                                                 initWithFetchRequest:fetchRequest
                                                                 managedObjectContext:self.managedObjectContext
                                                                 sectionNameKeyPath:nil cacheName:nil];
       
        self.fetchedResultsController = aFetchedResultsController;
        
        self.fetchedResultsController.delegate = self;
        
        NSError *error = nil;
        
        if (![self.fetchedResultsController performFetch:&error]) {
            
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            //abort();
        }
        
    }
	
	return _fetchedResultsController;
}


//--------------------------------------------------------------//
#pragma mark - Get CoreData Management
//--------------------------------------------------------------//

- (NSManagedObjectContext *)managedObjectContext {
	
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    SMWAppDelegate *appDelegate = (SMWAppDelegate *)[[UIApplication sharedApplication] delegate];
    self.managedObjectContext = [appDelegate managedObjectContext];
    return _managedObjectContext;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
	
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    SMWAppDelegate *appDelegate = (SMWAppDelegate *)[[UIApplication sharedApplication] delegate];
    self.persistentStoreCoordinator = [appDelegate persistentStoreCoordinator];
    return _persistentStoreCoordinator;
}


//--------------------------------------------------------------//
#pragma mark - Core Data stack
//--------------------------------------------------------------//

- (void)intervalOfLoadingOrEndWithFlag {

    if ([self isNowLoading]) {
        
        [self feedAction];
        
    } else {
        
        [self dismissIndicator];
        [self saveLastReadingTime];
    }
}

-(void)saveLastReadTime {
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    [ud setObject:[NSDate date] forKey:kLastDeleteCacheKey];
    [ud synchronize];
}

-(void)dismissIndicator {
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    [SVProgressHUD dismiss];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if (object == self.parseQueue && [keyPath isEqualToString:@"operationCount"]) {
        
        if (self.parseQueue.operationCount == 0 && [self isNowLoading]) {
            
            [self performSelectorOnMainThread:@selector(intervalOfLoadingOrEndWithFlag) withObject:nil waitUntilDone:NO];
        
        }else if (object == self.parseQueue && [keyPath isEqualToString:@"operationCount"]) {
            
            if (self.parseQueue.operationCount == 0 && ![self isNowLoading]) {
                
                [self performSelectorOnMainThread:@selector(backgroundDownloadCheck) withObject:nil waitUntilDone:NO];
            }
        }
    }else {
        [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
    }
}


//--------------------------------------------------------------//
#pragma mark - Table view delegate
//--------------------------------------------------------------//

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    FeedItem *feeditem = (FeedItem *)[self.fetchedResultsController objectAtIndexPath:indexPath];    
    [self performSegueWithIdentifier:@"feedItemCellPush" sender:feeditem];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"feedItemCellPush"]) {
        FeedItem *feeditem = sender;
        
        NSDictionary* dic = [NSDictionary dictionaryWithObjectsAndKeys:
                             feeditem.title, @"title",
                             feeditem.date, @"date",
                             feeditem.url, @"url",
                             feeditem.channelName.name, @"channelTitle",
                             feeditem.tweet, @"tweet",
                             feeditem.hatebu, @"hatebu", nil];
        
        [[SMWSaveBrowsingHistory new] saveBrowsingHistory:dic];
        SMWArticleWebViewController *webView = (SMWArticleWebViewController *)[segue destinationViewController];
        webView.webData = dic;
        
        NSString *chanStr = feeditem.channelName.name;
        [[SMWNavBarTitleLabell new] costomTitleString:chanStr andWebView:webView labelSize:170];
         
    }
}


//--------------------------------------------------------------//
#pragma mark - Segmented Controll
//--------------------------------------------------------------//

- (void)createSeg
{
    if (_sortRangeSegment) {
        return;
    }
    
    NSArray *items = [NSArray arrayWithObjects:@"24時間", @"3日間", @"1週間", nil];
    _sortRangeSegment = [[UISegmentedControl alloc]initWithItems:items];
    _sortRangeSegment.frame = CGRectMake(0, 0, 320, 32);
    
    [_sortRangeSegment addTarget:self action:@selector(sortSegmentValueChanged:) forControlEvents:UIControlEventValueChanged];
    [self addsegment];
}
-(void)addsegment {
    
    self.segmentView = [[UIView alloc] init];
    self.segmentView.backgroundColor = [UIColor whiteColor];
    [self.segmentView addSubview:_sortRangeSegment];
}

- (void)sortSegmentValueChanged:(id)sender
{
    switch (self.sortRangeSegment.selectedSegmentIndex) {
        case 0:
            [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"sortSegmentValue"];
            break;
            
        case 1:
            [[NSUserDefaults standardUserDefaults] setInteger:1 forKey:@"sortSegmentValue"];
            break;
        
        case 2:
            [[NSUserDefaults standardUserDefaults] setInteger:2 forKey:@"sortSegmentValue"];
            break;
    }
    [self.tableView setTableFooterView:nil];
    _roadingCount = 0;
    self.fetchedResultsController = nil;
    [self lastUpdateTime];
}

-(void)checkCurentSegment
{
    if (!self.isRankingMatomeFeedLoading) {
        
        _sortDescripterKey = @"date";
        _sortRangeOfDate = 1;

    } else {
        
        _sortDescripterKey = @"rank";
        _sortSegmentValue = (int)[[NSUserDefaults standardUserDefaults] integerForKey:@"sortSegmentValue"];
        
        if (_sortSegmentValue == 0 || !_sortSegmentValue) {
            
            _sortRangeOfDate = 1;
            self.sortRangeSegment.selectedSegmentIndex = 0;
            
        } else if (_sortSegmentValue == 1) {
            
            _sortRangeOfDate = 3;
            self.sortRangeSegment.selectedSegmentIndex = 1;
            
        } else {
            
            _sortRangeOfDate = 7;
            self.sortRangeSegment.selectedSegmentIndex = 2;
        }
    }
    
}

-(NSString *)categoryName {
    
    if (self.isRankingMatomeFeedLoading) {
            
        switch (self.sortRangeSegment.selectedSegmentIndex) {
            case 0:
                return @"SortByFavoriteOfOneDaysMatome";
                break;
                
            case 1:
                return @"SortByFavoriteOfThreeDaysMatome";
                break;
                    
            case 2:
                return @"SortByFavoriteOfOneWeeksMatome";
                break;
            }
        }
    return _userList.feedCategoryNameAry[_categoryNumber];
}


//--------------------------------------------------------------//
#pragma mark - Change Navigation Color
//--------------------------------------------------------------//

-(void)changeColorWithFlag:(BOOL)firstLoading {
    
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    BOOL isColorChange = [ud boolForKey:@"isColorChange"];
    if (isColorChange || firstLoading ) {
        
        SMWChangeColor *changeColor = [[SMWChangeColor alloc] initWithSegment:self.sortRangeSegment refreshControl:self.refreshControl navigarionItem:_viewPager.navigationItem navigationController:_viewPager.navigationController];
        [changeColor changeColor];
        [ud setBool:NO forKey:@"isColorChange"];
        [ud synchronize];
    }
}


//--------------------------------------------------------------//
#pragma mark - Action Refresh
//--------------------------------------------------------------//

-(void)refeshController
{
    self.refreshControl = [[UIRefreshControl alloc] init];
    [self.refreshControl addTarget:self action:@selector(actionRefresh:) forControlEvents:UIControlEventValueChanged];

}

-(IBAction)actionRefresh:(id)sender {

    SMWChangeColor *color = [[SMWChangeColor new] initWithSegment:nil refreshControl:self.refreshControl navigarionItem:nil navigationController:nil];
    
    [color changeColor];
    
    BOOL isBackgroundFetching = [[NSUserDefaults standardUserDefaults] boolForKey:@"BackgroundFetching"];
    if (isBackgroundFetching) {
        [self.refreshControl endRefreshing];
        return;
    }
    
    _page = 1;
    _roadingCount = 0;
    [self readyStertAction];
    [self.refreshControl endRefreshing];
}

- (void)resetScrollOffsetAndInset:(id)arg
{
    UIScrollView *scrollView = (UIScrollView *)self.view;
    UIEdgeInsets inset;
    inset.left = 0;
    inset.right = 0;
    inset.top = 0;
    inset.bottom = 0;
    scrollView.contentInset = inset;
    CGSize size = scrollView.contentSize;
    size.height = self.view.frame.size.height;
    scrollView.contentSize = size;
}


//--------------------------------------------------------------//
#pragma mark - Read Action
//--------------------------------------------------------------//

-(void)lastUpdateTime {
    
    if ([self isTimeOfRefresh]) {

        [self readyStertAction];
        
    } else {

        [self checkCurentSegment];
    }
    [self.tableView reloadData];
}

-(BOOL)isTimeOfRefresh {
    
    BOOL isBackgroundFetching = [[NSUserDefaults standardUserDefaults] boolForKey:@"BackgroundFetching"];
    if (isBackgroundFetching) {
        //NSLog(@"フェッチ！");
        return NO;
    }
    //NSLog(@"フェッチしてない！");
    
    if ([self isNowLoading]) {
        //NSLog(@"読み込まない");
        return NO;
    }
    [self checkUpCache];
    NSString *currentCategory = (self.isRankingMatomeFeedLoading) ? @"FavorRank" : [self categoryName];
    NSString *lastFeedUpdate = [currentCategory stringByAppendingString:kLastFeedUpdateKey];
    NSDate *lastUpdate = [[NSUserDefaults standardUserDefaults] objectForKey:lastFeedUpdate];

    if ( (-[lastUpdate timeIntervalSinceNow] > kRefreshTimeInterval) || !lastUpdate) {
        return YES;
    }
    return NO;
}

-(void)checkUpCache {
    
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    NSDate *lastDelete = [ud objectForKey:kLastDeleteCacheKey];
    
    if (-[lastDelete timeIntervalSinceNow] > kDeleteCacheTimeInterval) {
        
        [[SMWDeleteCache new] deleteCache];
        [ud setObject:[NSDate date] forKey:kLastDeleteCacheKey];
        
    }
    
    BOOL isClearCache = [ud boolForKey:@"clearCache"];
    if (isClearCache) {
        
        for (int i = 0; i < [[self deleteNameAry] count] ; i++) {
            
            NSString *str = self.deleteNameAry[i];
            NSString *lastFeedUpdate = [str stringByAppendingString:kLastFeedUpdateKey];
            [ud setObject:nil forKey:lastFeedUpdate];
        }
        [ud setBool:NO forKey:@"clearCache"];
    }
    [ud synchronize];
}

-(NSArray*)deleteNameAry {
    
    return@[@"TopFeeds",@"EntertainmentFeeds",@"PopularFeeds",@"SportsFeeds",@"ITFeeds",
            @"BusinessFeeds",@"WorldFeeds",@"SortByDateMatomeFeeds",@"FavorRank"];
}

-(void)readyStertAction
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeGradient];
    [self feedAction];
}

-(void)feedAction
{
    [self nowLoadingWithFlag:YES];
    [self checkCurentSegment];
    
    self.feedURLString = [self feedURL];
    
    if (![self isNowLoading]) {
        
        [self intervalOfLoadingOrEndWithFlag];
        return;
    }
    
    [self.parseQueue cancelAllOperations];
    [self.parseQueue removeObserver:self forKeyPath:@"operationCount"];
    self.parseQueue = [NSOperationQueue new];
    
    NSURLRequest *feedURLRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:self.feedURLString]];
    self.feedConnection = [[NSURLConnection alloc] initWithRequest:feedURLRequest delegate:self];

    NSAssert(self.feedConnection != nil, @"Failure to create URL connection.");
    
    [self.parseQueue addObserver:self forKeyPath:@"operationCount" options:0 context:NULL];
    
    // observe for any errors that come from our parser
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(feedItemsError:) name:kFeedsErrorNotificationName object:nil];
}

-(NSString *)feedURL {
    
    if (!_userList) {
        
        [self initialUserList];
    }
    if ([self isNowLoading]) {
        
        if (_userLists.count > _roadingCount) {
            _currentLoadingCategoryKey = _userList.feedCategoryNameAry[self.roadingCount];
            return _userLists[_roadingCount++];
        }
    }
    [self nowLoadingWithFlag:NO];
    return nil;
}

-(BOOL)isRankingMatomeFeedLoading {
    
    if (_categoryNumber == _viewPager.favorMatomeNumber) {
        
        return YES;
    }
    return NO;
}

-(void)foregroundReload {
        
    _roadingCount = 0;
    [SVProgressHUD showWithStatus:@"更新をチェック中..." maskType:SVProgressHUDMaskTypeGradient];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    NSDate *conmTime = [ud objectForKey:@"bgDownloadCompTime"];
    
    if ( (-[conmTime timeIntervalSinceNow] < kRefreshTimeInterval) && conmTime) {

        [SVProgressHUD showWithStatus:@"読み込んだデータを表示中" maskType:SVProgressHUDMaskTypeGradient];
        [self backgroundDownloadCheck];
        return;
    }

    BOOL isBackgroundFetching = [[NSUserDefaults standardUserDefaults] boolForKey:@"BackgroundFetching"];
    if (isBackgroundFetching) {
        [SVProgressHUD showWithStatus:@"読み込み中" maskType:SVProgressHUDMaskTypeGradient];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(backGroundRoading:) name:kBackGroundNotificationKey object:nil];
    } else if ([self isTimeOfRefresh]) {
        
        [self nowLoadingWithFlag:YES];
        [self performSelector:@selector(updatingNotice) withObject:nil afterDelay:1.5f];
        
    } else {
        [self performSelector:@selector(dismissIndicator) withObject:nil afterDelay:1.5f];
    }
}
-(void)updatingNotice {
    [SVProgressHUD showWithStatus:@"更新中..." maskType:SVProgressHUDMaskTypeGradient];
    [self feedAction];
}

- (void)backGroundRoading:(NSNotification*)notif {
    NSDictionary *dic = [notif userInfo];
    if (dic) {
        [self foregroundReload];
    }else {
        [self dismissIndicator];
    }
}


//--------------------------------------------------------------//
#pragma mark - Read More Scroll
//--------------------------------------------------------------//

- (void)readMoreScroll
{
    _page = 1;
    _tableFooterIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    [self.tableFooterIndicator setColor:[UIColor darkGrayColor]];
    [self.tableFooterIndicator setHidesWhenStopped:YES];
    [self.tableFooterIndicator stopAnimating];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (self.tableView.contentOffset.y >= (self.tableView.contentSize.height - self.tableView.bounds.size.height))
	{
        
		if([_tableFooterIndicator isAnimating]) {
			return;
		}
        
        if ((_feedCount == [[_fetchedResultsController fetchedObjects] count]) && (_feedCount / onceReadCount)<= _page ) {
            [self endFooter];
            return;
        }
        
        if (_feedCount > (_page * onceReadCount)) {
            [self startIndicator];
            [self performSelector:@selector(nextData) withObject:nil afterDelay:0.1f];
        }
		
    }
}

- (void)nextData
{
    _page++;
    [self.tableView reloadData];
    [self endIndicator];
}

- (void)startIndicator
{
    [_tableFooterIndicator startAnimating];
	CGRect footerFrame = self.tableView.tableFooterView.frame;
	footerFrame.size.height += 44.0f;
	
    [_tableFooterIndicator setFrame:footerFrame];
    [self.tableView setTableFooterView:_tableFooterIndicator];
}

- (void)endIndicator
{
    [_tableFooterIndicator stopAnimating];
    [_tableFooterIndicator removeFromSuperview];
    [self.tableView setTableFooterView:nil];
}

- (void)endFooter
{
    CGRect footerFrame = self.tableView.tableFooterView.frame;
	footerFrame.size.height = 30.0f;
    
    CGRect windowSize = [[UIScreen mainScreen] bounds];
    UIView *cherryIcon  = [[UIView alloc] init];
    UIImage *img = [UIImage imageNamed:@"endFooter.png"];
    UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(windowSize.size.width / 2 - 8, 8, 16, 16)];
    [btn setBackgroundImage:img forState:UIControlStateNormal];
    [btn addTarget:self action:nil forControlEvents:UIControlEventTouchUpInside];
    [cherryIcon addSubview:btn];
    [cherryIcon setFrame:footerFrame];
    
    self.tableView.tableFooterView = cherryIcon;
}

//--------------------------------------------------------------//
#pragma mark - Background Downlord Data Check
//--------------------------------------------------------------//

-(void)backgroundDownloadCheck {
    
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    
    if (_roadingCount >= _userLists.count) {
        [self dismissIndicator];
        [ud removeObjectForKey:@"bgDownloadCompTime"];
        [ud removeObjectForKey:@"downloadData"];
        [self saveLastReadTime];
        return ;
    }
    NSArray *dataAr = [ud arrayForKey:@"downloadData"];
    NSData *data = dataAr[_roadingCount];
    _currentLoadingCategoryKey =  _userList.feedCategoryNameAry[_roadingCount];
    [self saveLastReadingTime];
    
    _roadingCount++;
    SMWParseOperation *parseOperation = [[SMWParseOperation alloc] initWithData:data
                                                                      sharedPSC:self.persistentStoreCoordinator];
    
    parseOperation.isFavorMatome = ([[self getCurrentCategoryKey] isEqualToString:@"FavorRank"])? YES : NO;
    [self.parseQueue cancelAllOperations];
    [self.parseQueue removeObserver:self forKeyPath:@"operationCount"];
    self.parseQueue = [NSOperationQueue new];
    [self.parseQueue addOperation:parseOperation];
    [self.parseQueue addObserver:self forKeyPath:@"operationCount" options:0 context:NULL];
}
@end
