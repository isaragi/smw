//
//  SMWArticleWebViewController+Social.h
//  TheMatome
//
//  Created by Atsushi Igarashi on 2013/10/25.
//  Copyright (c) 2013年 Atsushi Igarashi. All rights reserved.
//

#import "SMWArticleWebViewController.h"
#import <Social/Social.h>

@interface SMWArticleWebViewController (Social)
- (void)postToFacebook:(id)sender;
- (void)postToTwitter:(id)sender;
- (void)postToLine:(id)sender;
- (void)savePocket;
- (void)goPocket;

- (void)postSocial1;
- (void)postSocial2;
- (void)postSocial3;

@end
