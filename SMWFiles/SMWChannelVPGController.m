//
//  SMWChannelVPGController.m
//  SMW
//
//  Created by atsushi igarashi on 2015/05/20.
//  Copyright (c) 2015年 Atsushi Igarashi. All rights reserved.
//

#import "SMWChannelVPGController.h"

@interface SMWChannelVPGController ()

@end

@implementation SMWChannelVPGController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    CGRect frame = self.view.frame;
    frame.origin.x = 0.0;
    frame.origin.y = 0.0;
    frame.size.width = CGRectGetWidth(self.view.frame)/1.16;
    frame.size.height = self.navigationController.navigationBar.frame.size.height;
    self.view.frame = frame;
    
    //GRect rect = CGRectMake(0, 0, -250, self.navigationController.navigationBar.frame.size.height);
    //self.view.frame = rect;
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)layoutSubviews {

    CGFloat topLayoutGuide = 20.0;
    if (self.navigationController && !self.navigationController.navigationBarHidden) {
        topLayoutGuide += self.navigationController.navigationBar.frame.size.height;
    }
    
    CGRect frame = self.tabsView.frame;
    frame.origin.x = 16.0;
    frame.origin.y = 0;
    frame.size.width = CGRectGetWidth(self.view.frame)/1.16;
    frame.size.height = self.navigationController.navigationBar.frame.size.height;
    self.tabsView.frame = frame;
    
    frame = self.contentView.frame;
    frame.origin.x = 0.0;
    frame.origin.y = topLayoutGuide;
    frame.size.width = CGRectGetWidth(self.view.frame);
    frame.size.height = CGRectGetHeight(self.view.frame) - topLayoutGuide - CGRectGetHeight(self.tabBarController.tabBar.frame);
    self.contentView.frame = frame;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
