//
//  SMWViewAnimation.h
//  TheMatome
//
//  Created by Atsushi Igarashi on 2014/02/10.
//  Copyright (c) 2014年 Atsushi Igarashi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SMWViewAnimation : NSObject

#define kPushedBackAnimationDuration 0.2
#define kBringForwardAnimationDuration 0.2

-(CAAnimationGroup*)animationGroupPushedBackward;
-(CAAnimationGroup*)animationGroupBringForward;
- (UIImageView *)screenshot;

@end
