//
//  SMWDateFormat.h
//  TheMatome
//
//  Created by Atsushi Igarashi on 2013/10/08.
//  Copyright (c) 2013年 Atsushi Igarashi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SMWDateFormat : NSObject

- (NSDateFormatter *)dateFormatter;

@end
