//
//  TMSearchTitleViewController.m
//  TheMatome
//
//  Created by Atsushi Igarashi on 2013/10/25.
//  Copyright (c) 2013年 Atsushi Igarashi. All rights reserved.
//

#import "SMWSearchBarViewController.h"
#import "SMWAppDelegate.h"
#import "SMWSearchBarCell.h"
#import "SVProgressHUD.h"
#import "SMWArticleWebViewController.h"
#import "SMWNavBarTitleLabel.h"
#import "SMWSaveBrowsingHistory.h"
#import "SMWChangeColor.h"
#import "FeedItem.h"
#import "ChannelName.h"

@interface SMWSearchBarViewController ()

@property (nonatomic, strong) NSMutableArray *searchData;

@end


@implementation SMWSearchBarViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[SMWNavBarTitleLabell new] costomTitleLabelWithTitle:@"検索" andNavigation:self.navigationItem];
    //[self.navigationController setToolbarHidden:YES animated:YES];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_searchData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"SearchTitleCell";
    
    SMWSearchBarCell *cell = (SMWSearchBarCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    NSArray* dic = [_searchData objectAtIndex:indexPath.row];
    [cell configureWithCellData:dic];
    cell.selectionStyle = UITableViewCellSelectionStyleBlue;
    [[SMWChangeColor new] richBlackCellWithTableView:self.tableView cell:cell refreshControl:nil];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"SearchTitleWebPush"]) {
        
        if([sender isKindOfClass:[UITableViewCell class]]) {
            NSIndexPath * indexPath = [self.tableView indexPathForCell:sender];
            NSArray *searchData = [_searchData objectAtIndex:indexPath.row];
            FeedItem *item = (FeedItem *)searchData;
            NSDictionary* webData = [NSDictionary dictionaryWithObjectsAndKeys:
                                 [searchData valueForKey:@"title"], @"title",
                                 [searchData valueForKey:@"date"], @"date",
                                 [searchData valueForKey:@"url"], @"url",
                                 item.channelName.name, @"channelTitle",
                                 [searchData valueForKey:@"tweet"], @"tweet",
                                 [searchData valueForKey:@"hatebu"], @"hatebu", nil];

            [[SMWSaveBrowsingHistory new] saveBrowsingHistory:webData];
            SMWArticleWebViewController *webView = [segue destinationViewController];
            webView.webData = webData;
            
            NSString *chanStr = item.channelName.name;
            [[SMWNavBarTitleLabell new] costomTitleString:chanStr andWebView:webView labelSize:165];

        }
    }
}

-(void)showAlert:(int)hitCount
{
    NSString *st = @"";
    if (hitCount == 0) {
        st =[NSString stringWithFormat:@"%d Hit...",hitCount];
        [SVProgressHUD showErrorWithStatus:st];
    }else {
        st =[NSString stringWithFormat:@"%d Hit☻",hitCount];
        [SVProgressHUD showSuccessWithStatus:st];
    }
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    if (_searchData) {
        _searchData =nil;
        [self.tableView reloadData];
    }
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if ([searchText isEqualToString:@""]) {
        return;
    }
    
    NSManagedObjectContext *managedObjectContext = [self checkManagedObjectContext];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setReturnsObjectsAsFaults:NO];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"FeedItem"
                                              inManagedObjectContext:managedObjectContext];
    [fetchRequest setEntity:entity];
    
    [fetchRequest setFetchBatchSize:1520];
    
    NSSortDescriptor *sort = [[NSSortDescriptor alloc] initWithKey:@"date" ascending:NO];
    [fetchRequest setSortDescriptors:[NSArray arrayWithObject:sort]];
    
    NSPredicate *pred;
    NSArray *array = [searchText componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    NSMutableArray *ar = [NSMutableArray arrayWithArray:array];
    [ar removeObject:@""];
    //AND検索
    if ([ar count] > 1) {
        
        NSString *st =@"title contains [c]%@";
        NSString *and =@" AND ";
        NSMutableString *mStr = [NSMutableString string];
        
        for (int i = 0; i < [ar count]; i++) {
            
            [mStr appendString:st];
            
            if (i < ([ar count]-1)) {
                
                [mStr appendString:and];
            }
        }
        
        pred = [NSCompoundPredicate predicateWithFormat:mStr argumentArray:ar];
    } else {
        
        NSString *replace = [searchText stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        pred = [NSCompoundPredicate predicateWithFormat:@"title contains [c]%@",replace];
    }
    
    [fetchRequest setPredicate:pred];
    
    NSFetchedResultsController *fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                                               managedObjectContext:managedObjectContext
                                                                                                 sectionNameKeyPath:nil
                                                                                                          cacheName:nil];
    
    NSError *error = nil;
    if (![fetchedResultsController performFetch:&error]) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
    }
    
    NSArray *fetchedAr = [fetchedResultsController fetchedObjects];
    _searchData = [NSMutableArray arrayWithArray:fetchedAr];
    
    //重複削除
    //もっとスマートなやり方があるきがする。
    FeedItem *feedItem = nil;
    NSMutableIndexSet* targetIndexes = [NSMutableIndexSet indexSet];
    NSUInteger index = 0;
    NSUInteger num = 0;
    for(feedItem in _searchData) {
        
        fetchRequest.predicate = [NSPredicate predicateWithFormat:@"channelName.name = %@ AND date = %@ AND title = %@", feedItem.channelName.name, feedItem.date, feedItem.title];
        NSArray *fetchedItems = [managedObjectContext executeFetchRequest:fetchRequest error:&error];
        
        if (fetchedItems.count >= 2) {
            
            if (index > num) {
                num++;
                
            } else {
                
                for (int i = 0; (fetchedItems.count-1) > i; i++) {

                    [targetIndexes addIndex:index];
                    index++;
                }
            }
        }
        if (num == index) {
            index++;
            num = index;
        }
    }

    [_searchData removeObjectsAtIndexes:targetIndexes];

    
    BOOL bl = [searchText hasSuffix:@" "];
    BOOL bl2 = [searchText hasSuffix:@"　"];
    if (!(bl || !bl2 ) || (!bl || bl2)) {
        [self showAlert:(int)[_searchData count]];
    }

}

- (NSManagedObjectContext *)checkManagedObjectContext {
	
    SMWAppDelegate *appDelegate = (SMWAppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *managedObjectContext = [appDelegate managedObjectContext];
    return managedObjectContext;
}


- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [self.tableView reloadData];
    [searchBar resignFirstResponder];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
}

@end
