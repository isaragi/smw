//
//  SMWChannelViewPagerController.m
//  TheMatome
//
//  Created by Atsushi Igarashi on 2014/02/15.
//  Copyright (c) 2014年 Atsushi Igarashi. All rights reserved.
//

#import "SMWChannelViewPagerController.h"
#import "SMWChannelViewController.h"
#import "SMWUserFeedList.h"
#import "SMWChangeColor.h"

@interface SMWChannelViewPagerController () <ViewPagerDataSource, ViewPagerDelegate>
@property (nonatomic, assign) NSUInteger numberOfTabs;
@property (nonatomic, strong) SMWUserFeedList *tabData;

@property (nonatomic, strong) TMChannelViewController *channelView;
@property (nonatomic, strong) NSMutableArray *channelViewArray;
@property (nonatomic, strong) SMWChangeColor *color;

@end

@implementation SMWChannelViewPagerController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    [self applicationWillEnterForegroundNotification];
    self.dataSource = self;
    self.delegate = self;
    [self loadContent];
    [self inserteNilForViewPegerAry];
    [self changeColorWithFlag:YES];
}

- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    [self changeColorWithFlag:NO];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)loadContent {
    
    _tabData = [SMWUserFeedList new];
    self.numberOfTabs = [_tabData.userTabList count];
    
    if (_tabData.isFavorMatomeFeed) {
        _favorMatomeNumber = _tabData.userFeedCount;
    } else {
        _favorMatomeNumber = 100;
    }
}

#pragma mark - Interface Orientation Changes
- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    
    // Update changes after screen rotates
    [self performSelector:@selector(setNeedsReloadOptions) withObject:nil afterDelay:duration];
}

#pragma mark - ViewPagerDataSource
- (NSUInteger)numberOfTabsForViewPager:(ViewPagerController *)viewPager {
    return self.numberOfTabs;
}
- (UIView *)viewPager:(ViewPagerController *)viewPager viewForTabAtIndex:(NSUInteger)index {
    
    UILabel *label = [UILabel new];
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont boldSystemFontOfSize:12.0];
    label.numberOfLines = 2;
    label.text = _tabData.userTabList[index];
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = ([SMWChangeColor isTranslucentColor]) ? [UIColor blackColor] : [UIColor whiteColor];
    [label sizeToFit];
    
    return label;
}

- (UIViewController *)viewPager:(ViewPagerController *)viewPager contentViewControllerForTabAtIndex:(NSUInteger)index {
    
    id obj =[_channelViewArray objectAtIndex:index];
    
    if (obj == [NSNull null]) {
        
        _channelView = [self.storyboard instantiateViewControllerWithIdentifier:@"channelViewController"];
        _channelView.feedNumber = index;
        _channelView.viewPager = self;
        [_channelViewArray replaceObjectAtIndex:index withObject:_channelView];
        
    } else {
        
        _channelView = (TMChannelViewController *)obj;
    }
    return _channelView;
}


#pragma mark - ViewPagerDelegate
- (CGFloat)viewPager:(ViewPagerController *)viewPager valueForOption:(ViewPagerOption)option withDefault:(CGFloat)value {
    
    CGFloat width = [UIScreen mainScreen].bounds.size.width;
    
    switch (option) {
        case ViewPagerOptionStartFromSecondTab:
            return 0.0;
        case ViewPagerOptionCenterCurrentTab:
            return 1.0;
        case ViewPagerOptionTabLocation:
            return 1.0;
        case ViewPagerOptionTabHeight:
            return 44.0;
        case ViewPagerOptionTabOffset:
            return 36.0;
        case ViewPagerOptionTabWidth:
            return ([self isFewTabs]) ? (width - 58) / self.numberOfTabs : 62.0;
        case ViewPagerOptionFixFormerTabsPositions:
            return 0.0;
        case ViewPagerOptionFixLatterTabsPositions:
            return 0.0;
        default:
            return value;
    }
}

-(BOOL)isFewTabs {
    if (self.numberOfTabs <= 4) {
        
        return YES;
    }
    return NO;
}

- (UIColor *)viewPager:(ViewPagerController *)viewPager colorForComponent:(ViewPagerComponent)component withDefault:(UIColor *)color {
    
    BOOL isNightBlockColor = [SMWChangeColor isNightBlackColor];
    UIColor *currentColor = [_color currentColor];
    
    switch (component) {
        case ViewPagerIndicator:
            return currentColor;
            
        case ViewPagerTabsView:
            return [UIColor clearColor];
            
        case ViewPagerContent:
            return (isNightBlockColor) ? [UIColor darkGrayColor]:[UIColor whiteColor];
            
        default:
            return color;
    }
}

-(void)inserteNilForViewPegerAry {
    
    _channelViewArray = [NSMutableArray arrayWithCapacity:_numberOfTabs];
    for (int i = 0; _numberOfTabs > i; i++) {
        [_channelViewArray addObject:[NSNull null]];
    }
}

-(void)changeColorWithFlag:(BOOL)firstLoading  {
    
    BOOL isColorChange = [[NSUserDefaults standardUserDefaults] boolForKey:@"isColorChange"];
    if (isColorChange || firstLoading ) {
        
        _color = [SMWChangeColor new];
        [self reloadData];
    }
}

-(void)applicationWillEnterForegroundNotification
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(willEnterForegroundLoad)
                                                 name:UIApplicationWillEnterForegroundNotification
                                               object:nil];
}

-(void)willEnterForegroundLoad
{
    BOOL isAutoReload = [[NSUserDefaults standardUserDefaults] boolForKey:@"AutoReloadSwitchValue"];
    if (!isAutoReload) {
        return ;
    }
    if(self.navigationController.visibleViewController == self) {
        
        [_channelView foregroundReload];
    }
}


@end
