//
//  SMWTitleXMLPaese.m
//  SMW
//
//  Created by atsushi igarashi on 2015/05/22.
//  Copyright (c) 2015年 Atsushi Igarashi. All rights reserved.
//

#import "SMWTitleXMLPaese.h"
@interface SMWTitleXMLPaese ()
@property (nonatomic, strong) NSMutableString *currentParsedCharacterData;
@end

@implementation SMWTitleXMLPaese {
    NSDateFormatter *_dateFormatter;
    BOOL _accumulatingParsedCharacterData;
    BOOL _didAbortParsing;
}

- (instancetype)init
{
    self = [super init];
    if (self) {

        _isItem = NO;
        self.currentParsedCharacterData = [[NSMutableString alloc] init];
    }
    return self;
}

//--------------------------------------------------------------//
#pragma mark - Parser constants
//--------------------------------------------------------------//
BOOL  _isItem;

static NSString * const kItemElementName = @"item";
static NSString * const kTitleElementName = @"title";

//--------------------------------------------------------------//
#pragma mark - NSXMLParser delegate methods
//--------------------------------------------------------------//
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict {
    
    if ([elementName isEqualToString:kItemElementName]) {
        
        _isItem = YES;
    }
    
    else if ([elementName isEqualToString:kTitleElementName] )
    {
        _accumulatingParsedCharacterData = YES;
        [self.currentParsedCharacterData setString:@""];
    }
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    if ([elementName isEqualToString:kItemElementName]) {
        
        _isItem = NO;
    }
    
    if ([elementName isEqualToString:kTitleElementName]) {
        if (_isItem) {
            NSString *title = [NSString stringWithString:self.currentParsedCharacterData];
            if ([self isNewTitleWithTitle:title]) {
                [self saveTitleWithTitle:title];
                [parser abortParsing];
                _didAbortParsing = YES;
            }
            
        }
    }
    _accumulatingParsedCharacterData = NO;
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {
    if (_accumulatingParsedCharacterData) {
        
        [self.currentParsedCharacterData appendString:string];
    }
}

- (void)parser:(NSXMLParser *)parser parseErrorOccurred:(NSError *)parseError {
    
    if ([parseError code] != NSXMLParserDelegateAbortedParseError && !_didAbortParsing) {
        //[self performSelectorOnMainThread:@selector(handleFeedItemsError:) withObject:parseError waitUntilDone:NO];
    }
}

- (BOOL)isNewTitleWithTitle:(NSString *)title {
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    NSString *oldTitle = [ud stringForKey:@"MatomeTitle"];
    NSArray *titleArray = [ud arrayForKey:@"MatomeTitleArray"];
    if ([title isEqualToString:oldTitle]) {
        return NO;
    }
    if ([titleArray containsObject:title]) {
        return NO;
    }
    return YES;
}

- (void)saveTitleWithTitle:(NSString *)title {
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    [ud setObject:title forKey:@"MatomeTitle"];
    [ud synchronize];
}

@end
