//
//  TMFeedItemTableViewCell.h
//  TheMatome
//
//  Created by Atsushi Igarashi on 13/07/19.
//  Copyright (c) 2013年 Atsushi Igarashi. All rights reserved.
//

#import <UIKit/UIKit.h>
@class FeedItem;

@interface SMWHomeTableViewCell : UITableViewCell

- (void)configureWithFeeditem:(FeedItem *)feeditem;

@end
