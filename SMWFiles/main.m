//
//  main.m
//  TheMatome
//
//  Created by Atsushi Igarashi on 13/07/19.
//  Copyright (c) 2013年 Atsushi Igarashi. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SMWAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([SMWAppDelegate class]));
    }
}
