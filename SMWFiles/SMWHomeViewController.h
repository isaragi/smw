//
//  TMViewController.h
//  TheMatome
//
//  Created by Atsushi Igarashi on 13/07/19.
//  Copyright (c) 2013年 Atsushi Igarashi. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SMWViewPagerController;

@interface SMWHomeViewController : UITableViewController
@property (nonatomic, assign) NSInteger feedNumber;
@property (nonatomic, strong) SMWViewPagerController *viewPager;
-(void)foregroundReload;
@end
