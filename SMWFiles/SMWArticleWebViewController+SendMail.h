//
//  SMWArticleWebViewController+SendMail.h
//  TheMatome
//
//  Created by Atsushi Igarashi on 2013/10/28.
//  Copyright (c) 2013年 Atsushi Igarashi. All rights reserved.
//

#import "SMWArticleWebViewController.h"
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>

@interface SMWArticleWebViewController (SendMail) <MFMailComposeViewControllerDelegate>

- (void)sendMailAlert;
- (void)sendEMail;

@end
